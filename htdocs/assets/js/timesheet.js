//* ***Javascript functions for the Timesheet app*****//
//* ***Sorted alphabetically*****//
// namespace all functions for organization and to encapsulate functions

// top level
var TimesheetApp = {}

TimesheetApp.Admin = {

  /**
	 * Submits Edit Hours modal form and handles response
   *
	 * @param    baseURL   input string
	 */
  chartUserHours: function (baseUrl, startDate, endDate, userId){

    //chartjs
    $.post('/data/get_user_hours_by_category_ajax',{start_date: startDate, end_date: endDate, user_id: userId})

    .then(function(data){

      //parse data
      var userHoursInformation = JSON.parse(data)

      var hoursData = userHoursInformation.hoursData

      if (userHoursInformation.jobCosts) {
        var jobCosts = userHoursInformation.jobCosts
      }

      if (hoursData.length > 0) {

        //create arrays of categories and data
        var category = []
        var hours = []
        //create variable to hold hours and job costs
        var hoursDescription

        $.each(hoursData, function( index, value ) {

          hoursDescription = null

          //filter out zero hours
          //add total hours and cost based on salary
          if (hoursData[index].category != 'zero') {

            hoursDescription = hoursData[index].hours

            if (jobCosts) {
              hoursDescription += ' | $' + jobCosts[hoursData[index].category]
            }

            category.push([hoursData[index].category, hoursDescription])
            hours.push(hoursData[index].hours)
          }

        });

        //get colors for dataset
        var colorPalette = TimesheetApp.Utilities.returnColorPallette(hours.length)

        //chart js function
        var ctx = document.getElementById('myPieChart');

        var myPieChart = new Chart(ctx, {
          type: 'horizontalBar',
          data: {
            labels: category,
            datasets: [{
              data: hours,
              backgroundColor: colorPalette,
              borderColor: 'rgb(128, 128, 128)'
            }],
          },
          options: {
            tooltips: {
              enabled: false
            },
            maintainAspectRatio: false,
            legend: {
              display: false,
              position: 'bottom'
            },
            layout: {
              padding: {
                left: 0,
                right: 50, //to prevent percentage from not displaying
                top: 0,
                bottom: 0
              }
            },
            plugins: {
              // Change options for ALL labels of THIS CHART
              datalabels: {
                formatter: (value, ctx) => {
                  let sum = 0;
                  let dataArr = ctx.chart.data.datasets[0].data;
                  dataArr.map(data => {
                      sum += data;
                  });
                  let percentage = (value*100 / sum).toFixed(2)+"%";
                  return percentage;
                },
                anchor: 'end',
                align: 'end'
              }
            }
          }

        })

      }

    })

  },

  /**
	 * Displays the modal for adding new users
	 *
	 */
  displayAddJobModal: function (){

    $('a#add-job').on('click', function (e) {

      // display modal
      $('#addJobModal').modal('show')

    })

  },

  /**
	 * Displays the modal for adding new users
	 *
	 */
  displayAddUserModal: function (){

    $('a#add-user').on('click', function (e) {

      // display modal
      $('#addUserModal').modal('show')

    })

  },

  /**
   * Displays the modal for inserting/updating user pay information
   *
   */
  displayPayInformationModal: function (){

    $('a#pay-info-link').on('click', function(){

			// display modal
	  	$('#payInformationModal').modal('show')

		})

  },

  /**
   * Displays the modal for adding hours in the supervisor portal
   *
   */
  displaySupervisorAddHoursModal: function () {
    // govern on click of hours edit
    $('a.add-hours').on('click', function (e) {
      var dataAttributes = $(this).data()



      // fill in modal
      $('#addHoursTitle').html('Add Hours <strong>' + dataAttributes.dateToAdd + '</strong>')
      $('#user_id_add_modal').val(dataAttributes.userId)

      $('#add_hours_date_add_modal').val(dataAttributes.dateToAdd)

      // display modal
      $('#addHoursModal').modal('show')
    })

  },

  /**
   * Displays the modal for editing hours on a user's timesheet
   *
   */
  displaySupervisorEditHoursModal: function () {

    //govern on click of edit hours link
    $('#unapprovedHoursModal .modal-body #editHoursButton').on('click', 'a', function(){

      //hide manage hours modal
      $('#unapprovedHoursModal').modal('hide')

      //fill out hours edit modal
      var dataAttributes = $(this).data()

      // fill in modal
      $('#hoursEditModalLabel').html('<h4 class="modal-title"><span class="badge badge-secondary">' + dataAttributes.name + '</span></h4><h5 class="modal-title">Edit <strong>' + dataAttributes.category + '</strong> Hours ' + dataAttributes.dateToEdit + '</h5>')

      $('#hours_date_edit_modal').val(dataAttributes.dateToEdit)
      $('#name_edit_modal').val(dataAttributes.name)
      $('#user_id_edit_modal').val(dataAttributes.userId)
      $('#category_edit_modal').val(dataAttributes.category)
      $('#hours_edit_modal').val(dataAttributes.hours)
      $('#cleanroom_hours_edit_modal').val(dataAttributes.cleanroomHours)
      $('#description_edit_modal').val(dataAttributes.description)

      //show edit hours modal
      $('#hoursEditModal').modal('show')

    })

  },

  /**
   * Displays the modal for managing hours in a user's timesheet
   *
   */
  displaySupervisorManageHoursModal: function () {

    $('a.manage-hours').on('click', function (e) {
			//get hours data
  		var dataAttributes = $(this).data()

      //check if hours have been approved
      var approvalCheck = $(this).hasClass("approved")

      //empty existing buttons
      $('#unapprovedHoursModal .modal-body #editHoursButton').empty()
      $('#unapprovedHoursModal .modal-body #approveHoursButton').empty()
			$('#unapprovedHoursModal .modal-body #discardHoursButton').empty()

			//generate modal body html
			var unapprovedHoursHtml = "<h5><strong>" + dataAttributes.category + "</strong></h5><div><strong>Hours:</strong> " + dataAttributes.hours + "</div><div><strong>Cleanroom Hours:</strong> " + dataAttributes.cleanroomHours + "</div><div><strong>Description:</strong> " + dataAttributes.description + "</div>"

			// fill in modal
			//header
      $('#unapprovedHoursModalLabel').html('<strong>' + dataAttributes.dateToEdit + '</strong> | ' + dataAttributes.name)
			//body
			$('#unapprovedHoursModal .modal-body #hoursInfo').html(unapprovedHoursHtml)
			//buttons
      $('#unapprovedHoursModal .modal-body #editHoursButton').html('<a id="editHoursLink" href="javascript:void(0);" data-date-to-edit="' + dataAttributes.dateToEdit + '" data-name="' + dataAttributes.name + '" data-user-id="' + dataAttributes.userId + '" data-hours="' + dataAttributes.hours + '" data-cleanroom-hours="' + dataAttributes.cleanroomHours + '" data-category="' + dataAttributes.category + '" data-description="' + dataAttributes.description + '" class="btn btn-warning btn-block">Edit Hours</a>')
      //write approval link if hours are not yet approved
      if (approvalCheck == false) {
          $('#unapprovedHoursModal .modal-body #approveHoursButton').html('<a href="/approve_hours/confirm_approve_hours/' + dataAttributes.hoursId + '" class="btn btn-success btn-block">Approve Hours</a>')
      }
			$('#unapprovedHoursModal .modal-body #discardHoursButton').html('<a href="/approve_hours/discard_hours/' + dataAttributes.hoursId + '" class="btn btn-danger btn-block">Discard Hours</a>')

			// display modal
      $('#unapprovedHoursModal').modal('show')

		})

  },

  /**
   * Draws a chart complete with job costs for user timesheets in the admin panel
   *
   */
  drawApexChartJobCosts: function (chartType, targetDivId, chartData, labels, jobCosts) {

    let legendFormatter

    //determine if job costs should be included in the legend
    if (jobCosts) {
      legendFormatter = function(seriesName, opts) {
          return [seriesName, ": ", opts.w.globals.series[opts.seriesIndex], ' | ', '$' + jobCosts[seriesName]]
      }
    } else {
      legendFormatter = function(seriesName, opts) {
          return [seriesName, ": ", opts.w.globals.series[opts.seriesIndex]]
      }
    }

    let options = {
      chart: {
        type: chartType
      },
      series: chartData,
      labels: labels,
      theme: {
          mode: 'light',
          palette: 'palette2',
      },
      dataLabels: {
        enabled: true,
        style: {
          colors: ['#000']
        },
        formatter: function (value, opts)
        {
          return [opts.w.globals.labels[opts.seriesIndex], value.toFixed(2) + "%"]
        }
      },
      legend: {
        /*
         * @param {string} seriesName - The name of the series corresponding to the legend
         * @param {object} opts - Contains additional information as below
         * opts: {
         *   seriesIndex
         *   w: {
         *     config,
         *     globals
         *   },
         * }
        */
        // formatter: function(seriesName, opts) {
        //     return [seriesName, ": ", opts.w.globals.series[opts.seriesIndex], ' | ', '$' + jobCosts[seriesName]]
        // }
        formatter: legendFormatter
      }
    }

    var chart = new ApexCharts(document.querySelector("#" + targetDivId), options);

    chart.render();

  },

  /**
   * Submit add user data
   *
   */
  submitAddJobModalForm: function (baseUrl) {

    // on submit click
    $('form#add-job-form').on('submit', function (e) {
			// stop the form from submitting
      e.preventDefault()

			// get values from form
      var addJob = $('form#add-job-form').serializeArray()

      $.post(baseUrl + 'settings/add_job_ajax', addJob)

      .then(function (data) {

        console.log(data)

        var response = JSON.parse(data)

        //on success reload the page
        if (response['success']) {
		  		window.location.reload(true)
        }

        //display any errors to the user
        if (response['errors']) {
          alert(response['errors'])
        }

      })

    })

  },

  /**
   * Submit add user data
   *
   */
  submitAddUserModalForm: function (baseUrl) {

    // on submit click
    $('form#add-user-form').on('submit', function (e) {
			// stop the form from submitting
      e.preventDefault()

			// get values from form
      var addUser = $('form#add-user-form').serializeArray()

      $.post(baseUrl + 'manage_users/register_user_ajax', addUser)

      .then(function (data) {

        console.log(data)

        var response = JSON.parse(data)

        //on success reload the page
        if (response['success']) {
		  		window.location.reload(true)
        }

        //display any errors to the user
        if (response['errors']) {
          alert(response['errors'])
        }

      })

    })

  },

  /**
	 * Submits Pay information modal form and handles response
   * @param baseUrl
	 *
	 */
  submitPayInformationForm: function(baseUrl){

    //on modal form submit
		$('a#pay-information-submit-modal').on('click', function(){

		  // get new values from form
		  var payInfo = $('#pay-information-form').serializeArray()

			// send to controller
		  $.post(baseUrl + 'manage_users/update_user_pay_information_ajax', payInfo)

			.then(function (data) {

		    var response = JSON.parse(data)

		    if (response['success']) {
					//refresh the page
		      location.reload()
		    }

		    if (response['errors']) {
		      alert(response['error'])
		    }

		  })

		})

  },

  /**
	 * Submits Add Hours modal form and handles response
   * @param
	 *
	 */
  submitSupervisorAddHoursModalForm: function (baseUrl) {
		// on submit of edit form
    $('form#add_hours_form').on('submit', function (e) {
			// stop the form from submitting
      e.preventDefault()

			// get values from form
      var addHours = $('#add_hours_form').serializeArray()

			// initialize data object
      dataObj = {}

			// create object from serialized array
      $(addHours).each(function (i, field) {
			  	dataObj[field.name] = field.value
      })

      //assign values to hours and cleanroom hours if "zero" category selected
      if (dataObj.category == 'zero') {
        dataObj.hours = 0
        dataObj.cleanroom_hours = 0
      }

			// send to controller
      $.post(baseUrl + 'manage_timesheets/validate_store_ajax', addHours)

			.then(function (data) {

        console.log(data)
        var response = JSON.parse(data)


        if (response['success']) {
      		// success message above form
          $('#add-success').html('<div class="alert alert-success alert-dismissible fade show" role="alert"><strong>Hours Added!</strong><div><strong>' + dataObj.category + '</strong> Hours: ' + dataObj.hours + ' | cleanroom: ' + dataObj.cleanroom_hours + '</div><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>')

      		// clear form fields for re-submission
          $('form#add_hours_form #category').val('')
          $('form#add_hours_form #hours').val('')
          $('form#add_hours_form #cleanroom_hours').val('')
          $('form#add_hours_form #description').val('')

      		// bind on close event that refreshes the page
          $('#addHoursModal').on('hidden.bs.modal', function () {
      			  			window.location.reload(true)
      			  		})
        }

        if (response['errors']) {
          alert(response['error'])

          $('#addHoursModal').modal('hide')
        }

      })
    })
  },

  /**
	 * Submits Edit Hours modal form and handles response
   *
	 * @param    baseURL   input string
	 */
  submitSupervisorEditHoursModalForm: function (baseUrl) {
		// on submit of edit form
    $('#hours-edit-submit-modal').on('click', function (e) {
			// get new values from form
      var newHours = $('#hours-edit-form').serializeArray()

			// send to controller
      $.post(baseUrl + 'manage_timesheets/validate_store_ajax', newHours)

			.then(function (data) {
        var response = JSON.parse(data)

        if (response['success']) {
          location.reload()
        }

        if (response['errors']) {
          alert(response['error'])

          $('#hoursEditModal').modal('hide')
        }
      })
    })
  },

  /**
	 * updates a user's access level
	 *
	 */
  updateUserAccessLevel: function () {
    $('.accessLevelSwitch').change(function(){

      //assign value of switch
      var checked = $(this).prop('checked')

      //get the user id
      var userId = $('#userId').val()

      //passin global variable declared in view files
      //prevent the active user from making changes
      if (!TimesheetApp.Utilities.denyActiveUserEdit(loggedInUserId, userId))
      {
        //undo user chnage on radio button
        if ($(this).prop('checked') == true) {
          $(this).prop('checked', false)
        }

        if ($(this).prop('checked') == false) {
          $(this).prop('checked', true)
        }

        return false
      }
      else
      {
        //get the access category
        var accessCategory = $(this).data('accessCategory')

        //update user access level
        $.post(baseUrl + 'manage_users/update_user_group_ajax', {checked: checked, access_category: accessCategory, user_id: userId})

        .then(function(responseData){

          //converst JSON to obj
          responseData = JSON.parse(responseData)

          //alert user in event of an error
          if(responseData.error){
            alert(responseData.error)
          }

        })

      }

    })

  },

  /**
	 * updates a user's active status
	 *
	 */
  updateJobStatus: function () {

		// on click update user status
		$('.update-job-status-link').on('click', function(){

			//get data attributes associated with link
			var jobData = $(this).data()

      //declare variable to hold new active status
			var newJobStatus

			//popup modal to confirm
      if(jobData.currentStatus == "active"){
      	$('div#activeConfirmModal #modal-body').html('You are about to make this job <span class="text-danger">INACTIVE</span>. Do you want to proceed?');
      	newJobStatus = 0
      } else if(jobData.currentStatus == "inactive"){
      	$('div#activeConfirmModal #modal-body').html('Warning: You are about to <span class="text-success">ACTIVATE</span> this job! Do you want to proceed?')
      	newJobStatus = 1
      }

      //display modal to user
			$("#activeConfirmModal").modal('show')

      //bind on click event to submit button
			$('#activeConfirmModal .active-status-submit').on('click', function(e){

				e.preventDefault()

        //make post call to update user status
				$.post(baseUrl + 'settings/update_job_status_ajax', {id: jobData.jobId, is_active: newJobStatus}, function(data){

					//on success close modal
					$('#activeConfirmModal').modal('hide')

					//refresh page
					window.location.reload(true)

				}).fail(function(data) {

					alert('There was a problem with your request. Please contact a web administrator!')
					console.log(data)
					return false

				})

			})

		})

  },

  /**
	 * updates a user's active status
	 *
	 */
  updateUserStatus: function (loggedInUserId) {

		// on click update user status
		$('.update-status-link').on('click', function(){

			//get data attributes associated with link
			var userData = $(this).data()

      //pass in global variable declared in view files
      if (!TimesheetApp.Utilities.denyActiveUserEdit(loggedInUserId, userData.userId))
      {
        return false
      }
      else
      {

        //declare variable to hold new active status
  			var newActiveStatus

  			//popup modal to confirm
        if(userData.currentStatus == "active"){
        	$('div#activeConfirmModal #modal-body').html('You are about to make this user <span class="text-danger">INACTIVE</span>. Do you want to proceed?');
        	newActiveStatus = 0
        } else if(userData.currentStatus == "inactive"){
        	$('div#activeConfirmModal #modal-body').html('Warning: You are about to <span class="text-success">ACTIVATE</span> this user! Do you want to proceed?')
        	newActiveStatus = 1
        }

        //display modal to user
  			$("#activeConfirmModal").modal('show')

        //bind on click event to submit button
  			$('#activeConfirmModal .active-status-submit').on('click', function(e){

  				e.preventDefault()

          //make post call to update user status
  				$.post(baseUrl + 'manage_users/update_active_status_ajax', {user_id: userData.userId, active: newActiveStatus}, function(data){

  					//on success close modal
  					$('#activeConfirmModal').modal('hide')

  					//refresh page
  					window.location.reload(true)

  				}).fail(function(data) {

  					alert('There was a problem with your request. Please contact a web administrator!')
  					console.log(data)
  					return false

  				})

  			})

      }

		})

  }

}

// Hours functions
TimesheetApp.Hours = {

	/**
	 * Displays the modal for adding hours
	 *
	 */
  displayAddHoursModal: function () {
		// govern on click of hours edit
    $('a.add-hours').on('click', function (e) {
      var dataAttributes = $(this).data()

			// fill in modal
      $('#addHoursModalLabel').html('Add Hours <strong>' + dataAttributes.dateToAdd + '</strong>')
      $('#add_hours_date_modal').val(dataAttributes.dateToAdd)

			// display modal
      $('#addHoursModal').modal('show')
    })

		// on close, clear out existing hours div
  },

	/**
	 * Displays the modal for editing hours
	 *
	 */
  displayEditHoursModal: function () {
		// govern on click of hours edit
    $('a.hours-edit').on('click', function (e) {
      var dataAttributes = $(this).data()

			// fill in modal
      $('#hoursEditModalLabel').html('Edit <strong>' + dataAttributes.category + '</strong> Hours ' + dataAttributes.dateToEdit)
      $('#hours_date_modal').val(dataAttributes.dateToEdit)
      $('#category_modal').val(dataAttributes.category)
      $('#hours_modal').val(dataAttributes.hours)
      $('#cleanroom_hours_modal').val(dataAttributes.cleanroomHours)
      $('#description_modal').val(dataAttributes.description)

      //update discard link with id of hours
      $("a#discard-hours").attr("href", "/hours/discard_hours/" + dataAttributes.hoursId)

			// display modal
      $('#hoursEditModal').modal('show')
    })
  },

	/**
	 * Submits modal form and handles response
   * @param
	 *
	 */
  submitAddHoursModalForm: function (baseUrl) {
		// on submit of edit form
    $('form#add_hours_form').on('submit', function (e) {
			// stop the form from submitting
      e.preventDefault()

			// get values from form
      var addHours = $('#add_hours_form').serializeArray()

			// initialize data object
      dataObj = {}

			// create object from serialized array
      $(addHours).each(function (i, field) {
			  	dataObj[field.name] = field.value
      })

      //assign values to hours and cleanroom hours if "zero" category selected
      if (dataObj.category == 'zero') {
        dataObj.hours = 0
        dataObj.cleanroom_hours = 0
      }

			// send to controller
      $.post(baseUrl + 'hours/validate_store_ajax', addHours)

			.then(function (data) {
        var response = JSON.parse(data)

        if (response['success']) {
      		// success message above form
          $('#add-success').html('<div class="alert alert-success alert-dismissible fade show" role="alert"><strong>Hours Added!</strong><div><strong>' + dataObj.category + '</strong> Hours: ' + dataObj.hours + ' | cleanroom: ' + dataObj.cleanroom_hours + '</div><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>')

      		// clear form fields for re-submission
          $('form#add_hours_form #category').val('')
          $('form#add_hours_form #hours').val('')
          $('form#add_hours_form #cleanroom_hours').val('')
          $('form#add_hours_form #description').val('')

      		// bind on close event that refreshes the page
          $('#addHoursModal').on('hidden.bs.modal', function () {
      			  			window.location.reload(true)
      			  		})
        }

        if (response['errors']) {
          alert(response['error'])

          $('#addHoursModal').modal('hide')
        }

      })
    })
  },

	/**
	 * Submits modal form and handles response
   *
	 * @param    baseURL   input string
	 */
  submitEditHoursModalForm: function (baseUrl) {
		// on submit of edit form
    $('#hours-edit-submit-modal').on('click', function (e) {
			// get new values from form
      var newHours = $('#hours-edit-form').serializeArray()

			// send to controller
      $.post(baseUrl + 'hours/validate_store_ajax', newHours)

			.then(function (data) {
        var response = JSON.parse(data)

        if (response['success']) {
          location.reload()
        }

        if (response['errors']) {
          alert(response['error'])

          $('#hoursEditModal').modal('hide')
        }
      })
    })
  },

	/**
	 * Takes an element identifier and display or hides the save changes
	 *
	 * @param    modalIdentifier   Input string
	 */
  toggleModalSaveChangesButton: function (modalIdentifier) {
		// display the save changes button anytime a modal input is updated
    $(modalIdentifier).on('input', '.modal-body input, .modal-body select, .modal-body textarea', function (e) {
		    $('.save-changes').fadeIn(1600)
    })
		// hide the save changes button when any modal is closed, to prep for the next modal to be opened
    $(modalIdentifier).on('hide.bs.modal', function () {
      $('.save-changes').hide()
    })
  },

  /**
	 * bind event to hours selector
   * Zero out should set hours to zero and disable inputs
   * any other selection should clear hours and enable inputs
	 *
	 * @param    modalIdentifier   Input string
	 */
  zeroOutFormHours: function () {

		$('select#category').change(function(){

			if ($(this).val() == 'zero') {

				$('input#hours').attr('value', 0).attr("disabled", true)
				$('input#cleanroom_hours').attr('value', 0).attr("disabled", true)

			} else {

				$('input#hours').val('').attr("disabled", false)
				$('input#cleanroom_hours').val('').attr("disabled", false)

			}

		})

  }

}

// Utility functions
TimesheetApp.Utilities = {

  denyActiveUserEdit: function(loggedInUserId, userToEdit) {
    //check if selected user is logged in
    if (loggedInUserId == userToEdit)
    {
      alert('Warning! You cannot alter settings for your own account! Please contact the Web Administrator if you need to perform this action!')

      return false

    }
    else
    {
      return true
    }
  },

  /**
   * Draws a chart of job hours
   *
   */
  drawApexChart: function (chartType, targetDivId, chartData, labels) {

    var options = {
      chart: {
        type: chartType
      },
      series: chartData,
      labels: labels,
      theme: {
          mode: 'light',
          palette: 'palette2',
      },
      dataLabels: {
        enabled: true,
        style: {
          colors: ['#000']
        },
        formatter: function (value, opts)
        {
          return [opts.w.globals.labels[opts.seriesIndex], value.toFixed(2) + "%"]
        }
      },
      legend: {
        /*
         * @param {string} seriesName - The name of the series corresponding to the legend
         * @param {object} opts - Contains additional information as below
         * opts: {
         *   seriesIndex
         *   w: {
         *     config,
         *     globals
         *   },
         * }
        */
        formatter: function(seriesName, opts) {
            return [seriesName, ": ", opts.w.globals.series[opts.seriesIndex]]
        }
      }
    }

    var chart = new ApexCharts(document.querySelector("#" + targetDivId), options);

    chart.render();

  },

  /**
	 * Takes an integer (array length) and returns an array of predefined colors
	 *
	 * @param    arrayLength   Input integer
	 * @return   array
	 */
  returnColorPallette: function (arrayLength) {

    var masterPallette = ['#1ba3c6', '#2cb5c0', '#30bcad', '#21B087', '#33a65c', '#57a337', '#a2b627', '#d5bb21', '#f8b620', '#f89217', '#f06719', '#e03426', '#f64971', '#fc719e', '#eb73b3', '#ce69be', '#a26dc2', '#7873c0', '#4f7cba']

    var slicedPallette = masterPallette.slice(0, arrayLength)

    return slicedPallette
  }

}
