<style media="screen">
    ul#accordionSidebar, .topbar {
        display: none;
    }
</style>

    <div class="container">
        <div class="row">

            <div class="col-12">

                <p class="h5"><?=$user->first_name?> <?=$user->last_name?></p>

                <p class="h6"><strong><?=date('M d, Y',strtotime($payperiod_start_date))?> - <?=date('M d, Y',strtotime($payperiod_end_date))?></strong></p>

                <table class="table table-sm">
                    <thead>
                        <tr>
                            <th scope="col">Date</th>
                            <th scope="col">Job</th>
                            <th scope="col">Hours</th>
                            <th scope="col">Cleanroom</th>
                            <th scope="col">Description</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $total_hours = 0;
                            $total_cleanroom_hours = 0;
                        ?>
                        <?php foreach($user_payperiod_data as $date => $data): ?>

                            <?php if($data['hours']):?>


                                      <?php foreach($data['hours'] as $hours):?>
                                           <tr>
                                          <td><?=$date?></td>
                                          <td><?=$hours->category?></td>
                                          <td><?=$hours->hours?></td>
                                          <td><?=$hours->cleanroom_hours?></td>
                                          <td><small><?=$hours->description?></small></td>
                                          </tr>

                                      <?php endforeach; ?>

                                      <?php
                                          $total_hours = $total_hours + $data['stats']['total_hours'];
                                          $total_cleanroom_hours = $total_cleanroom_hours + $data['stats']['total_cleanroom_hours'];
                                      ?>


                            <?php endif; ?>

                        <?php endforeach; ?>

                        <tr>
                            <td></td>
                            <td>Total:</td>
                            <td><?=$total_hours?></td>
                            <td><?=$total_cleanroom_hours?></td>
                        </tr>

                        <tr>
                            <td></td>
                            <td>Total Sick Leave</td>
                            <td><?=(empty($user_payperiod_sick_leave)) ? '0' : $user_payperiod_sick_leave?></td>
                        </tr>

                        <tr>
                            <td></td>
                            <td>Total Vacation/PTO</td>
                            <td><?=(empty($user_payperiod_paid_leave)) ? '0' : $user_payperiod_paid_leave?></td>
                        </tr>



                    </tbody>
                </table>

                <div >
                    Employee Signature ________________________ Date_____________
                </div>
                <div class="mt-3">
                    CEO ________________________ Date_____________
                </div>

            </div>

        </div>


    </div>





<?php //print_r($user_payperiod_data); ?>

<script type="text/javascript">

    //wait for the DOM to load
    $(document).ready(function () {

        //get rid of the background gradient for colorizing
        $('body').removeClass('bg-gradient-primary')

        function fixit(selector) {
            selector.each(function () {
                var values = $(this).find("tr>td:first-of-type")
                var run = 1
                for (var i=values.length-1;i>-1;i--){
                    if ( values.eq(i).text()=== values.eq(i-1).text()){
                        values.eq(i).remove()
                        run++
                    }else{
                        values.eq(i).attr("rowspan",run)
                        run = 1
                    }
                }
            })
        }
        fixit($("table"))

    })

</script>
