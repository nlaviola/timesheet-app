<!-- Admin Label -->
<div class="admin-label my-1">
	<span class="badge badge-info">Admin</span>
</div>

<!-- Page Heading - User Account -->
<div class="d-sm-flex align-items-center justify-content-between mb-2">
	<h1 class="h3 mb-0 text-gray-800"><i class="fas fa-fw fa-user-cog"></i></i> <?=$user->first_name;?> <?=$user->last_name;?></h1>
</div>

<!-- Breadcrumb -->
<nav aria-label="breadcrumb">
	<ol class="breadcrumb bg-transparent mb-0">
	<li class="breadcrumb-item"><a href="/manage_users">Manage Users</a></li>
	<li class="breadcrumb-item active" aria-current="page"><?=$user->first_name;?> <?=$user->last_name;?></li>
	</ol>
</nav>

<!-- Content Row -->
<div class="row">

	<div class="col-12">
		<?php if ($user->active == 0): ?>
			<span class="badge badge-pill badge-danger mb-2">Inactive</span>
		<?php endif; ?>
	</div>

    <!-- View Timesheets Card -->
	<div class="col-xl-3 col-lg-3 mb-2">
		<div class="card shadow h-100 py-2">
            <div class="card-body py-1">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      	<div class="h6 mb-0 font-weight-bold text-center"><a href="/manage_timesheets/display_user_payperiod_list/<?=$user->id;?>">View Timesheets <i class="far fa-fw fa-calendar-alt"></i></i></a></div>
                    </div>
                    <div class="col-auto">

                    </div>
                </div>
            </div>
        </div>
	</div>

	<!-- Edit profile Card -->
	<div class="col-xl-3 col-lg-3 mb-2">
		<div class="card shadow h-100 py-2">
            <div class="card-body py-1">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      	<div class="h6 mb-0 font-weight-bold text-center"><a href="#">Edit User <i class="fas fa-fw fa-edit"></i></a></div>
                    </div>
                </div>
            </div>
        </div>
	</div>



</div>

<!-- Content Row -->
<div class="row">

	<!-- Reset Password Card -->
	<div class="col-xl-3 col-lg-3 mb-2">
		<div class="card shadow h-100 py-2">
            <div class="card-body py-1">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      	<div class="h6 mb-0 font-weight-bold text-center"><a id="password_reset_action" href="/manage_users/reset_user_password/<?=$user->id;?>">Reset Password <i class="fas fa-key"></i></a></div>
                    </div>
                </div>
            </div>
        </div>
	</div>

    <!-- Update Pay Card -->
	<div class="col-xl-3 col-lg-3 mb-2">
		<div class="card shadow h-100 py-2">
            <div class="card-body py-1">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      	<div class="h6 mb-0 font-weight-bold text-center"><a id="pay-info-link" href="javascript:void(0);">Update Pay Info <i class="fas fa-dollar-sign"></i></a></div>
                    </div>
                </div>
            </div>
        </div>
	</div>

</div>

<!-- Content Row -->
<div class="row">

	<!-- password reset URL -->
	<div class="col-xl-6 col-lg-6">

	</div>

</div>

<!-- Content Row -->
<div class="row">

	<!-- Line Break -->
	<div class="col-xl-6 col-lg-6">
		<hr/>
	</div>

</div>

<!-- Content Row -->
<div class="row">

	<div class="col-xl-6 col-lg-6">

		<div id="message">

		<?php if (!empty($this->session->flashdata('message'))): ?>

			<!-- Alert Message -->
			<div class="alert alert-success alert-dismissible fade show" role="alert">
			  	<strong>You did it!</strong>
			  	<div>
			  		<?=$this->session->flashdata('message');?>
			  	</div>
			  	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			    	<span aria-hidden="true">&times;</span>
			  	</button>
			</div>

		<?php endif; ?>

		</div>

		<div id="error">

		<?php if (!empty($this->session->flashdata('error'))): ?>

			<!-- Alert Message -->
			<div class="alert alert-danger alert-dismissible fade show" role="alert">
			  	<strong>Uh Oh!</strong>
			  	<div>
			  		<?=$this->session->flashdata('error');?>
			  	</div>
			  	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			    	<span aria-hidden="true">&times;</span>
			  	</button>
			</div>

		<?php endif; ?>

		</div>

	</div>


</div>

<!-- Content Row -->
<div class="row">

	<div class="col-xl-6 col-lg-6">

		<?php if(isset($password_reset_status)): ?>

			<?php if($password_reset_status == 'active'): ?>
				<div class="alert alert-info fade show" role="alert">
					<div>
						<strong>Password Reset Initiated!</strong>
					</div>
					<div>
						<small>This is a one time code that will no longer be available if you leave the page</small>
					</div>
					<div>
						 <textarea class="form-control" rows="5"><?=$password_reset_link;?></textarea>
					</div>
					<small>Copy link text and email to user</small>
				</div>
			<?php endif; ?>

			<?php if($password_reset_status == 'expired'): ?>
				<div class="alert alert-warning fade show" role="alert">
					<strong>Password Reset Code Out of Date!</strong>
					<div>
						This user's password has been reset, but the password code has expired. Please generate a new password reset code and email the link to the user!
					</div>
				</div>
			<?php endif; ?>

			<?php if($password_reset_status == 'invalid'): ?>
				<div class="alert alert-danger fade show" role="alert">
					<strong>Password Reset Code Invalid!</strong>
					<div>
						This user's password has been reset, but the password code is now invalid. Please generate a new password reset code and email the link to the user!
					</div>
				</div>
			<?php endif; ?>

		<?php endif; ?>
	</div>

</div>

<!-- Content Row -->
<div class="row">

	<div class="col-xl-3 col-md-6 mb-4">
        <div class="card shadow h-100 py-1">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      	<div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Pay Type</div>
                      	<div class="h5 mb-0 font-weight-bold text-gray-800"><?=(isset($pay_type) ? $pay_type : '<span class="text-warning">Not Set</span>')?></div>
                    </div>
                    <div class="col-auto">
                      	<i class="fas fa-certificate fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

	<div class="col-xl-3 col-md-6 mb-4">
        <div class="card shadow h-100 py-1">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      	<div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Pay Rate</div>
                      	<div class="h5 mb-0 font-weight-bold text-gray-800"><?=(isset($rate) ? $rate : '<span class="text-warning">Empty</span>')?> <?=(isset($pay_type) && $pay_type == 'hourly' ? '/hr' : '')?></div>
                    </div>
                    <div class="col-auto">
                      	<i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<!-- Content Row -->
<div class="row">

    <div class="col-xl-6 col-lg-6">

        <!-- Access Level Card -->
      	<div class="card shadow mb-4">

            <!-- Card Header - Dropdown -->
	        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
	          	<h6 class="m-0 font-weight-bold text-secondary">User Access</h6>
	        </div>

	        <!-- Card Body -->
	        <div class="card-body">

                <input type="hidden" id="userId" name="userId" value="<?=$user->id;?>">

                <div class="custom-control custom-switch">
                    <input type="checkbox" class="custom-control-input accessLevelSwitch" data-access-category="admin" id="adminSwitch" <?=($this->ion_auth->is_admin($user->id))? 'checked' : '';?>>
                    <label class="custom-control-label" for="adminSwitch">Admin Group</label>
                </div>

                <div>
                    <p>Admins have access to all site functions</p>
                </div>

                <div class="custom-control custom-switch">
                    <input type="checkbox" class="custom-control-input accessLevelSwitch" data-access-category="supervisor" id="supervisorSwitch" <?=($this->ion_auth->in_group('supervisor', $user->id))? 'checked' : '';?>>
                    <label class="custom-control-label" for="supervisorSwitch">Supervisor Group</label>
                </div>

                <div>
                    <p>Supervisors can approve user hours</p>
                </div>

            </div>

        </div>

    </div>

</div>

<!-- Dashboard Pay Information Modal-->
<div class="modal fade" id="payInformationModal" tabindex="-1" role="dialog" aria-labelledby="payInformationModalLabel" aria-hidden="true">

	<div class="modal-dialog" role="document">

	  	<div class="modal-content">

		   	<div class="modal-header">
				<h5 class="modal-title" id="payInformationModalLabel"><?=$user->first_name;?> <?=$user->last_name;?> | Update Pay Information</h5>
				<button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
		    </div>

		    <div class="modal-body">

		    	<form id="pay-information-form">

			    	<input id="user_id_modal" name="user_id" type="hidden" value="<?=$user->id;?>">

	          		<div class="form-group">
	          			<label for="pay_type">Pay Type</label>
						<select class="form-control custom-select" id="pay_type" name="pay_type" required>
				    		<option value="">Select a Pay Type</option>
							<option value="salary" <?=(isset($pay_type) && $pay_type == 'salary') ? 'selected' : ''?>>Salary</option>
							<option value="hourly" <?=(isset($pay_type) && $pay_type == 'hourly') ? 'selected' : ''?>>Hourly</option>
						</select>
	          		</div>

	          		<div class="form-group">
	          			<label for="rate">Rate</label>
		    			<input type="text" class="form-control" id="rate_modal" name="rate" value="<?=(isset($rate)) ? $rate : ''?>" required>
						<small><strong>Yearly Salary</strong> or <strong>Hourly Rate</strong></small>
	          		</div>

	          	</form>

			</div>

		    <div class="modal-footer">

		      	<button class="btn btn-secondary " type="button" data-dismiss="modal">Cancel</button>

		      	<a id="pay-information-submit-modal" class="btn btn-primary btn-icon-split save-changes" href="javascript:void(0);" style="display:none;">
		      		<span class="icon text-white-50">
	                  	<i class="fas fa-arrow-right"></i>
	                </span>
	                <span class="text">Save Changes</span>
		      	</a>

		    </div>

		</div>

	</div>

</div>

<script type="text/javascript">
    /*** GLOBAL Variables ***/

    //get base_url for use in AJAX calls
    var baseUrl = <?=json_encode(base_url());?>;

    //get the current logged in user
    var loggedInUserId = <?=$this->ion_auth->user()->row()->id;?>;

    //wait for the DOM to load
    $(document).ready(function () {

        TimesheetApp.Admin.updateUserAccessLevel()

		TimesheetApp.Hours.toggleModalSaveChangesButton('#payInformationModal')

		TimesheetApp.Admin.displayPayInformationModal()

		TimesheetApp.Admin.submitPayInformationForm(baseUrl)

		$('a#password_reset_action').on('click', function(e){

			//get the user id
      		var userId = $('#userId').val()

			//verify that the logged in user is not reseting their own password
			if (!TimesheetApp.Utilities.denyActiveUserEdit(loggedInUserId, userId)) {
				e.preventDefault()
			} else {

				var r = confirm("You are about to reset the password for this user! Do you wish to continue?")
				if (r == false) {
				  e.preventDefault()
				}

			}



		})

    });
</script>
