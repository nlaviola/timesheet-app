<!-- Admin Label -->
<div class="admin-label my-1">
	<span class="badge badge-info">Admin</span>
</div>

<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
	<h1 class="h3 mb-0 text-gray-800"><i class="far fa-fw fa-calendar-alt"></i> <?=$year?> Data - <?=$user->first_name?> <?=$user->last_name?></h1>
</div>

<!-- Breadcrumb -->
<nav aria-label="breadcrumb">
	<ol class="breadcrumb bg-transparent mb-0">
		<li class="breadcrumb-item"><a href="/manage_users">Manage Users</a></li>
	    <li class="breadcrumb-item"><a href="/manage_users/display_user_account/<?=$user->id?>"><?=$user->first_name?> <?=$user->last_name?></a></li>
		<li class="breadcrumb-item"><a href="/manage_timesheets/display_user_payperiod_list/<?=$user->id?>">Pay Period Links</a></li>
        <li class="breadcrumb-item active" aria-current="page"><?=$year?></li>
	</ol>
</nav>

<!-- Content Row -->
<div class="row justify-content-center">
	<div class="col-xl-9">
		<h1 class="h5 m-2 text-gray-800 text-center font-weight-bold"><?=$year?> Yearly Hours - <?=$user->first_name?> <?=$user->last_name?></h1>
		<div id="user-yearly-hours-chart" class="px-3"></div>
	</div>
	<div class="col-xl-9">
		<h1 class="h5 m-2 text-gray-800 text-center font-weight-bold"><?=$year?> Yearly Cleanroom Hours - <?=$user->first_name?> <?=$user->last_name?></h1>
		<div id="user-yearly-cleanroom-hours-chart" class="px-3"></div>
	</div>
</div>

<script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>

<!-- Page specific JS -->
<script type="text/javascript">

	/*** GLOBAL Variables ***/

	//get base_url for use in AJAX calls
	let baseUrl = <?php echo json_encode(base_url()); ?>;
	//get the year
    let userId = <?php echo json_encode($user->id); ?>;
	let year = <?php echo json_encode($year); ?>;
    let startDate = <?php echo json_encode(date($year . '-01-01')); ?>;
	let endDate = <?php echo json_encode(date($year . '-12-31')); ?>;

	//wait for the DOM to load
	$(document).ready(function () {

        //query for payperiods in that year
        $.post(baseUrl + 'data/get_user_payperiod_hours_ajax/' + startDate + '/' + endDate + '/' + userId)

        .then(function(data){

            //parse data
            let userHoursInformation = JSON.parse(data)

            //initialize variables
            let hoursChartData = []
            let hoursLabels = []
            let cleanroomHoursChartData = []
            let cleanroomHoursLabels = []

            //evaluate hours for pay period
            if (userHoursInformation.user_hours.length > 0) {

                //build arrays for chart data
                $.each(userHoursInformation.user_hours, function(key, value){

                    hoursChartData.push(value.hours)
                    hoursLabels.push(value.category)

                })

                //call function to draw charts
                TimesheetApp.Utilities.drawApexChart('pie', 'user-yearly-hours-chart', hoursChartData, hoursLabels)

            } else {
                $('#user-yearly-hours-chart').html('<p class="text-center">No recorded hours</p>')
            }

            if (userHoursInformation.user_cleanroom_hours.length > 0) {

                //build arrays for chart data
                $.each(userHoursInformation.user_cleanroom_hours, function(key, value){

                    cleanroomHoursChartData.push(value.cleanroom_hours)
                    cleanroomHoursLabels.push(value.category)

                })

                //call function to draw charts
                TimesheetApp.Utilities.drawApexChart('pie', 'user-yearly-cleanroom-hours-chart', cleanroomHoursChartData, cleanroomHoursLabels)

            } else {
                $('#user-yearly-cleanroom-hours-chart').html('<p class="text-center">No recorded cleanroom hours</p>')
            }

        })

    })

</script>
