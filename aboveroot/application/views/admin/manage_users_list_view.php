<!-- Admin Label -->
<div class="admin-label my-1">
	<span class="badge badge-info">Admin</span>
</div>

<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
	<h1 class="h3 mb-0 text-gray-800"><i class="fas fa-fw fa-users-cog"></i> Manage Users</h1>
</div>

<div class="row">
	<div class="col-12">
		<div class="my-2">
			<a id="add-user" href="javascript:void(0)"><i class="far fa-fw fa-plus-square fa-lg"></i> Add New User</a>
		</div>
	</div>
	<div class="col-12">
		<div id="user-message">

		<?php if (!empty($this->session->flashdata('message'))): ?>

			<!-- User Alert -->
			<div class="alert alert-success alert-dismissible fade show" role="alert">
			  	<strong>User successfully added!</strong>
			  	<div>
			  		<?=$this->session->flashdata('message');?>
			  	</div>
			  	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			    	<span aria-hidden="true">&times;</span>
			  	</button>
			</div>

		<?php endif; ?>

		</div>
	</div>
</div>

<!-- Active User List Table -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-success">Active Users</h6>
    </div>
    <div class="card-body">

		<div class="table-responsive-md">
			<table class="table table-borderless table-hover">
	            <thead>
	                <tr>
	                    <th scope="col">User</th>
	                    <th scope="col">Last login</th>
	                    <th scope="col">Status</th>
	                    <th scope="col">Access Level</th>
						<th></th>
						<th scope="col">Actions</th>
	                </tr>
	            </thead>
	            <tbody>

				<?php foreach($active_users as $info): ?>

	                <tr>
	                    <td><a href="/manage_users/display_user_account/<?=$info->id;?>"><?=$info->first_name;?> <?=$info->last_name;?></a></td>
	                    <td><?=date('M dS, Y', $info->last_login);?></td>
	                    <td>Active</td>
	                    <td><?=($this->ion_auth->in_group('members', $info->id)) ? 'Employee' : '';?> <?=($this->ion_auth->in_group('admin', $info->id)) ? '<strong>Admin</strong>' : '';?> <?=($this->ion_auth->in_group('supervisor', $info->id)) ? '<strong>Supervisor</strong>' : '';?></td>
						<td> <a href="/manage_timesheets/display_user_payperiod_list/<?=$info->id;?>">View Timesheets</a> </td>
						<td>

							<div class="dropdown no-arrow mb-4">
			                    <a class="dropdown-toggle text-primary" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="javascript:void(0)">
			                      <i class="fas fa-fw fa-ellipsis-h"></i>
							  	</a>
			                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
			                      	<a class="dropdown-item text-danger update-status-link" data-current-status='active' data-user-id="<?=$info->id;?>" href="javascript:void(0)"><i class="fas fa-fw fa-user-slash"></i> Inactivate User</a>
			                    </div>
			                </div>

						</td>
	                </tr>

				<?php endforeach; ?>



	            </tbody>
	        </table>
		</div>

    </div>
</div>

<!-- Inactive User List Table -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-danger">Inactive Users</h6>
    </div>
    <div class="card-body">

		<div class="table-responsive-md">
			<table class="table table-borderless table-hover">
	            <thead>
	                <tr>
	                    <th scope="col">User</th>
	                    <th scope="col">Last login</th>
	                    <th scope="col">Status</th>
	                    <th scope="col">Access Level</th>
						<th scope="col">Actions</th>
	                </tr>
	            </thead>
	            <tbody>

				<?php foreach($inactive_users as $info): ?>

	                <tr>
	                    <td><a href="/manage_users/display_user_account/<?=$info->id;?>"><?=$info->first_name;?> <?=$info->last_name;?></a></td>
	                    <td><?=date('M dS, Y', $info->last_login);?></td>
	                    <td>Inactive</td>
	                    <td>
							<?=($this->ion_auth->in_group('members', $info->id)) ? 'Employee' : '';?> <?=($this->ion_auth->in_group('admin', $info->id)) ? '<strong>Admin</strong>' : '';?> <?=($this->ion_auth->in_group('supervisor', $info->id)) ? '<strong>Supervisor</strong>' : '';?>
						</td>
						<td>

							<div class="dropdown no-arrow mb-4">
			                    <a class="dropdown-toggle text-primary" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="javascript:void(0)">
			                      <i class="fas fa-fw fa-ellipsis-h"></i>
							  	</a>
			                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
			                      	<a class="dropdown-item text-success update-status-link" data-current-status='inactive' data-user-id="<?=$info->id;?>" href="javascript:void(0)"><i class="fas fa-fw fa-user-check"></i> Activate User</a>
			                    </div>
			                </div>

						</td>
	                </tr>

				<?php endforeach; ?>

	            </tbody>
	        </table>
		</div>


    </div>
</div>

<!-- Add New User Modal-->
<div class="modal fade" id="addUserModal" tabindex="-1" role="dialog" aria-labelledby="addUserModalLabel" aria-hidden="true">

	<div class="modal-dialog" role="document">

	  	<div class="modal-content">

		   	<div class="modal-header">
				<h5 class="modal-title text-success" id="addUserModalLabel">Add New User</h5>
				<button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
		    </div>

		    <div class="modal-body">
				<div>
					<form id="add-user-form">

					    <?=form_label('First name','first_name','class="sr-only"')?>
					    <?=form_error('first_name')?>
					    <?=form_input('first_name', '','class="form-control"  placeholder="First Name" required'). '<br/>'?>
					    <?=form_label('Last name','last_name','class="sr-only"')?>
					    <?=form_error('last_name')?>
					    <?=form_input('last_name', '','class="form-control"  placeholder="Last Name" required'). '<br/>'?>
					    <?=form_label('Email','email','class="sr-only"')?>
					    <?=form_error('email')?>
					    <?=form_input('email', '' ,'class="form-control" placeholder="Email Address" required'). '<br/>'?>
						<button class="btn btn-secondary " type="button" data-dismiss="modal">Cancel</button>

				      	<button id="add-user-submit-modal" class="btn btn-primary btn-icon-split save-changes" type="submit" href="javascript:void(0);">
				      		<span class="icon text-white-50">
			                  	<i class="fas fa-plus"></i>
			                </span>
			                <span class="text">Add User</span>
				      	</button>

					</form>
				</div>

				<div>
					<small>A password reset link will be generated once the user is created.</small>
				</div>

				<div>
					<small>Please email this to the user to have them set their password.</small>
				</div>


			</div>



		</div>

	</div>

</div>

<!-- Popup modal user Active Status -->
<div id="activeConfirmModal" class="modal fade" tabindex="-1" role="dialog">
  	<div class="modal-dialog" role="document">
	    <!-- Modal content-->
	    <div class="modal-content">
	      	<div class="modal-header">
				<h5 class="modal-title">Confirm Active Status Change</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          	<span aria-hidden="true">&times;</span>
		        </button>
	      	</div>
	      	<div id="modal-body" class="modal-body">
	      	</div>
	      	<div class="modal-footer">
		        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		        <button type="button" class="btn btn-primary active-status-submit">Confirm</button>
		    </div>
	    </div>
	</div>
</div>

<script type="text/javascript">

	/*** GLOBAL Variables ***/

	//get base_url for use in AJAX calls
	var baseUrl = <?=json_encode(base_url());?>;

	//get the current logged in user
	var loggedInUserId = <?=$this->ion_auth->user()->row()->id;?>;

	//wait for the DOM to load
	$(document).ready(function () {

		TimesheetApp.Admin.updateUserStatus(loggedInUserId)

		TimesheetApp.Admin.displayAddUserModal()

		TimesheetApp.Admin.submitAddUserModalForm(baseUrl)

	})

</script>
