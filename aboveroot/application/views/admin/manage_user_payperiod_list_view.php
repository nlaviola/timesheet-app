<!-- Admin Label -->
<div class="admin-label my-1">
	<span class="badge badge-info">Admin</span>
</div>

<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
	<h1 class="h3 mb-0 text-gray-800"><i class="fas fa-fw fa-history"></i> <?=$user->first_name?> <?=$user->last_name?> Pay Period Links</h1>
</div>

<!-- Breadcrumb -->
<nav aria-label="breadcrumb">
	<ol class="breadcrumb bg-transparent mb-0">
		<li class="breadcrumb-item"><a href="/manage_users">Manage Users</a></li>
	    <li class="breadcrumb-item"><a href="/manage_users/display_user_account/<?=$user->id?>"><?=$user->first_name?> <?=$user->last_name?></a></li>
		<li class="breadcrumb-item active" aria-current="page">Pay Period Links</li>
	</ol>
</nav>

<!-- display pay period ranges if user has hours entered -->
<?php if (count($payperiod_ranges)> 0): ?>

	<?php foreach ($payperiod_ranges as $year => $quarter_list): ?>

	    <!-- Yearly Pay Period Links Card -->
	    <div class="card shadow mb-4">
	        <div class="card-header py-3">
	            <h6 class="m-0 font-weight-bold text-gray-800"><?=$year;?><a class="float-right font-weight-normal" href="/data/download_user_yearly_data_in_pay_periods/<?=$user->id;?>/<?=$year;?>"><i class="fas fa-file-csv"></i></a><a class="float-right mr-2 font-weight-normal" href="/data/view_user_yearly_data/<?=$user->id;?>/<?=$year;?>">Total Yearly Hours Chart</a></h6>
	        </div>
	        <div class="card-body">

				<?php foreach ($quarter_list as $quarter => $payperiods): ?>
	            	<!-- Quarter Section -->
	            	<div class="row">

	            		<div class="col-12">
	            			<h1 class="h5 mb-2 text-gray-800"><?=$quarter;?></h1>
	            		</div>

	            	<?php foreach ($payperiods as $date_ranges): ?>

	            		<?php if (date('Y-m-d', strtotime($date_ranges['start'])) == date('Y-m-d', strtotime($current_payperiod_start_date))): ?>

	            			<!-- current Payperiod Button Link -->
										<div class="col-xl-6 col-md-6 mb-4">
											<a href="/manage_timesheets/display_user_payperiod/<?=$date_ranges['start'];?>/<?=$date_ranges['end'];?>/<?=$user->id?>" class="btn btn-primary btn-block"><?=date('F jS', strtotime($date_ranges['start'])) . ' <i class="fas fa-long-arrow-alt-right"></i> ' . date('F jS', strtotime($date_ranges['end']));?></a>
										</div>

	            		<?php else: ?>

	            			<!-- Previous Payperiod Button Link -->
										<div class="col-xl-6 col-md-6 mb-4">
											<a href="/manage_timesheets/display_user_payperiod/<?=$date_ranges['start'];?>/<?=$date_ranges['end'];?>/<?=$user->id?>" class="btn btn-secondary btn-block"><?=date('F jS', strtotime($date_ranges['start'])) . ' <i class="fas fa-long-arrow-alt-right"></i> ' . date('F jS', strtotime($date_ranges['end']));?></a>
										</div>

	            	  <?php endif; ?>

	            	<?php endforeach; ?>

	            	</div>

	            <?php endforeach; ?>

	        </div>
	    </div>

	<?php endforeach; ?>

<?php else: ?>

	<div class="row">
		<div class="col-xl-6 col-md-6 mb-4">

			<div class="alert alert-warning">
				This user has not entered any hours!
			</div>

		</div>
	</div>

<?php endif; ?>
