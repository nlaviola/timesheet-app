<!-- Admin Label -->
<div class="admin-label my-1">
	<span class="badge badge-info">Admin</span>
</div>

<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
	<h1 class="h3 mb-0 text-gray-800"><i class="fas fa-fw fa-database"></i> Data</h1>
</div>

<!-- Content Row -->
<div class="row">

		<!-- links to additional Data -->
		<div class="col-12 mb-2">
			<a href="/data/view_labor_reports"><i class="fas fa-table"></i> Labor Reports</a>
		</div>

    <!-- Overall Hours Data -->
    <div class="col-12">

			<div class="row">
				<div class="col-xl-5 col-lg-6 col-md-8">
					<div class="alert alert-primary py-1 px-2" role="alert">
						<small><i class="fas fa-info-circle"></i> Data includes hours submitted but not yet approved</small>
					</div>
				</div>
			</div>

			<!-- Overall Hours by Job Card -->
			<div class="card shadow mb-4">

					<!-- Card Header -->
					<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
							<h6 class="m-0 font-weight-bold">Overall Hours by Job</h6>
					</div>

					<!-- Card Body -->
					<div class="card-body">

							<div class="row">

									<div class="col-xl-4 col-lg-6 col-md-8">
											<select id="year-select" class="custom-select" name="year-select">
													<option value="">Select a Year</option>
													<?php foreach($years  as $year): ?>
															<option value=""><?=$year?></option>
													<?php endforeach; ?>
											</select>
									</div>

									<div class="col-xl-4 col-lg-6 col-md-8">
										<div id="payperiod-options"></div>
									</div>

									<div class="col-xl-4 col-lg-6 col-md-8">
										<div id="download-data" class="float-right"></div>
									</div>

							</div>

							<div class="row justify-content-center">

									<div class="col-xl-10 col-lg-12">
						<h1 class="h4 m-2 text-gray-800 font-weight-bold">Hours</h1>
						<div id="overall-hours"></div>
									</div>

									<div class="col-xl-10 col-lg-12">
						<h1 class="h4 m-2 text-gray-800 ">Cleanroom Hours</h1>
											<div id="overall-cleanroom-hours"></div>
									</div>

							</div>

					</div>

			</div>

    </div>

	<!-- Date Range Hours Data -->
    <div class="col-12">

			<div class="card shadow mb-4">

					<!-- Card Header -->
					<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
							<h6 class="m-0 font-weight-bold">Hours Total by Date Range</h6>
					</div>

					<!-- Card Body -->
					<div class="card-body">

							<div class="row">

									<div class="col-xl-2 col-lg-2 col-md-8 pb-2">
											<select id="job-select-data" class="custom-select range-data-select" name="job-select">
													<option value="blank">Select a Job</option>
							<option value="All">All</option>
													<?php foreach($jobs  as $job): ?>
															<option value=""><?=$job->category?></option>
													<?php endforeach; ?>
											</select>
									</div>

					<div class="col-xl-2 col-lg-2 col-md-8 pb-2">
											<select id="user-select-data" class="custom-select range-data-select" name="job-select">
													<option value="blank">Select a User</option>
							<option value="All">All</option>
													<?php foreach($active_users  as $user): ?>
															<option value="<?=$user->id?>"><?=$user->first_name?> <?=$user->last_name?></option>
													<?php endforeach; ?>
							<?php foreach($inactive_users  as $user): ?>
															<option value="<?=$user->id?>"><?=$user->first_name?> <?=$user->last_name?></option>
													<?php endforeach; ?>
											</select>

									</div>

					<div class="col-xl-3 col-lg-3 col-md-8 pb-2">
						<input type="text" class="form-control range-data-date" id="datepicker-start-data" placeholder="Start Date">

									</div>

					<div class="col-xl-3 col-lg-3 col-md-8 pb-2">
						<input type="text" class="form-control range-data-date" id="datepicker-end-data" placeholder="End Date">
									</div>

					<div class="col-xl-2 col-lg-2">
						<button type="button" id="button-data" class="btn btn-primary">Get Data</button>
					</div>


							</div>

				<hr>

							<div id="date-range-results" class="row">
				
					<div class="col-xl-2 col-lg-2 col-md-8pb-2">
					<div class="pl-2" id="job-result"></div>
									</div>

					<div class="col-xl-2 col-lg-2 col-md-8pb-2">
						<div class="pl-2" id="user-result"></div>

									</div>

					<div class="col-xl-3 col-lg-3 col-md-8pb-2">
						<div class="pl-2" id="date-start-result"></div>
									</div>

				<div class="col-xl-3 col-lg-3 col-md-8pb-2">
						<div class="pl-2" id="date-end-result"></div>
				</div>
				
				<div id="hours-range" class="col-12 pt-3"></div>
				<div id="cleanroom-hours-range" class="col-12"></div>
				<div id="hours-table" class="col-12"></div>
				<div id="cleanroom-hours-table" class="col-12"></div>

							</div>

					</div>

			</div>

    </div>


</div>
<script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
<script type="text/javascript">
    /*** GLOBAL Variables ***/



    //get base_url for use in AJAX calls
    let baseUrl = <?=json_encode(base_url());?>;

    //wait for the DOM to load
    $(document).ready(function () {

		//DatePicker
		$('.range-data-date').datepicker({
			dateFormat: "yy-mm-dd"
		});

		$('#button-data').on('click', function(){

			//clear existing data
			$('#job-result').empty()
			$('#user-result').empty()
			$('#date-start-result').empty()
			$('#date-end-result').empty()
			$('#hours-range').empty()
			$('#cleanroom-hours-range').empty()
			$('#hours-table').empty()
			$('#cleanroom-hours-table').empty()

			let job, user, userName, dateStart, dateEnd
			let isValid= true

			//check to see if select options chosen
			$('.range-data-select').each(function(index){
				if ($(this).val() == 'blank') {
					//highlight box requiring input
					$(this).addClass('is-invalid')

					//set isValid to false
					isValid = false 
				}
			})

			//check to see if dates filled
			$('.range-data-date').each(function(index){
				if ($(this).val() == '') {
					//highlight box requiring input
					$(this).addClass('is-invalid')

					//set isValid to false
					isValid = false 
				}
			})

			//if all fields are filled out, post for hours data
			if (isValid) {

				//get user inputs
				job = $('#job-select-data').children("option:selected").text()
				user = $('#user-select-data').children("option:selected").val()
				userName = $('#user-select-data').children("option:selected").text()
				dateStart = $('#datepicker-start-data').val()
				dateEnd = $('#datepicker-end-data').val()

				//post and return data
				$.post(baseUrl + 'data/get_hours_by_date_range_ajax',{job: job, user: user, date_start: dateStart, date_end: dateEnd})

				.then(function(data){

					//parse JSON data
					let hoursData = JSON.parse(data)

					console.log(hoursData)

					let hours_sum = (hoursData.hours_sum ? hoursData.hours_sum : 0)
					let cleanroomHours_sum = (hoursData.cleanroom_hours_sum ? hoursData.cleanroom_hours_sum : 0)

					//write in selected values
					$('#job-result').html('<strong>' +job+ '</strong>')
					$('#user-result').html('<strong>' +userName+ '</strong>')
					$('#date-start-result').html('<strong>' +dateStart+ '</strong>')
					$('#date-end-result').html('<strong>' +dateEnd+ '</strong>')

					//write in hours
					$('#hours-range').html('<h1 class="h4 m-2 text-gray-800 ">Hours: <strong>' + hours_sum +'</strong></h1>')
					$('#cleanroom-hours-range').html('<h1 class="h4 m-2 text-gray-800 ">Cleanroom Hours: <strong>' + cleanroomHours_sum + '</strong></h1>')

					//write in hour rows
					let hoursTableHtml = '<hr/><h3>Hours Table - ' + hours_sum + '</h3><table class="table table-striped"><thead><tr><th scope="col">Job</th><th scope="col">Date</th><th scope="col">User</th><th scope="col">Description</th><th scope="col">Hours</th>'

					for (const row of hoursData.hours) {
						hoursTableHtml += '<tr><td>' + row.category + '</td><td>' + row.hours_date + '</td><td>' + row.name + '</td><td>' + row.description + '</td><td>' + row.hours + '</td></tr>'
					}

					$('#hours-table').html(hoursTableHtml)

					let cleanroomHoursTableHtml = '<h3>Cleanroom Hours Table - ' + cleanroomHours_sum + '</h3><table class="table table-striped"><thead><tr><th scope="col">Job</th><th scope="col">Date</th><th scope="col">User</th><th scope="col">description</th><th scope="col">Cleanroom Hours</th>'

					for (const row of hoursData.cleanroom_hours) {
							cleanroomHoursTableHtml += '<tr><td>' + row.category + '</td><td>' + row.hours_date + '</td><td>' + row.name + '</td><td>' + row.description + '</td><td>' + row.cleanroom_hours + '</td></tr>'
					}

					$('#cleanroom-hours-table').html(cleanroomHoursTableHtml)


				})

				
			}

		})


		$('#year-select').on('change', function (){

			//empty all divs
			$('div#overall-hours').empty()
			$('div#overall-cleanroom-hours').empty()
			$('div#payperiod-options').empty()

		    let year = $(this).children("option:selected").text()

		    if (year != 'Select a Year') {

				//query for payperiods in that year
				$.post(baseUrl + 'payperiod/get_payperiod_ranges_ajax',{year: year})

				.then(function(data){

					//parse JSON data
					let payperiodRanges = JSON.parse(data)

					//create select html for pay periods
					let quarterSelectHTML = '<select id="payperiod-select" class="custom-select" name="year-select"><option>Select a Payperiod</option><option value="all">Year Total</option>'

					$.each( payperiodRanges, function( key, value ) {

						let startDateFormatted = moment(value.start).format("MMM D, YYYY")
						let endDateFormatted = moment(value.end).format("MMM D, YYYY")

						quarterSelectHTML += '<option value=' + value.start + ':' + value.end + '>' +  startDateFormatted + ' | ' + endDateFormatted + '</option>'

					});

					quarterSelectHTML += '</select>'

					//write in new select
					$('#payperiod-options').html(quarterSelectHTML).hide().fadeIn()

				})

		    }

			//clear options if Select a year is chosen
			if (year == 'Select a Year') {
				$('div#overall-hours').empty()
				$('div#overall-cleanroom-hours').empty()
				$('div#payperiod-options').empty()
			}

		})

		//written this way to bind to any future selects that are written in this div
		$('#payperiod-options').on('change', 'select', function (){

			//get the payperiod
			let payPeriodSelected = $(this).children("option:selected").val()
			//get the user selected year
			let yearSelected = $("#year-select option:selected").text()

			//clear chart div and write new canvas element
			$('div#overall-hours').empty().html('<div id="hours-chart"></div>')
			$('div#overall-cleanroom-hours').empty().html('<div id="cleanroom-chart"></div>')

			//call function to draw charts
			prepOverallChartData(baseUrl, yearSelected, payPeriodSelected)

		})


		function prepOverallChartData(baseUrl, year, payPeriod){

			let overallHours, overallCleanroomHours

			//post call for hours data
			$.post(baseUrl + 'data/get_overall_hours_ajax',{year: year, payperiod: payPeriod})

			.then(function(data){

			//parse data
	    let overallHoursInformation = JSON.parse(data)

			//initialize variables
			let hoursChartData = []
			let hoursLabels = []
			let cleanroomChartData = []
			let cleanroomLabels = []

			//build arrays for chart data
			$.each(overallHoursInformation.overall_hours, function(key, value){

				hoursChartData.push(value.hours)
				hoursLabels.push(value.category)

			})
			$.each(overallHoursInformation.overall_cleanroom_hours, function(key, value){

				cleanroomChartData.push(value.cleanroom_hours)
				cleanroomLabels.push(value.category)

			})

			//call function to draw charts
			TimesheetApp.Utilities.drawApexChart('pie', 'hours-chart', hoursChartData, hoursLabels)
			TimesheetApp.Utilities.drawApexChart('pie', 'cleanroom-chart', cleanroomChartData, cleanroomLabels)

					})

			}

    })
</script>
