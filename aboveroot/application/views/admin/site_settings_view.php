<!-- Admin Label -->
<div class="admin-label my-1">
	<span class="badge badge-info">Admin</span>
</div>

<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
	<h1 class="h3 mb-0 text-gray-800"><i class="fas fa-fw fa-cogs"></i> Site Settings</h1>
</div>

<!-- Content Row -->
<div class="row">

    <!-- Add Jobs -->
	<div class="col-12">
		<div class="my-2">
			<a id="add-job" href="javascript:void(0)"><i class="fas fa-fw fa-folder-plus"></i> Add New job</a>
		</div>
	</div>
	<div class="col-12">
		<div id="job-message">

		<?php if (!empty($this->session->flashdata('message'))): ?>

			<!-- User Alert -->
			<div class="alert alert-success alert-dismissible fade show" role="alert">
			  	<strong>Job successfully added!</strong>
			  	<div>
			  		<?=$this->session->flashdata('message');?>
			  	</div>
			  	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			    	<span aria-hidden="true">&times;</span>
			  	</button>
			</div>

		<?php endif; ?>

		</div>
	</div>


	<!-- Manage Active Jobs -->
    <div class="col-12">

        <!-- Manage Active Jobs Card -->
      	<div class="card shadow mb-4">

	        <!-- Card Header -->
	        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
	          	<h6 class="m-0 font-weight-bold">Manage Active Jobs</h6>
	        </div>

	        <!-- Card Body -->
	        <div class="card-body">


                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">Code</th>
                            <th scope="col">Description</th>
                            <th scope="col">Status</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php foreach ($active_jobs as $job): ?>

                            <tr>
                                <td><strong><?=$job->category?></strong></td>
                                <td><?=$job->description?></td>
                                <td>Active</td>
                                <td>

                                    <div class="dropdown no-arrow mb-4">
          			                    <a class="dropdown-toggle text-primary" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="javascript:void(0)">
          			                      <i class="fas fa-fw fa-ellipsis-h"></i>
          							  	</a>
          			                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
          			                      	<a class="dropdown-item text-danger update-job-status-link" data-current-status='active' data-job-id="<?=$job->id;?>" href="javascript:void(0)"><i class="far fa-fw fa-stop-circle"></i> Inactivate job</a>
          			                    </div>
      			                    </div>

                                </td>
                            </tr>

                        <?php endforeach; ?>

                    </tbody>
                </table>

            </div>

        </div><!-- END: Manage Jobs Card -->

    </div>

    <!-- Manage Inactive Jobs -->
    <div class="col-12">

        <!-- Manage Inactive Jobs Card -->
      	<div class="card shadow mb-4">

	        <!-- Card Header -->
	        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
	          	<h6 class="m-0 font-weight-bold">Manage Inactive Jobs</h6>
	        </div>

	        <!-- Card Body -->
	        <div class="card-body">


                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">Code</th>
                            <th scope="col">Description</th>
                            <th scope="col">Status</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php foreach ($inactive_jobs as $job): ?>

                            <tr>
                                <td><strong><?=$job->category?></strong></td>
                                <td><?=$job->description?></td>
                                <td>Inactive</td>
                                <td>

                                    <div class="dropdown no-arrow mb-4">
          			                    <a class="dropdown-toggle text-primary" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="javascript:void(0)">
          			                      <i class="fas fa-fw fa-ellipsis-h"></i>
          							  	</a>
          			                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
          			                      	<a class="dropdown-item text-success update-job-status-link" data-current-status='inactive' data-job-id="<?=$job->id;?>" href="javascript:void(0)"><i class="fas fa-fw fa-folder-plus"></i> Activate job</a>
          			                    </div>
      			                    </div>

                                </td>
                            </tr>

                        <?php endforeach; ?>

                    </tbody>
                </table>

            </div>

        </div><!-- END: Manage Jobs Card -->

    </div>

</div>

<!-- Add New Job Modal-->
<div class="modal fade" id="addJobModal" tabindex="-1" role="dialog" aria-labelledby="addJobModalLabel" aria-hidden="true">

	<div class="modal-dialog" role="document">

	  	<div class="modal-content">

		   	<div class="modal-header">
				<h5 class="modal-title text-success" id="addUserModalLabel">Add New Job</h5>
				<button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
		    </div>

		    <div class="modal-body">
				<div>
					<form id="add-job-form">

					    <?=form_label('Job Code','category','class="sr-only"')?>
					    <?=form_error('category')?>
					    <?=form_input('category', '','class="form-control"  placeholder="Job Code" required'). '<br/>'?>
					    <?=form_label('Description','description','class="sr-only"')?>
					    <?=form_error('description')?>
					    <?=form_textarea('description', '','class="form-control"  placeholder="Short Description" rows="3" required'). '<br/>'?>

						<button class="btn btn-secondary " type="button" data-dismiss="modal">Cancel</button>

				      	<button id="add-user-submit-modal" class="btn btn-primary btn-icon-split save-changes" type="submit" href="javascript:void(0);">
				      		<span class="icon text-white-50">
			                  	<i class="fas fa-plus"></i>
			                </span>
			                <span class="text">Add Job</span>
				      	</button>

					</form>
				</div>

			</div>

		</div>

	</div>

</div>

<!-- Popup modal Job Status -->
<div id="activeConfirmModal" class="modal fade" tabindex="-1" role="dialog">
  	<div class="modal-dialog" role="document">
	    <!-- Modal content-->
	    <div class="modal-content">
	      	<div class="modal-header">
				<h5 class="modal-title">Confirm Active Status Change</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          	<span aria-hidden="true">&times;</span>
		        </button>
	      	</div>
	      	<div id="modal-body" class="modal-body">
	      	</div>
	      	<div class="modal-footer">
		        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		        <button type="button" class="btn btn-primary active-status-submit">Confirm</button>
		    </div>
	    </div>
	</div>
</div>

<script type="text/javascript">

    /*** GLOBAL Variables ***/

    //get base_url for use in AJAX calls
    var baseUrl = <?=json_encode(base_url());?>;

    //wait for the DOM to load
    $(document).ready(function () {

        TimesheetApp.Admin.updateJobStatus()

        TimesheetApp.Admin.displayAddJobModal()

		TimesheetApp.Admin.submitAddJobModalForm(baseUrl)

    })

</script>
