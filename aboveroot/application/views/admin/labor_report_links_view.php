<!-- Admin Label -->
<div class="admin-label my-1">
	<span class="badge badge-info">Admin</span>
</div>

<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-2">
	<h1 class="h3 mb-0 text-gray-800"><i class="fas fa-fw fa-table"></i> Labor Reports</h1>
</div>

<!-- Breadcrumb -->
<nav aria-label="breadcrumb">
	<ol class="breadcrumb bg-transparent mb-0">
	<li class="breadcrumb-item"><a href="/data/data_display">Data</a></li>
	<li class="breadcrumb-item active" aria-current="page">Labor Reports</li>
	</ol>
</nav>

<div class="row">
  <div class="col-xl-6 col-lg-6">
    <div class="alert alert-primary" role="alert">
    <i class="fas fa-info-circle"></i> Click on a pay period to download a Labor Report
    </div>
  </div>
</div>

<div class="row">

<?php foreach ($payperiod_ranges as $year => $quarter_list): ?>

  <div class="col-xl-6 col-md-6">

    <div class="card shadow mb-2">
      <div class="card-header py-3">
	      <h6 class="m-0">
          <?php if(date('Y') == $year): ?>
            <a href="/data/download_labor_report/<?=date('Y-01-01')?>/<?=date('Y-m-d')?>"><?=$year?></a>
          <?php else: ?>
            <a href="/data/download_labor_report/<?=date($year . '-01-01')?>/<?=date($year . '-12-31')?>"><?=$year?></a>
          <?php endif; ?>
          
        </h6>
	    </div>
	    <div class="card-body">
        <?php foreach ($quarter_list as $quarter => $payperiods): ?>
          <h1 class="h5 mb-2 text-gray-800"><?=$quarter;?></h1>
          <?php foreach ($payperiods as $date_ranges): ?>
            <?php if (date('Y-m-d', strtotime($date_ranges['start'])) == date('Y-m-d', strtotime($current_payperiod_start_date))): ?>

              <!-- current Payperiod Button Link -->
              <div class="mb-2">
                <a href="/data/download_labor_report/<?=$date_ranges['start'];?>/<?=$date_ranges['end'];?>" class="btn btn-outline-primary btn-block"><?=date('F jS', strtotime($date_ranges['start'])) . ' <i class="fas fa-long-arrow-alt-right"></i> ' . date('F jS', strtotime($date_ranges['end']));?></a>
              </div>

            <?php else: ?>

              <!-- Previous Payperiod Button Link -->
              <div class="mb-2">
                <a href="/data/download_labor_report/<?=$date_ranges['start'];?>/<?=$date_ranges['end'];?>" class="btn btn-outline-secondary btn-block"><?=date('F jS', strtotime($date_ranges['start'])) . ' <i class="fas fa-long-arrow-alt-right"></i> ' . date('F jS', strtotime($date_ranges['end']));?></a>
              </div>

            <?php endif; ?>
          <?php endforeach; ?>
        <?php endforeach; ?>
      </div>
    </div>

  </div>
<?php endforeach; ?>

</div>