<!-- Admin Label -->
<div class="admin-label my-1">
	<span class="badge badge-info">Admin</span>
</div>

<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
	<h1 class="h3 mb-0 text-gray-800"><i class="far fa-fw fa-check-square"></i> Approve Hours</h1>
</div>

<div class="row">

	<div class="col-12">
		<!-- Alert messages -->
		<div id="message">

		<?php if (!empty($this->session->flashdata('message'))): ?>

			<!-- Alert Message -->
			<div class="alert alert-success alert-dismissible fade show" role="alert">
			  	<strong>You did it!</strong>
			  	<div>
			  		<?=$this->session->flashdata('message');?>
			  	</div>
			  	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			    	<span aria-hidden="true">&times;</span>
			  	</button>
			</div>

		<?php endif; ?>

		<?php if (!empty($this->session->flashdata('error'))): ?>

			<!-- Alert Message -->
			<div class="alert alert-danger alert-dismissible fade show" role="alert">
			  	<strong>You did it!</strong>
			  	<div>
			  		<?=$this->session->flashdata('error');?>
			  	</div>
			  	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			    	<span aria-hidden="true">&times;</span>
			  	</button>
			</div>

		<?php endif; ?>

		</div>

	</div>

    <div class="col-12">

        <?php foreach ($unapproved_hours_data as $name => $user_data): ?>

            <!-- User Approve Hours Card -->
            <div class="card shadow mb-1">
                <!-- Card Header - Accordion -->
                <a href="#collapseCard<?=$user_data['id']?>" class="d-block card-header border-left-primary py-3 collapsed" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="collapseCard<?=$user_data['id']?>">
                    <h6 class="m-0 text-gray-800"><strong><?=$name?></strong> <span class="badge badge-secondary"><?=count($user_data['hours'])?></span></h6>
                </a>

                <!-- Card Content - Collapse -->
                <div class="collapse" id="collapseCard<?=$user_data['id']?>" style="">
                    <div class="card-body py-1">
												<!-- Only admins can approve hours -->
												<?php if ($this->ion_auth->is_admin()): ?>
													<a href="/approve_hours/approve_all_user_hours/<?=$user_data['id']?>"  class="btn btn-success btn-icon-split my-2 approve-all-hours" data-user-id="<?=$user_data['id']?>">
															<span class="icon text-white-50">
																<i class="fas fa-check-square"></i>
															</span>
															<span class="text">Approve All</span>
													</a>
												<?php endif; ?>

                        <?php foreach ($user_data['hours'] as $unapproved_entry): ?>

                            <div class="card border-left-warning shadow h-100 py-2 mb-2">
		                		<div class="card-body py-1">
		                 			<div class="row no-gutters align-items-center">
		                    			<div class="col mr-2">
		                    				<div class="h6 mb-0 font-weight-bold text-gray-800"><?=$unapproved_entry->hours_date;?> | <?=$unapproved_entry->category;?>: <?=$unapproved_entry->hours;?> <?=($unapproved_entry->is_approved == 0 ? '<i class="fas fa-fw fa-exclamation-triangle text-warning"></i>' : ''); ?></div>
		                    				<?php if ($unapproved_entry->cleanroom_hours > 0): ?>
		                    					<div class="text-s font-weight-bold mb-1">Cleanroom: <?=$unapproved_entry->cleanroom_hours;?></div>
		                    				<?php endif; ?>
		                      				<div class="text-s text-secondary mb-1"><?=$unapproved_entry->description;?></div>
		                    			</div>
		                    			<div class="col-auto">
		                    				<a class="unapproved-hours-edit" data-hours-id="<?=$unapproved_entry->id;?>" data-user-id="<?=$unapproved_entry->user_id;?>" data-name="<?=$unapproved_entry->name;?>" data-date-to-edit="<?=$unapproved_entry->hours_date;?>" data-category="<?=$unapproved_entry->category;?>" data-hours="<?=$unapproved_entry->hours;?>" data-cleanroom-hours="<?=$unapproved_entry->cleanroom_hours;?>" data-description="<?=htmlspecialchars($unapproved_entry->description);?>" href="javascript:void(0);"><i class="fas fa-edit "></i></a>
						                </div>
		                  			</div>
		                		</div>
		              		</div>

                        <?php endforeach; ?>

                    </div>
                </div>
            </div>

        <?php endforeach; ?>

    </div>

</div>

<!-- Unapproved Hours Modal-->
<div class="modal fade" id="unapprovedHoursModal" tabindex="-1" role="dialog" aria-labelledby="unapprovedHoursModalLabel" aria-hidden="true">

	<div class="modal-dialog" role="document">

	  	<div class="modal-content">

		   	<div class="modal-header">
				<h5 class="modal-title" id="unapprovedHoursModalLabel"></h5>
				<button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
		    </div>

			<div class="modal-body">

				<div id="hoursInfo" class="mb-1"></div>
				<!-- Only admins can approve hours -->
				<?php if ($this->ion_auth->is_admin()): ?>
					<div id="approveHoursButton"></div>
				<?php endif; ?>
	      		<hr>
	      		<div id="discardHoursButton"></div>

			</div>


		</div>

	</div>

</div>

<!-- Page specific JS -->
<script type="text/javascript">

	/*** GLOBAL Variables ***/

	//get base_url for use in AJAX calls
	var baseUrl = <?php echo json_encode(base_url()); ?>;
	//get today's date
	var dateToday = <?php echo json_encode($date_today); ?>;

    //wait for the DOM to load
	$(document).ready(function () {

		// govern on click of unapproved hours edit
    	$('a.unapproved-hours-edit').on('click', function (e) {
			//get hours data
      		var dataAttributes = $(this).data()

			//generate modal body html
			var unapprovedHoursHtml = "<h5><strong>" + dataAttributes.category + "</strong></h5><div><strong>Hours:</strong> " + dataAttributes.hours + "</div><div><strong>Cleanroom Hours:</strong> " + dataAttributes.cleanroomHours + "</div><div><strong>Description:</strong> " + dataAttributes.description + "</div>"

			// fill in modal
			//header
      		$('#unapprovedHoursModalLabel').html('<strong>' + dataAttributes.dateToEdit + '</strong> | ' + dataAttributes.name)
			//body
			$('#unapprovedHoursModal .modal-body #hoursInfo').html(unapprovedHoursHtml)
			//buttons
			$('#unapprovedHoursModal .modal-body #approveHoursButton').html('<a href="/approve_hours/confirm_approve_hours/' + dataAttributes.hoursId + '" class="btn btn-success btn-block">Approve Hours</a>')
			$('#unapprovedHoursModal .modal-body #discardHoursButton').html('<a href="/approve_hours/discard_hours/' + dataAttributes.hoursId + '" class="btn btn-danger btn-block">Discard Hours</a>')

			// display modal
      		$('#unapprovedHoursModal').modal('show')

		})

        //onclick alert user to approve all action
        $('a.approve-all-hours').on('click', function (e) {

            //if the user does not confirm, prevent the link navigation
            if (!confirm('You are about to approve all hours for this user. Do you wish to continue?')) {
                e.preventDefault();
            }

        })

		//onclick alert user to discard action
        $('#discardHoursButton').on('click', function (e) {

            //if the user does not confirm, prevent the link navigation
            if (!confirm('You are about to discard hours for this user. Do you wish to continue?')) {
                e.preventDefault();
            }

        })

    });

</script>
