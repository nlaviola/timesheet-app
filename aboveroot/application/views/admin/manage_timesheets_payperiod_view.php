<!-- Admin Label -->
<div class="admin-label my-1">
  	<span class="badge badge-info">Admin</span>
</div>

<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-2">
	<h1 class="h3 mb-0 text-gray-800"><?=$user->first_name?> <?=$user->last_name?> | <?=date('M d', strtotime($payperiod_start_date));?> - <?=date('M d', strtotime($payperiod_end_date));?></h1>
</div>

<!-- Breadcrumb -->
<nav aria-label="breadcrumb">
	<ol class="breadcrumb bg-transparent mb-0">
	<li class="breadcrumb-item"><a href="/manage_users">Manage Users</a></li>
    <li class="breadcrumb-item"><a href="/manage_users/display_user_account/<?=$user->id?>"><?=$user->first_name?> <?=$user->last_name?></a></li>
	<li class="breadcrumb-item"><a href="/manage_timesheets/display_user_payperiod_list/<?=$user->id?>">Pay Periods</a></li>
    <li class="breadcrumb-item active" aria-current="page"><?=date('M d', strtotime($payperiod_start_date));?> - <?=date('M d', strtotime($payperiod_end_date));?></li>
	</ol>
</nav>

<!-- Content Row -->
<div class="row">

	<!-- Legend Card -->
	<div class="col-xl-3 col-l-4 col-md-6 mb-2 d-none d-sm-none d-md-block">
		<div class="card shadow h-100 py-2">
            <div class="card-body py-1">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                    	<div><a href="#" class="btn btn-success btn-sm"></a><span class="text-gray-800"> approved</span></div>
                      	<div><a href="#" class="btn btn-warning btn-sm"></a><span class="text-gray-800"> needs approval</span></div>
                      	<div><a href="#" class="btn btn-danger btn-sm"></a><span class="text-gray-800"> missing hours</span></div>
                    </div>

                </div>
            </div>
        </div>
	</div>

	<!-- Working Days -->
    <div class="col-xl-3 col-md-6 mb-2 d-none d-sm-none d-md-block">
        <div class="card shadow h-100 py-2">
            <div class="card-body py-3">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      	<div class="text-xs font-weight-bold text-secondary text-uppercase mb-1">Working Days in Pay Period</div>
                      	<div class="h5 mb-0 font-weight-bold text-gray-800"><?=$working_days_in_payperiod_count;?></div>
                    </div>
                    <div class="col-auto">
                    	<i class="fas fa-sun fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>


<?php if (count($missing_days) > 0): ?>
	<!-- Missing Days -->
    <div class="col-xl-3 col-md-6 mb-2">
        <div class="card border-left-danger shadow h-100 py-2">
            <div class="card-body py-3">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      	<div class="text-xs font-weight-bold text-danger text-uppercase mb-1">Missing days!</div>
                      	<div class="h5 mb-0 font-weight-bold text-gray-800"><?=count($missing_days);?></div>
                    </div>
                    <div class="col-auto">
                    	<i class="fas fa-skull-crossbones fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>

    <!-- Payperiod Download Links -->
    <div class="col-xl-3 col-md-6 mb-2 d-none d-sm-none d-md-block">
        <a href="/data/download_user_payperiod_data/<?=date('Y-m-d', strtotime($payperiod_start_date));?>/<?=date('Y-m-d', strtotime($payperiod_end_date));?>/<?=$user->id?>" class="btn btn-block btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> payperiod data</a>
        <a href="/manage_timesheets/print_timesheet/<?=date('Y-m-d', strtotime($payperiod_start_date));?>/<?=date('Y-m-d', strtotime($payperiod_end_date));?>/<?=$user->id?>" target="_blank" class="btn btn-block btn-sm btn-primary shadow-sm"><i class="fas fa-print fa-sm text-white-50"></i> print payperiod</a>
        <a href="/data/download_user_cleanroom_data/<?=date('Y-m-01', strtotime($payperiod_start_date));?>/<?=date('Y-m-t', strtotime($payperiod_end_date));?>/<?=$user->id?>" class="btn btn-block btn-sm btn-primary shadow-sm"><i class="fas fa-microscope fa-sm text-white-50"></i> monthly cleanroom hours</a>
    </div>

</div>
<!-- Content Row -->
<div class="row">

    <div class="col-12">

        <?php if ($unapproved_check): ?>

					<!-- Only admins can approve hours -->
					<?php if ($this->ion_auth->is_admin()): ?>
            <!-- Link to approve all hours in payperiod -->
            <a href="/approve_hours/approve_all_user_payperiod_hours/<?=$user->id?>/<?=date('Y-m-d', strtotime($payperiod_start_date));?>/<?=date('Y-m-d', strtotime($payperiod_end_date));?>"  class="btn btn-success btn-icon-split my-2" id="approve-all-user-payperiod-hours" data-user-id="<?=$user->id?>">
                <span class="icon text-white-50">
                  <i class="fas fa-check-square"></i>
                </span>
                <span class="text">Approve All Payperiod Hours</span>
            </a>
					<?php endif; ?>

        <?php endif; ?>

    </div>

</div>

<!-- Content Row -->
<div class="row">

	<div class="col">

		<div id="message">

		<?php if (!empty($this->session->flashdata('message'))): ?>

			<!-- Alert Message -->
			<div class="alert alert-success alert-dismissible fade show" role="alert">
			  	<strong>You did it!</strong>
			  	<div>
			  		<?=$this->session->flashdata('message');?>
			  	</div>
			  	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			    	<span aria-hidden="true">&times;</span>
			  	</button>
			</div>

		<?php endif; ?>

		</div>

	</div>


</div>

<!-- Content Row -->
<div class="row">

	<!-- Daily Hours Cards -->
	<div class="col-12">

	<?php foreach ($user_payperiod_data as $date => $data): ?>


		<?php if ($date > date('Y-m-d')): ?>
			<!-- Disabled Card -->
			<div class="card shadow mb-1">

			    <!-- Card Header - Accordion -->
			    <div class="d-block card-header py-3">
			      	<h6 class="m-0 text-gray-500"><strong><?=$date;?></strong> Hours: <strong><?=(isset($data['stats']) ? $data['stats']['total_hours'] : 0 ); ?></strong> <?= (date('N', strtotime($date)) > 5 ? 'Weekend' : '') ;?></h6>
			    </div>

			</div>

		<?php else: ?>

			<!-- Interactive Card -->
			<div class="card shadow mb-1">
			    <!-- Card Header - Accordion -->
			    <a href="#collapseCard<?=$date;?>" class="d-block card-header <?=(!isset($data['stats']) && date('N', strtotime($date)) < 6 && $date < date('Y-m-d') ? 'border-left-danger' : ''); ?><?=(isset($data['stats']) && $data['stats']['unapproved_hours'] == 1 ? 'border-left-warning' : ''); ?><?=(isset($data['stats']) && date('N', strtotime($date)) < 6 && $data['stats']['unapproved_hours'] == 0 ? 'border-left-success' : ''); ?> <?= (date('N', strtotime($date)) > 5 ? 'border-left-secondary' : '') ;?> py-3 collapsed" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="collapseCard<?=$date;?>">
			      	<h6 class="m-0 text-gray-800"><strong><?=$date;?></strong> Hours: <strong><?=(isset($data['stats']) ? $data['stats']['total_hours'] : 0 ); ?></strong><?=(isset($data['stats']['total_cleanroom_hours']) && $data['stats']['total_cleanroom_hours'] > 0 ? ' | cleanroom: <strong>' .$data['stats']['total_cleanroom_hours'] . '</strong>' : '' ); ?> <?= (date('N', strtotime($date)) > 5 ? 'Weekend' : '') ;?> <?=(!isset($data['stats']) && date('N', strtotime($date)) < 6 && $date < date('Y-m-d') ? ' <i class="fas fa-fw fa-skull-crossbones fa-lg text-danger"></i>' : ''); ?></h6>
			    </a>
			    <!-- Card Content - Collapse -->
			    <div class="collapse" id="collapseCard<?=$date;?>" style="">
			      	<div class="card-body py-1">

			      		<div class='my-2 text-gray-800'><a class="add-hours" data-user-id="<?=$user->id;?>" data-date-to-add="<?=$date;?>" href="javascript:void(0);"><i class="far fa-fw fa-plus-square fa-lg"></i></a></div>

		      		<?php if (isset($data['stats'])): ?>

				       	<?php foreach ($data['hours'] as $entry): ?>

				       		<div class="card <?=($entry->is_approved == 0 ? 'border-left-warning' : 'border-left-success'); ?> shadow h-100 py-2 mb-2">
		                		<div class="card-body py-1">
		                 			<div class="row no-gutters align-items-center">
		                    			<div class="col mr-2">
		                    				<div class="h6 mb-0 font-weight-bold text-gray-800"><?=$entry->category;?>: <?=$entry->hours;?> <?=($entry->is_approved == 0 ? '<i class="fas fa-fw fa-exclamation-triangle text-warning"></i>' : ''); ?></div>
		                    				<?php if ($entry->cleanroom_hours > 0): ?>
		                    					<div class="text-s font-weight-bold mb-1">Cleanroom: <?=$entry->cleanroom_hours;?></div>
		                    				<?php endif; ?>
		                      				<div class="text-s text-secondary mb-1"><?=$entry->description;?></div>
		                    			</div>
		                    			<div class="col-auto">
		                    				<a class="manage-hours<?=($entry->is_approved == 1 ? ' approved' : ''); ?>" data-name="<?=$entry->name;?>" data-hours-id="<?=$entry->id;?>" data-user-id="<?=$entry->user_id;?>" data-date-to-edit="<?=$entry->hours_date;?>" data-category="<?=$entry->category;?>" data-hours="<?=$entry->hours;?>" data-cleanroom-hours="<?=$entry->cleanroom_hours;?>" data-description="<?=htmlspecialchars($entry->description);?>" href="javascript:void(0);"><i class="fas fa-edit "></i></a>
						                </div>
		                  			</div>
		                		</div>
		              		</div>

				       	<?php endforeach; ?>

					<?php endif; ?>

			      	</div>
			    </div>
			</div>

		<?php endif; ?>

	<?php endforeach; ?>

	</div>

</div>

<!-- Content Row -->
<div class="row">

    <div class="col-12">

          <div class="card shadow mb-4">
            <!-- Card Header - Dropdown -->
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-secondary">Total Project Hours <?=date('M d', strtotime($payperiod_start_date));?> - <?=date('M d', strtotime($payperiod_end_date));?></h6>
            </div>
            <!-- Card Body -->
            <div class="card-body">

                <div class="row">
                    <div class="col-xl-8">
                        <div id='job-cost-chart'></div>
                    </div>
                </div>

            </div>
          </div>
    </div>

</div>

<!-- Payperiod Hours Edit Modal-->
<div class="modal fade" id="hoursEditModal" tabindex="-1" role="dialog" aria-labelledby="hoursEditModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">

		<div class="modal-content">

		    <div class="modal-header">

                <div id="hoursEditModalLabel"></div>

			    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
			    </button>
		    </div>

		    <div class="modal-body">

		    	<form id="hours-edit-form">
			    	<input id="hours_date_edit_modal" name="hours_date" type="hidden" value="">

                    <input id="user_id_edit_modal" name="user_id" type="hidden" value="">

                    <input id="name_edit_modal" name="name" type="hidden" value="">

			    	<input id="category_edit_modal" name="category" type="hidden" value="">

                    <input id="form_type" name="form_type" type="hidden" value="edit_hours_form">

	          		<div class="form-group">
	          			<label for="hours">Hours</label>
	    				<input type="text" class="form-control" id="hours_edit_modal" name="hours" value="" required>
	          		</div>
	          		<div class="form-group">
	          			<label for="cleanroom_hours">Cleanroom Hours</label>
		    			<input type="text" class="form-control" id="cleanroom_hours_edit_modal" name="cleanroom_hours" value="">
	          		</div>
	          		<div class="form-group">
	          			<label for="description" class="">Description</label>
			    		<textarea class="form-control" rows="2" id="description_edit_modal" name="description" required></textarea>
	          		</div>
	          	</form>

			</div>

		    <div class="modal-footer">
		      	<button class="btn btn-secondary " type="button" data-dismiss="modal">Cancel</button>
		      	<a id="hours-edit-submit-modal" class="btn btn-primary btn-icon-split save-changes" href="javascript:void(0);" style="display:none;">
		      		<span class="icon text-white-50">
                      	<i class="fas fa-arrow-right"></i>
                    </span>
                    <span class="text">Save Changes</span>
		      	</a>
		    </div>

		</div>

	</div>
</div>

<!-- Payperiod Add Hours Modal-->
<div class="modal fade" id="addHoursModal" tabindex="-1" role="dialog" aria-labelledby="addHoursModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">

	  	<div class="modal-content">

		    <div class="modal-header">
                <div>
                    <h4 class="modal-title" id="addHoursModalLabel"><span class="badge badge-secondary"><?=$user->first_name?> <?=$user->last_name?></span></h4>
                    <h5><span id="addHoursTitle"></span></h5>
                </div>

			      <button class="close" type="button" data-dismiss="modal" aria-label="Close">
			        	<span aria-hidden="true">×</span>
			      </button>
		    </div>

		    <div class="modal-body">

		    	<!-- Success message -->
		    	<div id="add-success"></div>

		    	<!-- Log hours Form -->
	          	<form id="add_hours_form">

                    <input id="user_id_add_modal" name="user_id" type="hidden" value="">

	          		<input id="add_hours_date_add_modal" name="hours_date" type="hidden" value="">

                    <input id="form_type" name="form_type" type="hidden" value="add_hours_form">

	          		<div class="form-group">
	          			<label for="category">Category</label>
				    	<select class="form-control custom-select" id="category" name="category" required>
				    		<option value="">Select a Job</option>
			    		<?php foreach ($jobs as $job): ?>
			    			<option><?=$job->category; ?></option>
			    		<?php endforeach; ?>
                            <option value="zero">Zero Out</option>
						</select>
	          		</div>
	          		<div class="form-group">
	          			<label for="hours">Hours</label>
	    				<input type="text" class="form-control" id="hours" name="hours" value="" required>
	          		</div>
	          		<div class="form-group">
	          			<label for="cleanroom_hours">Cleanroom Hours</label>
		    			<input type="text" class="form-control" id="cleanroom_hours" name="cleanroom_hours" value="">
	          		</div>
	          		<div class="form-group">
	          			<label for="description" class="">Description</label>
			    		<textarea class="form-control" rows="2" id="description" name="description" required></textarea>
	          		</div>
	          		<div class="form-group">
	          			<button id="add-hours-submit-modal" type="submit" class="btn btn-primary btn-icon-split">
		                    <span class="icon text-white-50">
		                      	<i class="fas fa-arrow-right"></i>
		                    </span>
		                    <span class="text text-white">Submit</span>
		                </button>
	          		</div>
	          	</form>

			</div>

		</div>

	</div>
</div>

<!-- Manage Hours Modal-->
<div class="modal fade" id="unapprovedHoursModal" tabindex="-1" role="dialog" aria-labelledby="unapprovedHoursModalLabel" aria-hidden="true">

	<div class="modal-dialog" role="document">

	  	<div class="modal-content">

		   	<div class="modal-header">
				<h5 class="modal-title" id="unapprovedHoursModalLabel"></h5>
				<button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
		    </div>

			<div class="modal-body">

				<div id="hoursInfo" class="mb-1"></div>
					<!-- Only admins can approve hours -->
					<?php if ($this->ion_auth->is_admin()): ?>
						<div id="approveHoursButton"></div>
					<?php endif; ?>
					<hr>
							<div id="editHoursButton"></div>
					<hr>
					<div id="discardHoursButton"></div>

			</div>


		</div>

	</div>

</div>
<script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
<!-- Page specific JS -->
<script type="text/javascript">

	/*** GLOBAL Variables ***/

	//get base_url for use in AJAX calls
	var baseUrl = <?php echo json_encode(base_url()); ?>;
	//get today's date
	var dateToday = <?php echo json_encode($date_today); ?>;
    //get the period start & end date
    var startDate = <?php echo json_encode($payperiod_start_date); ?>;
    var endDate = <?php echo json_encode($payperiod_end_date); ?>;
    //get the user id
    var userId = <?php echo json_encode($user->id); ?>;

	//wait for the DOM to load
	$(document).ready(function () {

        TimesheetApp.Hours.toggleModalSaveChangesButton('#hoursEditModal')

		TimesheetApp.Admin.displaySupervisorAddHoursModal()

		TimesheetApp.Admin.submitSupervisorAddHoursModalForm(baseUrl)

        TimesheetApp.Admin.displaySupervisorManageHoursModal()

        TimesheetApp.Admin.displaySupervisorEditHoursModal()

        TimesheetApp.Admin.submitSupervisorEditHoursModalForm(baseUrl)

        TimesheetApp.Hours.zeroOutFormHours()

        //get user timesheet data and put it in an Apex Chart
        $.post('/data/get_user_hours_by_category_ajax',{start_date: startDate, end_date: endDate, user_id: userId})

        .then(function(data){

          //parse data
          var userHoursInformation = JSON.parse(data)

          var hoursData = userHoursInformation.hoursData

          if (userHoursInformation.jobCosts) {
            var jobCosts = userHoursInformation.jobCosts
          }

          if (hoursData.length > 0) {

              //initialize variables
              let hoursChartData = []
              let hoursLabels = []

              //build arrays for chart data
              $.each(hoursData, function(key, value){

                  hoursChartData.push(value.hours)
                  hoursLabels.push(value.category)

              })

                //draw the chart
                TimesheetApp.Admin.drawApexChartJobCosts('pie', 'job-cost-chart', hoursChartData, hoursLabels, jobCosts)

          }

        })

        //onclick alert user to approve all action
        $('a#approve-all-user-payperiod-hours').on('click', function (e) {

            //if the user does not confirm, prevent the link navigation
            if (!confirm('You are about to approve all payperiod hours for this user. Do you wish to continue?')) {
                e.preventDefault();
            }

        })

        //onclick alert user to discard action
        $('#discardHoursButton').on('click', function (e) {

            //if the user does not confirm, prevent the link navigation
            if (!confirm('You are about to discard hours for this user. Do you wish to continue?')) {
                e.preventDefault();
            }

        })

	})

</script>
