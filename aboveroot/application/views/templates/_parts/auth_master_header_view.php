<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?=$pagetitle; ?></title>

    <!-- Custom fonts for this template-->
    <link href="<?php echo base_url('assets/css/fontawesome-all.min.css'); ?>" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="<?php echo base_url('assets/css/sb-admin-2.css'); ?>" rel="stylesheet">

    <!-- jQuery UI CSS -->
    <link href="<?php echo base_url('assets/css/jquery-ui.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/jquery-ui.theme.min.css'); ?>" rel="stylesheet">

    <!-- Bootstrap core JavaScript-->
    <script src="<?php echo base_url('assets/js/jquery.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/bootstrap.bundle.min.js'); ?>"></script>

    <!-- Page level plugins -->
    <script src="<?php echo base_url('assets/js/Chart.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/chartjs-plugin-datalabels.min.js'); ?>"></script>

    <!-- Moment.js -->
    <script src="<?php echo base_url('assets/js/moment.min.js'); ?>"></script>

    <!-- jQuery UI -->
    <script src="<?php echo base_url('assets/js/jquery-ui.js'); ?>"></script>

    <!-- Application JS -->
    <script src="<?php echo base_url('assets/js/timesheet.js'); ?>"></script>

</head>

    <body id="page-top">

        <!-- Page Wrapper -->
        <div id="wrapper">
