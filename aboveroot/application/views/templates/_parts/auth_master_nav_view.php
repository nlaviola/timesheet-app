<!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

        <!-- Sidebar - Brand -->
        <a class="sidebar-brand d-flex align-items-center justify-content-center" href="/dashboard">
            <div class="sidebar-brand-icon">
              <i class="far fa-clock"></i>
            </div>
            <div class="sidebar-brand-text mx-3">Timesheet<sup>v2</sup></div>
        </a>

        <!-- Divider -->
        <hr class="sidebar-divider my-0">

        <!-- Nav Item - Dashboard -->
        <li class="nav-item <?=($this->uri->segment(1)=='dashboard' || $this->uri->segment(1)=='') ? 'active' : '';?>">
            <a class="nav-link" href="/dashboard">
                <i class="fas fa-fw fa-tachometer-alt"></i>
                <span>Dashboard</span>
            </a>
        </li>

        <!-- Divider -->
        <hr class="sidebar-divider">

        <!-- Heading -->
        <div class="sidebar-heading">
            Pay Periods
        </div>

        <!-- Nav Item - Current -->
        <li class="nav-item <?=($this->uri->segment(2)=='current') ? 'active' : '';?>">
            <a class="nav-link" href="/payperiod/current/<?=$current_payperiod_start_date;?>/<?=$current_payperiod_end_date;?>">
                <i class="fas fa-fw fa-calendar-week"></i>
                <span>Current</span>
            </a>
        </li>

        <!-- Nav Item - Previous Collapse Menu -->
        <li class="nav-item <?=($this->uri->segment(2)=='links') ? 'active' : '';?>">
            <a class="nav-link <?=($this->uri->segment(2)=='links') ? '' : 'collapsed';?>" href="#" data-toggle="collapse" data-target="#collapsePrevious" aria-expanded="<?=($this->uri->segment(2)=='links') ? 'false' : 'true';?>" aria-controls="collapsePrevious">
                <i class="fas fa-fw fa-history"></i>
                <span>Previous</span>
            </a>
            <div id="collapsePrevious" class="collapse <?=($this->uri->segment(2)=='links') ? 'show' : '';?>" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <h6 class="collapse-header">Year(s)</h6>
                    <?php foreach ($years_worked as $year): ?>
                        <a class="collapse-item <?=($this->uri->segment(2)=='links' && $this->uri->segment(3)==$year) ? 'active' : '';?>" href="/payperiod/links/<?=$year;?>"><?=$year;?></a>
                    <?php endforeach; ?>
                </div>
            </div>
        </li>

        <!-- Divider -->
        <hr class="sidebar-divider">

        <!-- Heading -->
        <div class="sidebar-heading">
            Info
        </div>

        <!-- Nav Item - Current -->
        <li class="nav-item <?=($this->uri->segment(1)=='job_codes') ? 'active' : '';?>">
            <a class="nav-link" href="/job_codes">
                <i class="fas fa-fw fa-info-circle"></i>
                <span>Job Codes</span>
            </a>
        </li>

         <!-- Divider -->
         <hr class="sidebar-divider">

    <?php if ($this->ion_auth->is_admin() || $this->ion_auth->in_group('supervisor')): ?>

        <!-- Heading -->
        <div class="sidebar-heading">
            Admin
        </div>

        <!-- Nav Item - Manage Users -->
        <li class="nav-item <?=($this->uri->segment(1)=='manage_users' || $this->uri->segment(1)=='manage_timesheets') ? 'active' : '';?>">
            <a class="nav-link" href="/manage_users">
                <i class="fas fa-fw fa-user-clock"></i>
                <span>Users / Timesheets</span>
            </a>
        </li>

        <!-- Nav Item - Approve Users -->
        <li class="nav-item <?=($this->uri->segment(1)=='approve_hours') ? 'active' : '';?>">
            <a class="nav-link" href="/approve_hours">
                <i class="far fa-fw fa-check-square"></i>
                <span>Approve Hours</span>
            </a>
        </li>

        <!-- Nav Item - Data -->
        <li class="nav-item <?=($this->uri->segment(1)=='data') ? 'active' : '';?>">
            <a class="nav-link" href="/data/data_display">
                <i class="fas fa-fw fa-chart-pie"></i>
                <span>Data</span>
            </a>
        </li>

        <!-- Nav Item - Data -->
        <li class="nav-item <?=($this->uri->segment(1)=='settings') ? 'active' : '';?>">
            <a class="nav-link" href="/settings">
                <i class="fas fa-fw fa-cogs"></i>
                <span>Site Settings</span>
            </a>
        </li>

        <!-- Divider -->
        <hr class="sidebar-divider d-none d-md-block">

    <?php endif; ?>

        <!-- Sidebar Toggler (Sidebar) -->
        <div class="text-center d-none d-md-inline">
            <button class="rounded-circle border-0" id="sidebarToggle"></button>
        </div>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

        <!-- Main Content -->
        <div id="content">

            <!-- Topbar -->
            <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                <!-- Sidebar Toggle (Topbar) -->
                <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                    <i class="fa fa-bars"></i>
                </button>

                <!-- Topbar Navbar -->
                <ul class="navbar-nav ml-auto">

                    <div class="topbar-divider d-none d-sm-block"></div>

                    <!-- Nav Item - User Information -->
                    <li class="nav-item dropdown no-arrow">
                        <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="mr-2 d-none d-lg-inline text-gray-600"><?=$current_user; ?></span>
                            <i class="fas fa-ellipsis-v text-gray-600"></i>
                        </a>
                        <!-- Dropdown - User Information -->
                        <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                            <a class="dropdown-item" href="/user/logout">
                                <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                                Logout
                            </a>
                        </div>
                    </li>

                </ul>

            </nav>
            <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">
