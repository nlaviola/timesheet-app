				</div>
				<!-- /.container-fluid -->
			</div>
			<!-- End of Main Content -->

			<!-- Footer -->
		    <footer class="sticky-footer bg-white">
		        <div class="container my-auto">
		          	<div class="copyright text-center my-auto">
		            	<span>Copyright &copy; Praevium Research, Inc <?=date('Y')?></span>
		          	</div>
		        </div>
		    </footer>
		    <!-- End of Footer -->

		</div>
		<!-- End of Content Wrapper -->
	</div>
	<!-- End of Page Wrapper -->
</body>

<!-- Core plugin JavaScript-->
<script src="<?php echo base_url('assets/js/jquery.easing.min.js'); ?>"></script>

<!-- Custom scripts for all pages-->
<script src="<?php echo base_url('assets/js/sb-admin-2.min.js'); ?>"></script>

</html>
