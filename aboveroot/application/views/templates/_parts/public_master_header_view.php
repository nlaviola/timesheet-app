<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap.css'); ?>">

    <!-- Application CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/app.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/sb-admin-2.css'); ?>">

    <!-- Custom fonts for this template-->
    <link href="<?php echo base_url('assets/css/fontawesome-all.min.css'); ?>" rel="stylesheet" type="text/css">

    <!-- Bootstrap JS + popper bundle & jQuery -->
    <script src="<?php echo base_url('assets/js/jquery.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/bootstrap.bundle.min.js'); ?>"></script>

    <title><?php echo $pagetitle;?></title>
  </head>
  <body class="bg-gradient-primary">
