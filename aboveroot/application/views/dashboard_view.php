<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
	<h1 class="h3 mb-0 text-gray-800"><i class="fas fa-fw fa-tachometer-alt"></i> Dashboard</h1>
</div>

<!-- Content Row -->
<div class="row">

	<!-- Today's Date Card -->
    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card shadow h-100 py-1">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      	<div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Today's Date</div>
                      	<div class="h5 mb-0 font-weight-bold text-gray-800"><?=date('M d, Y');?></div>
                    </div>
                    <div class="col-auto">
                      	<i class="fas fa-calendar-day fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Period -->
    <div class="col-xl-3 col-md-6 mb-4 d-none d-sm-none d-md-block">
        <div class="card shadow h-100 py-1">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      	<div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Pay Period</div>
                      	<div class="h5 mb-0 font-weight-bold text-gray-800"><?=date('M d', strtotime($current_payperiod_start_date));?> - <?=date('M d', strtotime($current_payperiod_end_date));?></div>
                    </div>
                    <div class="col-auto">
                    	<i class="fas fa-calendar-week fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Working Days -->
    <div class="col-xl-3 col-md-6 mb-4 d-none d-sm-none d-md-block">
        <div class="card shadow h-100 py-1">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      	<div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Working Days</div>
                      	<div class="h5 mb-0 font-weight-bold text-gray-800"><?=$working_days_in_current_payperiod_count;?></div>
                    </div>
                    <div class="col-auto">
                    	<i class="fas fa-sun fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Missing Days -->
<?php if (count($missing_days) > 0): ?>
    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-danger shadow h-100 py-1">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      	<div class="text-xs font-weight-bold text-danger text-uppercase mb-1">Missing days!</div>
                      	<div class="h5 mb-0 font-weight-bold text-gray-800"><?=count($missing_days);?></div>
                    </div>
                    <div class="col-auto">
                    	<i class="fas fa-skull-crossbones fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>

</div>

<!-- Content Row -->
<div class="row">

	<!-- Log Hours -->
    <div class="col-xl-6 col-lg-6">

    	<div id="message-left">

    	<?php if (!empty($this->session->flashdata('error'))): ?>

			<!-- User Alert -->
			<div class="alert alert-danger alert-dismissible fade show" role="alert">
			  	<strong>Uh oh! You should check on some of those fields below.</strong>
			  	<div>
			  		<?=$this->session->flashdata('error');?>
			  	</div>
			  	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			    	<span aria-hidden="true">&times;</span>
			  	</button>
			</div>

		<?php endif; ?>

		</div>

		<!-- Log Hours Card -->
      	<div class="card shadow mb-4">

	        <!-- Card Header -->
	        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
	          	<h6 class="m-0 font-weight-bold text-primary">Log Daily Hours</h6>
	        </div>

	        <!-- Card Body -->
	        <div class="card-body">

	        	<!-- Log hours Form -->
	          	<?=form_open('hours/validate_store_redirect', 'id="hours_form"');?>

	          		<input name="hours_date" type="hidden" value="<?=$date_today;?>">

	          		<div class="form-group">
	          			<label for="category">Category</label>
				    	<select class="form-control custom-select" id="category" name="category" required>
				    		<option value="">Select a Job</option>
			    		<?php foreach ($jobs as $job): ?>
			    			<option><?=$job->category; ?></option>
			    		<?php endforeach; ?>
							<option value="zero">Zero Out</option>
						</select>
	          		</div>
	          		<div class="form-group">
	          			<label for="hours">Hours</label>
	    				<input type="text" class="form-control" id="hours" name="hours" value="" required>
	          		</div>
	          		<div class="form-group">
	          			<label for="cleanroom_hours">Cleanroom Hours</label>
		    			<input type="text" class="form-control" id="cleanroom_hours" name="cleanroom_hours" value="">
	          		</div>
	          		<div class="form-group">
	          			<label for="description" class="">Description</label>
			    		<textarea class="form-control" rows="2" id="description" name="description" required></textarea>
	          		</div>
	          		<div class="form-group">
	          			<button type="submit" class="btn btn-primary btn-icon-split">
		                    <span class="icon text-white-50">
		                      	<i class="fas fa-arrow-right"></i>
		                    </span>
		                    <span class="text">Submit</span>
		                </button>
	          		</div>
	          	<?=form_close()?>

	        </div>

      	</div>

    </div>


    <div class="col-xl-6 col-lg-6">

    	<div id="message-right">

		<?php if (!empty($this->session->flashdata('message'))): ?>

			<!-- User Alert -->
			<div class="alert alert-success alert-dismissible fade show" role="alert">
			  	<strong>You did it!</strong>
			  	<div>
			  		<?=$this->session->flashdata('message');?>
			  	</div>
			  	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			    	<span aria-hidden="true">&times;</span>
			  	</button>
			</div>

		<?php endif; ?>

		</div>

		<!-- Daily Hours Card -->
    	<div class="card shadow mb-4">

	        <!-- Card Header - Dropdown -->
	        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
	          	<h6 class="m-0 font-weight-bold text-primary">Today's Hours Logged: <?=$daily_hours_count[0]->hours;?></h6>
	        </div>

	        <!-- Card Body -->
	        <div class="card-body">

        	<?php if ($daily_hours_count[0]->hours == 0 && count($daily_hours_category) == 0): ?>

        		<div class="card bg-warning text-white shadow">
                	<div class="card-body">
                		<h4>You have not logged any hours today!</h4>
                	</div>
             	</div>

			<?php else: ?>

				<?php foreach ($daily_hours_category as $category): ?>

				<div class="card border-left-success shadow h-100 py-2 mb-2">
            		<div class="card-body py-1">
             			<div class="row no-gutters align-items-center">
                			<div class="col mr-2">
                				<div class="h5 mb-0 font-weight-bold text-gray-800"><?=$category->category;?>: <?=$category->hours;?></div>
            				<?php if ($category->cleanroom_hours > 0): ?>
            					<div class="text-s font-weight-bold mb-1">Cleanroom: <?=$category->cleanroom_hours;?></div>
            				<?php endif; ?>
                  				<div class="text-s font-weight-bold text-primary mb-1"><?=$category->description;?></div>
                			</div>
                			<div class="col-auto">
                				<a class="hours-edit" data-hours-id="<?=$category->id;?>" data-date-to-edit="<?=$date_today;?>" data-category="<?=$category->category;?>" data-hours="<?=$category->hours;?>" data-cleanroom-hours="<?=$category->cleanroom_hours;?>" data-description="<?=htmlspecialchars($category->description);?>" href="javascript:void(0);"><i class="fas fa-edit "></i></a>
			                </div>
              			</div>
            		</div>
          		</div>

				<?php endforeach; ?>


			<?php endif; ?>

	        </div>
	    </div>
    </div>
</div>

<div class="row">
	<!-- Today's Date Card -->
    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card shadow h-100 py-1">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      	<div class="text-xs font-weight-bold text-primary text-uppercase mb-1"><?=date('Y');?> Paid Leave Taken</div>
                      	<div class="h5 mb-0 font-weight-bold text-gray-800"><?=($user_yearly_paid_leave > 0) ? $user_yearly_paid_leave: '0'?> hours</div>
                    </div>
                    <div class="col-auto">
						<i class="fas fa-suitcase fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Period -->
    <div class="col-xl-3 col-md-6 mb-4 d-none d-sm-none d-md-block">
        <div class="card shadow h-100 py-1">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      	<div class="text-xs font-weight-bold text-primary text-uppercase mb-1"><?=date('Y');?> Sick Leave Taken</div>
                      	<div class="h5 mb-0 font-weight-bold text-gray-800"><?=($user_yearly_sick_leave > 0) ? $user_yearly_sick_leave: '0'?> hours</div>
                    </div>
                    <div class="col-auto">
						<i class="fas fa-temperature-high fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Dashboard Hours Edit Modal-->
<div class="modal fade" id="hoursEditModal" tabindex="-1" role="dialog" aria-labelledby="hoursEditModalLabel" aria-hidden="true">

	<div class="modal-dialog" role="document">

	  	<div class="modal-content">

		   	<div class="modal-header">
				<h5 class="modal-title" id="hoursEditModalLabel"></h5>
				<button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
		    </div>

		    <div class="modal-body">

		    	<form id="hours-edit-form">
			    	<input id="hours_date_modal" name="hours_date" type="hidden" value="">

			    	<input id="category_modal" name="category" type="hidden" value="">

	          		<div class="form-group">
	          			<label for="hours">Hours</label>
	    				<input type="text" class="form-control" id="hours_modal" name="hours" value="" required>
	          		</div>
	          		<div class="form-group">
	          			<label for="cleanroom_hours">Cleanroom Hours</label>
		    			<input type="text" class="form-control" id="cleanroom_hours_modal" name="cleanroom_hours" value="">
	          		</div>
	          		<div class="form-group">
	          			<label for="description" class="">Description</label>
			    		<textarea class="form-control" rows="2" id="description_modal" name="description" required></textarea>
	          		</div>
	          	</form>

			</div>

		    <div class="modal-footer">

				<a href="#" id="discard-hours" class="text-danger mr-auto"><i class="fas fa-trash-alt"></i></a>

		      	<button class="btn btn-secondary " type="button" data-dismiss="modal">Cancel</button>

		      	<a id="hours-edit-submit-modal" class="btn btn-primary btn-icon-split save-changes" href="javascript:void(0);" style="display:none;">
		      		<span class="icon text-white-50">
	                  	<i class="fas fa-arrow-right"></i>
	                </span>
	                <span class="text">Save Changes</span>
		      	</a>

		    </div>

		</div>

	</div>

</div>

<!-- Page specific JS -->
<script type="text/javascript">

	/*** GLOBAL Variables ***/

	//get base_url for use in AJAX calls
	var baseUrl = <?php echo json_encode(base_url()); ?>;
	//get today's date
	var dateToday = <?php echo json_encode($date_today); ?>;

	//wait for the DOM to load
	$(document).ready(function () {

		TimesheetApp.Hours.displayEditHoursModal()

		TimesheetApp.Hours.submitEditHoursModalForm(baseUrl)

		TimesheetApp.Hours.toggleModalSaveChangesButton('#hoursEditModal')

		TimesheetApp.Hours.zeroOutFormHours()

		//onclick alert user to discard action
        $('a#discard-hours').on('click', function (e) {

            //if the user does not confirm, prevent the link navigation
            if (!confirm('You are about to discard These hours. This action cannot be undone. Do you wish to continue?')) {
                e.preventDefault();
            }

        })

	})

</script>
