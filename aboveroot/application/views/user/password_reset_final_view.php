<div class="container text-center">

        <?=form_open('user/password_reset_final', 'class="form-signin border rounded"')?>
        <?=isset($_SESSION['auth_message']) ? '<div class="alert alert-danger" role="alert">' . $_SESSION['auth_message'] .'</div>'  : FALSE?>
        <i class="fas fa-fw fa-door-open fa-4x text-gray-100"></i>
        <h1 class="h4 mt-2 mb-3 text-gray-100 font-weight-normal">Timesheet Password Reset</h1>
        <!-- error reporting -->

        <?php if (validation_errors()): ?>

            <!-- User Alert -->
			<div class="alert alert-danger alert-dismissible fade show" role="alert">

			  	<?=validation_errors();?>

			  	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			    	<span aria-hidden="true">&times;</span>
			  	</button>
			</div>

        <?php endif; ?>

        <?php if (!empty($this->session->flashdata('error'))): ?>

			<!-- User Alert -->
			<div class="alert alert-danger alert-dismissible fade show" role="alert">
			  	<strong>Uh oh! You should check on some of those fields below.</strong>
			  	<div>
			  		<?=$this->session->flashdata('error');?>
			  	</div>
			  	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			    	<span aria-hidden="true">&times;</span>
			  	</button>
			</div>

		<?php endif; ?>

        <?php if (!empty($this->session->flashdata('message'))): ?>

			<!-- User Alert -->
			<div class="alert alert-danger alert-dismissible fade show" role="alert">

			  	<?=$this->session->flashdata('message');?>

			  	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			    	<span aria-hidden="true">&times;</span>
			  	</button>
			</div>

		<?php endif; ?>

        <p class="text-gray-100 font-weight-normal">Enter a new password</p>
        <p class="text-gray-100 font-weight-light">8-20 characters</p>
        <input name="forgotten_password_code" type="hidden" value="<?=$reset_code;?>">
        <?=form_label('Password:', 'password', 'class="sr-only"')?>
        <?=form_password('password', '' ,'class="form-control" placeholder="Password" required')?>
        <?=form_label('Confirm password','confirm_password','class="sr-only"')?>
        <?=form_password('confirm_password', '' ,'class="form-control" placeholder="Confirm Password" required'). '<br/>'?>
        <?=form_submit('submit','Submit', 'class="btn btn-lg btn-secondary btn-block text-gray-100 my-3"')?>
        <?=form_close()?>

</div>
