<div class="container text-center">

    <?php if($code_status == 'active'): ?>
        <?=form_open('user/password_reset_confirm', 'class="form-signin border rounded"')?>
        <?=isset($_SESSION['auth_message']) ? '<div class="alert alert-danger" role="alert">' . $_SESSION['auth_message'] .'</div>'  : FALSE?>
        <i class="fas fa-fw fa-key fa-4x text-gray-100"></i>
        <h1 class="h4 mt-2 mb-3 text-gray-100 font-weight-normal">Timesheet Password Reset</h1>
        <!-- error reporting -->
        <?php if (!empty($this->session->flashdata('error'))): ?>

			<!-- User Alert -->
			<div class="alert alert-danger alert-dismissible fade show" role="alert">
			  	<strong>Uh oh! You should check on some of those fields below.</strong>
			  	<div>
			  		<?=$this->session->flashdata('error');?>
			  	</div>
			  	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			    	<span aria-hidden="true">&times;</span>
			  	</button>
			</div>

		<?php endif; ?>

        <?php if (!empty($this->session->flashdata('message'))): ?>

			<!-- User Alert -->
			<div class="alert alert-danger alert-dismissible fade show" role="alert">

			  	<?=$this->session->flashdata('message');?>

			  	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			    	<span aria-hidden="true">&times;</span>
			  	</button>
			</div>

		<?php endif; ?>

        <p class="text-gray-100 font-weight-normal">Enter your email to reset your password:</p>
        <input name="forgotten_password_code" type="hidden" value="<?=$reset_code;?>">
        <?=form_label('Email:','email', 'class="sr-only"')?>
        <?=form_error('email')?>
        <?=form_input('email', '' ,'class="form-control" id="email" placeholder="Email address" required')?>
        <?=form_submit('submit','Submit', 'class="btn btn-lg btn-secondary btn-block text-gray-100 my-3"')?>
        <?=form_close()?>
    <?php endif; ?>

    <?php if($code_status == 'expired'): ?>
        <h1 class="h3 mb-3 text-gray-100 font-weight-normal"><i class="fas fa-fw fa-key text-gray-100"></i> Invalid Reset Code</h1>
        <p class="text-gray-100 font-weight-normal">Please contact the Web Administrator</p>
    <?php endif; ?>

</div>
