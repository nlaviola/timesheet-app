<div class="container text-center">

    <?=form_open('user/login', 'class="form-signin"')?>
    <i class="far fa-fw fa-clock fa-4x text-gray-100"></i>
    <?=isset($_SESSION['auth_message']) ? '<div class="alert alert-danger" role="alert">' . $_SESSION['auth_message'] .'</div>'  : FALSE?>
    <?=isset($_SESSION['password_reset_message']) ? '<div class="alert alert-success my-3" role="alert"><i class="far fa-fw fa-thumbs-up fa-2x"></i> Password successfully reset!</div>'  : FALSE?>
    <h1 class="h3 mb-3 text-gray-100 font-weight-normal">Timesheet Login</h1>
    <?=form_label('Email:','email', 'class="sr-only"')?>
    <?=form_error('email')?>
    <?=form_input('email', '' ,'class="form-control" id="email" placeholder="Email address" required autofocus')?>
    <?=form_label('Password:', 'password', 'class="sr-only"')?>
    <?=form_error('password')?>
    <?=form_password('password', '' ,'class="form-control" placeholder="Password" required')?>
    <div class="checkbox my-3 text-gray-100">
    	<?=form_checkbox('remember','1',FALSE).' Remember me<br />'?>
    </div>
    <?=form_submit('submit','Log In', 'class="btn btn-lg btn-secondary btn-block text-gray-100"')?>
    <p class="mt-4 mb-3 text-gray-100">&copy; 2001-<?=date('Y')?></p>
    <?=form_close()?>

</div>
