<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-2">
	<h1 class="h3 mb-0 text-gray-800"><?=($this->uri->segment(2)=='current') ? '<i class="fas fa-fw fa-calendar-week"></i> Current' : '';?><?=($this->uri->segment(2)=='previous') ? '<i class="fas fa-fw fa-history"></i> Previous' : '';?> | <?=date('M d', strtotime($payperiod_start_date));?> - <?=date('M d', strtotime($payperiod_end_date));?></h1>
</div>

<!-- Breadcrumb -->
<nav aria-label="breadcrumb">
	<ol class="breadcrumb bg-transparent mb-0">
	<li class="breadcrumb-item"><a href="/payperiod/links/<?=date('Y', strtotime($payperiod_start_date));?>"><?=date('Y', strtotime($payperiod_start_date));?></a></li>
	<li class="breadcrumb-item active" aria-current="page"><?=date('M d', strtotime($payperiod_start_date));?> - <?=date('M d', strtotime($payperiod_end_date));?></li>
	</ol>
</nav>

<!-- Content Row -->
<div class="row">

	<!-- Legend Card -->
	<div class="col-xl-3 col-l-4 col-md-6 mb-2 d-none d-sm-none d-md-block">
		<div class="card shadow h-100 py-2">
            <div class="card-body py-1">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                    	<div><a href="#" class="btn btn-success btn-sm"></a><span class="text-gray-800"> approved</span></div>
                      	<div><a href="#" class="btn btn-warning btn-sm"></a><span class="text-gray-800"> needs approval</span></div>
                      	<div><a href="#" class="btn btn-danger btn-sm"></a><span class="text-gray-800"> missing hours</span></div>
                    </div>

                </div>
            </div>
        </div>
	</div>

	<!-- Working Days -->
    <div class="col-xl-3 col-md-6 mb-2 d-none d-sm-none d-md-block">
        <div class="card shadow h-100 py-2">
            <div class="card-body py-3">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      	<div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Working Days in Pay Period</div>
                      	<div class="h5 mb-0 font-weight-bold text-gray-800"><?=$working_days_in_payperiod_count;?></div>
                    </div>
                    <div class="col-auto">
                    	<i class="fas fa-sun fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>


<?php if (count($missing_days) > 0): ?>
	<!-- Missing Days -->
    <div class="col-xl-3 col-md-6 mb-2">
        <div class="card border-left-danger shadow h-100 py-2">
            <div class="card-body py-3">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      	<div class="text-xs font-weight-bold text-danger text-uppercase mb-1">Missing days!</div>
                      	<div class="h5 mb-0 font-weight-bold text-gray-800"><?=count($missing_days);?></div>
                    </div>
                    <div class="col-auto">
                    	<i class="fas fa-skull-crossbones fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>

</div>

<!-- Content Row -->
<div class="row">

	<div class="col">

		<div id="message">

		<?php if (!empty($this->session->flashdata('message'))): ?>

			<!-- Alert Message -->
			<div class="alert alert-success alert-dismissible fade show" role="alert">
			  	<strong>You did it!</strong>
			  	<div>
			  		<?=$this->session->flashdata('message');?>
			  	</div>
			  	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			    	<span aria-hidden="true">&times;</span>
			  	</button>
			</div>

		<?php endif; ?>

		</div>

	</div>


</div>

<!-- Content Row -->
<div class="row">

	<!-- Daily Hours Cards -->
	<div class="col">

	<?php foreach ($user_payperiod_data as $date => $data): ?>


		<?php if ($date > date('Y-m-d')): ?>
			<!-- Disabled Card -->
			<div class="card shadow mb-1">

			    <!-- Card Header - Accordion -->
			    <div class="d-block card-header py-3">
			      	<h6 class="m-0 text-gray-500"><strong><?=$date;?></strong> Hours: <strong><?=(isset($data['stats']) ? $data['stats']['total_hours'] : 0 ); ?></strong> <?= (date('N', strtotime($date)) > 5 ? 'Weekend' : '') ;?></h6>
			    </div>

			</div>

		<?php else: ?>

			<!-- Interactive Card -->
			<div class="card shadow mb-1">
			    <!-- Card Header - Accordion -->
			    <a href="#collapseCard<?=$date;?>" class="d-block card-header <?=(!isset($data['stats']) && date('N', strtotime($date)) < 6 && $date < date('Y-m-d') ? 'border-left-danger' : ''); ?><?=(isset($data['stats']) && $data['stats']['unapproved_hours'] == 1 ? 'border-left-warning' : ''); ?><?=(isset($data['stats']) && date('N', strtotime($date)) < 6 && $data['stats']['unapproved_hours'] == 0 ? 'border-left-success' : ''); ?> <?= (date('N', strtotime($date)) > 5 ? 'border-left-secondary' : '') ;?> py-3 collapsed" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="collapseCard<?=$date;?>">
			      	<h6 class="m-0 text-gray-800"><strong><?=$date;?></strong> Hours: <strong><?=(isset($data['stats']) ? $data['stats']['total_hours'] : 0 ); ?></strong><?=(isset($data['stats']['total_cleanroom_hours']) && $data['stats']['total_cleanroom_hours'] > 0 ? ' | cleanroom: <strong>' .$data['stats']['total_cleanroom_hours'] . '</strong>' : '' ); ?> <?= (date('N', strtotime($date)) > 5 ? 'Weekend' : '') ;?> <?=(!isset($data['stats']) && date('N', strtotime($date)) < 6 && $date < date('Y-m-d') ? ' <i class="fas fa-fw fa-skull-crossbones fa-lg text-danger"></i>' : ''); ?></h6>
			    </a>
			    <!-- Card Content - Collapse -->
			    <div class="collapse" id="collapseCard<?=$date;?>" style="">
			      	<div class="card-body py-1">

					<?php if (date("Y-m-d", strtotime($end_date_buffer)) >= date("Y-m-d", strtotime($drop_date))): ?>

			      		<div class='my-2 text-gray-800'><a class="add-hours" data-date-to-add="<?=$date;?>" href="javascript:void(0);"><i class="far fa-fw fa-plus-square fa-lg"></i></a></div>

					<?php endif; ?>

		      		<?php if (isset($data['stats'])): ?>

				       	<?php foreach ($data['hours'] as $entry): ?>

				       		<div class="card <?=($entry->is_approved == 0 ? 'border-left-warning' : 'border-left-success'); ?> shadow h-100 py-2 mb-2">
		                		<div class="card-body py-1">
		                 			<div class="row no-gutters align-items-center">
		                    			<div class="col mr-2">
		                    				<div class="h6 mb-0 font-weight-bold text-gray-800"><?=$entry->category;?>: <?=$entry->hours;?> <?=($entry->is_approved == 0 ? '<i class="fas fa-fw fa-exclamation-triangle text-warning"></i>' : ''); ?></div>
		                    				<?php if ($entry->cleanroom_hours > 0): ?>
		                    					<div class="text-s font-weight-bold mb-1">Cleanroom: <?=$entry->cleanroom_hours;?></div>
		                    				<?php endif; ?>
		                      				<div class="text-s text-secondary mb-1"><?=$entry->description;?></div>
		                    			</div>
		                    			<div class="col-auto">
											<?php if ($payperiod_start_date > date("Y-m-d", strtotime('-1 months', strtotime($current_payperiod_end_date)))): ?>
		                    					<a class="hours-edit" data-hours-id="<?=$entry->id;?>" data-date-to-edit="<?=$entry->hours_date;?>" data-category="<?=$entry->category;?>" data-hours="<?=$entry->hours;?>" data-cleanroom-hours="<?=$entry->cleanroom_hours;?>" data-description="<?=htmlspecialchars($entry->description);?>" href="javascript:void(0);"><i class="fas fa-edit "></i></a>
											<?php endif; ?>
						                </div>
		                  			</div>
		                		</div>
		              		</div>

				       	<?php endforeach; ?>

					<?php endif; ?>

			      	</div>
			    </div>
			</div>

		<?php endif; ?>

	<?php endforeach; ?>

	</div>
</div>

<!-- Content Row -->
<div class="row justify-content-center">
	<div class="col-xl-9">
		<h1 class="h5 m-2 text-gray-800 text-center font-weight-bold">Hours</h1>
		<div id="user-hours-chart" class="px-3"></div>
	</div>
	<div class="col-xl-9">
		<h1 class="h5 m-2 text-gray-800 text-center font-weight-bold">Cleanroom Hours</h1>
		<div id="user-cleanroom-hours-chart" class="px-3"></div>
	</div>
</div>

<!-- Payperiod Hours Edit Modal-->
<div class="modal fade" id="hoursEditModal" tabindex="-1" role="dialog" aria-labelledby="hoursEditModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">

		<div class="modal-content">

		    <div class="modal-header">
			      <h5 class="modal-title" id="hoursEditModalLabel"></h5>
			      <button class="close" type="button" data-dismiss="modal" aria-label="Close">
			        	<span aria-hidden="true">×</span>
			      </button>
		    </div>

		    <div class="modal-body">

		    	<form id="hours-edit-form">
			    	<input id="hours_date_modal" name="hours_date" type="hidden" value="">

			    	<input id="category_modal" name="category" type="hidden" value="">

	          		<div class="form-group">
	          			<label for="hours">Hours</label>
	    				<input type="text" class="form-control" id="hours_modal" name="hours" value="" required>
	          		</div>
	          		<div class="form-group">
	          			<label for="cleanroom_hours">Cleanroom Hours</label>
		    			<input type="text" class="form-control" id="cleanroom_hours_modal" name="cleanroom_hours" value="">
	          		</div>
	          		<div class="form-group">
	          			<label for="description" class="">Description</label>
			    		<textarea class="form-control" rows="2" id="description_modal" name="description" required></textarea>
	          		</div>
	          	</form>

			</div>

		    <div class="modal-footer">

				<?php if ($payperiod_start_date == $current_payperiod_start_date): ?>

					<a href="#" id="discard-hours" class="text-danger mr-auto"><i class="fas fa-trash-alt"></i></a>

				<?php endif; ?>

		      	<button class="btn btn-secondary " type="button" data-dismiss="modal">Cancel</button>

		      	<a id="hours-edit-submit-modal" class="btn btn-primary btn-icon-split save-changes" href="javascript:void(0);" style="display:none;">
		      		<span class="icon text-white-50">
                      	<i class="fas fa-arrow-right"></i>
                    </span>
                    <span class="text">Save Changes</span>
		      	</a>
		    </div>

		</div>

	</div>
</div>

<!-- Payperiod Add Hours Modal-->
<div class="modal fade" id="addHoursModal" tabindex="-1" role="dialog" aria-labelledby="addHoursModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">

	  	<div class="modal-content">

		    <div class="modal-header">
			      <h5 class="modal-title" id="addHoursModalLabel"></h5>
			      <button class="close" type="button" data-dismiss="modal" aria-label="Close">
			        	<span aria-hidden="true">×</span>
			      </button>
		    </div>

		    <div class="modal-body">

		    	<!-- Success message -->
		    	<div id="add-success"></div>

		    	<!-- Log hours Form -->
	          	<form id="add_hours_form">

	          		<input id="add_hours_date_modal" name="hours_date" type="hidden" value="">

				    <input id="form_type" name="form_type" type="hidden" value="add_hours_form">

	          		<div class="form-group">
	          			<label for="category">Category</label>
				    	<select class="form-control custom-select" id="category" name="category" required>
				    		<option value="">Select a Job</option>
			    		<?php foreach ($jobs as $job): ?>
			    			<option><?=$job->category; ?></option>
			    		<?php endforeach; ?>
							<option value="zero">Zero Out</option>
						</select>
	          		</div>
	          		<div class="form-group">
	          			<label for="hours">Hours</label>
	    				<input type="text" class="form-control" id="hours" name="hours" value="" required>
	          		</div>
	          		<div class="form-group">
	          			<label for="cleanroom_hours">Cleanroom Hours</label>
		    			<input type="text" class="form-control" id="cleanroom_hours" name="cleanroom_hours" value="">
	          		</div>
	          		<div class="form-group">
	          			<label for="description" class="">Description</label>
			    		<textarea class="form-control" rows="2" id="description" name="description" required></textarea>
	          		</div>
	          		<div class="form-group">
	          			<button id="add-hours-submit-modal" type="submit" class="btn btn-primary btn-icon-split">
		                    <span class="icon text-white-50">
		                      	<i class="fas fa-arrow-right"></i>
		                    </span>
		                    <span class="text text-white">Submit</span>
		                </button>
	          		</div>
	          	</form>

			</div>

		</div>

	</div>
</div>

<script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>

<!-- Page specific JS -->
<script type="text/javascript">

	/*** GLOBAL Variables ***/

	//get base_url for use in AJAX calls
	let baseUrl = <?php echo json_encode(base_url()); ?>;
	//get today's date
	let dateToday = <?php echo json_encode($date_today); ?>;
	//get the current period start and end dates
	let startDate = <?php echo json_encode($payperiod_start_date); ?>;
	let endDate = <?php echo json_encode($payperiod_end_date); ?>;

	//wait for the DOM to load
	$(document).ready(function () {

		TimesheetApp.Hours.displayEditHoursModal()

		TimesheetApp.Hours.submitEditHoursModalForm(baseUrl)

		TimesheetApp.Hours.displayAddHoursModal()

		TimesheetApp.Hours.submitAddHoursModalForm(baseUrl)

		TimesheetApp.Hours.toggleModalSaveChangesButton('#hoursEditModal')

		TimesheetApp.Hours.zeroOutFormHours()

		//onclick alert user to discard action
        $('a#discard-hours').on('click', function (e) {

            //if the user does not confirm, prevent the link navigation
            if (!confirm('You are about to discard These hours. This action cannot be undone. Do you wish to continue?')) {
                e.preventDefault();
            }

        })

		prepUserChartData(baseUrl, startDate, endDate)


		//Apex Chart of user hours in payperiod
		function prepUserChartData(baseUrl, startDate, endDate){

			var userHours

            //post call for hours data
            $.get(baseUrl + 'data/get_user_payperiod_hours_ajax/' + startDate+ '/' + endDate)

            .then(function(data){

				//parse data
	      		let userHoursInformation = JSON.parse(data)

				//initialize variables
				let hoursChartData = []
				let hoursLabels = []
				let cleanroomHoursChartData = []
				let cleanroomHoursLabels = []

				//evaluate hours for pay period
				if (userHoursInformation.user_hours.length > 0) {

					//build arrays for chart data
					$.each(userHoursInformation.user_hours, function(key, value){

						hoursChartData.push(value.hours)
						hoursLabels.push(value.category)

					})

					//call function to draw charts
					TimesheetApp.Utilities.drawApexChart('pie', 'user-hours-chart', hoursChartData, hoursLabels)

				} else {
					$('#user-hours-chart').html('<p class="text-center">No recorded hours</p>')
				}

				if (userHoursInformation.user_cleanroom_hours.length > 0) {

					//build arrays for chart data
					$.each(userHoursInformation.user_cleanroom_hours, function(key, value){

						cleanroomHoursChartData.push(value.cleanroom_hours)
						cleanroomHoursLabels.push(value.category)

					})

					//call function to draw charts
					TimesheetApp.Utilities.drawApexChart('pie', 'user-cleanroom-hours-chart', cleanroomHoursChartData, cleanroomHoursLabels)

				} else {
					$('#user-cleanroom-hours-chart').html('<p class="text-center">No recorded cleanroom hours</p>')
				}

            })

        }

	})

</script>
