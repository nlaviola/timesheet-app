<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
	<h1 class="h3 mb-0 text-gray-800"><i class="fas fa-fw fa-info-circle"></i> Job Codes</h1>
</div>

<!-- Content Row -->
<div class="row">

	<!-- Manage Active Jobs -->
    <div class="col-12">

        <!-- Manage Active Jobs Card -->
      	<div class="card shadow mb-4">

	        <!-- Card Header -->
	        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
	          	<h6 class="m-0 font-weight-bold">Active Jobs</h6>
	        </div>

	        <!-- Card Body -->
	        <div class="card-body">


                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">Code</th>
                            <th scope="col">Description</th>
                            <th scope="col">Status</th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php foreach ($active_jobs as $job): ?>

                            <tr>
                                <td><strong><?=$job->category?></strong></td>
                                <td><?=$job->description?></td>
                                <td>Active</td>
                            </tr>

                        <?php endforeach; ?>

                    </tbody>
                </table>

            </div>

        </div><!-- END: Manage Jobs Card -->

    </div>

</div>