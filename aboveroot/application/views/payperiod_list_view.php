<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
	<h1 class="h3 mb-0 text-gray-800"><i class="fas fa-fw fa-history"></i> <?=$year;?> Period Links</h1>
</div>

<div class="row">
	<!-- Today's Date Card -->
    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card shadow h-100 py-1">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      	<div class="text-xs font-weight-bold text-primary text-uppercase mb-1"><?=$year;?> Paid Leave Taken</div>
                      	<div class="h5 mb-0 font-weight-bold text-gray-800"><?=($user_yearly_paid_leave > 0) ? $user_yearly_paid_leave: '0'?> hours</div>
                    </div>
                    <div class="col-auto">
						<i class="fas fa-suitcase fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Period -->
    <div class="col-xl-3 col-md-6 mb-4 d-none d-sm-none d-md-block">
        <div class="card shadow h-100 py-1">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      	<div class="text-xs font-weight-bold text-primary text-uppercase mb-1"><?=$year;?> Sick Leave Taken</div>
                      	<div class="h5 mb-0 font-weight-bold text-gray-800"><?=($user_yearly_sick_leave > 0) ? $user_yearly_sick_leave: '0'?> hours</div>
                    </div>
                    <div class="col-auto">
						<i class="fas fa-temperature-high fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php foreach ($payperiod_ranges as $quarter => $payperiods): ?>
	<!-- Quarter Section -->
	<div class="row">

		<div class="col-12">
			<h1 class="h4 mb-2 text-gray-800"><?=$quarter;?></h1>
		</div>

	<?php foreach ($payperiods as $date_ranges): ?>

		<?php if (date('Y-m-d', strtotime($date_ranges['start'])) == date('Y-m-d', strtotime($current_payperiod_start_date))): ?>

			<!-- current Payperiod Button Link -->
		    <div class="col-xl-6 col-md-6 mb-4">
		        <a href="/payperiod/current/<?=$date_ranges['start'];?>/<?=$date_ranges['end'];?>" class="btn btn-primary btn-block"><?=date('F jS', strtotime($date_ranges['start'])) . ' <i class="fas fa-long-arrow-alt-right"></i> ' . date('F jS', strtotime($date_ranges['end']));?></a>
		    </div>

		<?php else: ?>

			<!-- Previous Payperiod Button Link -->
		    <div class="col-xl-6 col-md-6 mb-4">
		        <a href="/payperiod/previous/<?=$date_ranges['start'];?>/<?=$date_ranges['end'];?>" class="btn btn-secondary btn-block"><?=date('F jS', strtotime($date_ranges['start'])) . ' <i class="fas fa-long-arrow-alt-right"></i> ' . date('F jS', strtotime($date_ranges['end']));?></a>
		    </div>

	    <?php endif; ?>

	<?php endforeach; ?>

	</div>

<?php endforeach; ?>

<hr />

<div class="row justify-content-center">

	<div class="col-12">
		<h1 class="h4 mb-2 text-gray-800"><i class="fas fa-fw fa-chart-pie"></i> Data</h1>
	</div>

	<div class="col-xl-9">
		<h1 class="h5 m-2 text-gray-800 text-center font-weight-bold"><?=$year;?> Hours</h1>
		<div id="user-yearly-hours-chart" class="px-3"></div>
	</div>
	<div class="col-xl-9">
		<h1 class="h5 m-2 text-gray-800 text-center font-weight-bold"><?=$year;?> Cleanroom Hours</h1>
		<div id="user-yearly-cleanroom-hours-chart" class="px-3"></div>
	</div>

</div>

<script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>

<script type="text/javascript">

	/*** GLOBAL Variables ***/

	//get base_url for use in AJAX calls
	let baseUrl = <?php echo json_encode(base_url()); ?>;

	//get the current year start and end date
	let startDate = <?php echo json_encode(date($year . '-01-01')); ?>;
	let endDate = <?php echo json_encode(date($year . '-12-31')); ?>;

	let year = <?php echo json_encode($year); ?>;

	console.log(year)

	console.log(startDate + ' ' + endDate)


	//wait for the DOM to load
	$(document).ready(function () {

		prepUserChartData(baseUrl, startDate, endDate)


		//Apex Chart of user hours in payperiod
		function prepUserChartData(baseUrl, startDate, endDate){

			var userHours

			//post call for hours data
			$.get(baseUrl + 'data/get_user_payperiod_hours_ajax/' + startDate+ '/' + endDate)

			.then(function(data){

				//parse data
				let userHoursInformation = JSON.parse(data)

				//initialize variables
				let hoursChartData = []
				let hoursLabels = []
				let cleanroomHoursChartData = []
				let cleanroomHoursLabels = []

				//evaluate hours for pay period
				if (userHoursInformation.user_hours.length > 0) {

					//build arrays for chart data
					$.each(userHoursInformation.user_hours, function(key, value){

						hoursChartData.push(value.hours)
						hoursLabels.push(value.category)

					})

					//call function to draw charts
					TimesheetApp.Utilities.drawApexChart('pie', 'user-yearly-hours-chart', hoursChartData, hoursLabels)

				} else {
					$('#user-hours-chart').html('<p class="text-center">No recorded hours</p>')
				}

				if (userHoursInformation.user_cleanroom_hours.length > 0) {

					//build arrays for chart data
					$.each(userHoursInformation.user_cleanroom_hours, function(key, value){

						cleanroomHoursChartData.push(value.cleanroom_hours)
						cleanroomHoursLabels.push(value.category)

					})

					//call function to draw charts
					TimesheetApp.Utilities.drawApexChart('pie', 'user-yearly-cleanroom-hours-chart', cleanroomHoursChartData, cleanroomHoursLabels)

				} else {
					$('#user-cleanroom-hours-chart').html('<p class="text-center">No recorded cleanroom hours</p>')
				}

			})

		}

	})

</script>
