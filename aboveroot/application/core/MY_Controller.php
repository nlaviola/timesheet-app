<?php
  defined('BASEPATH') OR exit('No direct script access allowed');

//all controllers should now extend MY_controller
class MY_Controller extends CI_Controller {
    protected $data = array();
    function __construct(){
      parent::__construct();

      //load in URL helper
      //used in all views that render the master header
      $this->load->helper('url');

      //load in File helper
      //used to auto generate js filenames using MY_file_helper.php to get around caching issues when changes are made
      $this->load->helper('file');

      //initalize data array in the event a model fails and data is not initialized in a controller method
      $this->data['pagetitle'] = 'Praevium';
    }

	/**
	 * renders a view from header, content, and footer
	 *
	 * @param       string 	$the_view 	Input string
	 * @param       string 	$template 	Input string
	 * @param       string 	$run 		Input string
	 *
	 */
    protected function render($the_view = NULL, $template = 'public_master'){
      function __construct() {
        parent::__construct();
      }
      //if we specify NULL for the template, the function will just render the view by itself
      if(is_null($template)){
        $this->load->view($the_view,$this->data);
      } else {
        //render the view along with the header and footer
        $this->data['the_view_content'] = (is_null($the_view)) ? '' : $this->load->view($the_view,$this->data, TRUE);
        $this->load->view('templates/'.$template.'_view', $this->data);
      }
    }
}

class Auth_Controller extends MY_Controller {
    function __construct() {
        parent::__construct();

        // loads authentication content
        $this->load->library('ion_auth');

        // loads user library to track user
        $this->load->library('user_agent');

        // Saves where user is prior to logout due to timeout
        $this->session->set_userdata('redirect_back', $this->agent->referrer() );

        if($this->ion_auth->logged_in()===FALSE)
        {
            redirect('user/login');
        } else {
        	//store the username for the current session to be used in master_header_view
        	$this->data['current_user'] = $this->ion_auth->user()->row()->first_name . " " . $this->ion_auth->user()->row()->last_name;
        }

    }

    //pull in the render function from the parent class
    protected function render($the_view = NULL, $template = 'auth_master')
    {
        parent::render($the_view, $template);
    }

    //restrict access to an array of methods for users not in given group(s)
    protected function restrict_access($protected_methods = NULL, $groups)
    {
    	//determine if the logged in user is a public user
  		if(!$this->ion_auth->in_group($groups)){

  			//if a protected method is accessed, redirect to error page
  			if(in_array($this->router->method, $protected_methods)){

          		//redirect to an error page
          		redirect('errors/permission_error');

       		}

  		}
    }

}

class Timesheet_Controller extends Auth_Controller {
    function __construct() {
        parent::__construct();

        //Load resource(s)
        $this->load->library('payperiod_library');
        $this->load->library('timesheet_utilities_library');
        $this->load->model('hours_model');

        //set today's date for use in all methods
        $this->date_today = date('Y-m-d');

        //set period bounds for use in all methods
        if(date('d') <= 15){
            $this->current_payperiod_start_date = date('Y-m-01');
            $this->current_payperiod_end_date = date('Y-m-15');
        } else {
            $this->current_payperiod_start_date = date('Y-m-16');
            $this->current_payperiod_end_date = date('Y-m-t');
        }

        //get working days in current date period
        //payperiod library loaded in Auth Controller
        $this->working_days_in_current_payperiod = $this->payperiod_library->get_working_days($this->current_payperiod_start_date, $this->current_payperiod_end_date);

         //assign to view data variable
        $this->data['current_payperiod_start_date'] = $this->current_payperiod_start_date;
        $this->data['current_payperiod_end_date'] = $this->current_payperiod_end_date;
        $this->data['working_days_in__current_payperiod'] = $this->working_days_in_current_payperiod;


         //determine years worked by the logged in user
        //user to create navbar year links
        //query for earliest recorded hours_date from user
        $earliest_hours_date = $this->hours_model->get_earliest_hours_date($this->ion_auth->user()->row()->id);

        //if hours exist create array of years worked
        //otherwise return NULL
        if (count($earliest_hours_date) == 1) {

            $year_start = date('Y', strtotime($earliest_hours_date[0]->hours_date));

            $year_current = date('Y');

            $years_worked = [];

            while ($year_current >= $year_start) {

                $years_worked[] = $year_current;

                $year_current--;
            }
            //set variable for use in the nav bar
            $this->data['years_worked'] = $years_worked;


        } else {
            //set variable for use in the nav bar
            $this->data['years_worked'] = date('Y');
        }

    }

}
