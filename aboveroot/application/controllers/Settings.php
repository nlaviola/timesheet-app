<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends Timesheet_Controller {

	function __construct(){
     	parent::__construct();

     	$this->data['pagetitle'] = 'Timesheet - Settings';

		//assign today's date to view
		$this->data['date_today'] = $this->date_today;

        //define controllers not accessible by "public users"
     	//list of protected methods to access (for example only by admin )
		$protected_methods = get_class_methods('Settings');//returns list of all method names in class

  		//assign the group(s) to be checked
		$groups = [
				'admin',
				'supervisor'
			];

		//execute the restrict access method
		parent::restrict_access($protected_methods, $groups);

    }

    /**
	 * Index Page for this controller.
	 *
	 */
    public function index()
    {
        //load resource(s)
        $this->load->model('jobs_model');
        $this->load->helper('form');

        //query for active jobs
        $active_jobs = $this->jobs_model->get_active_jobs();
        //query for inactive jobs
        $inactive_jobs = $this->jobs_model->get_inactive_jobs();

        $this->data['active_jobs'] = $active_jobs;
        $this->data['inactive_jobs'] = $inactive_jobs;

        $this->render('admin/site_settings_view.php');

    }

    /**
	 * Adds a new Job
	 *
	 */
	public function add_job_ajax()
	{
		//display 404 page if controller accessed directly
		if (!$this->input->post())
		{
			show_404();
		}

		//load resources
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model('jobs_model'); //allows access to validation $rules

		//validate form data using rules in model
		$rules = $this->jobs_model->rules['add_job_form'];
    	$this->form_validation->set_rules($rules);

		if ($this->form_validation->run() == FALSE)
		{

			//set validation errors
    		$data['errors'] = validation_errors();

		}
		else
		{

			//sanitize form data
			$post_data = $this->input->post(NULL,TRUE);

			//prepare data array for db insert
            $job_data = [
                'category' => $post_data['category'],
                'description' => $post_data['description'],
                'is_active' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')

            ];

			//insert new user
			if($this->jobs_model->add_job($job_data))
			{

				//set flash data
				$this->session->set_flashdata('message', "<strong>" . $post_data['category'] . "</strong> successfully added to the application!");
				$data['success'] = TRUE;
	        }
			else
			{
				//set error message
				$data['errors'] = 'There was a problem with the database insertion. Please contact the Web Admin.';
			}

		}

		echo json_encode($data);

	}

    /**
	 * updates job active status
	 * sets flash data and redirects user
	 *
	 */
	public function update_job_status_ajax()
	{
		//display 404 page if controller accessed directly
		if (!$this->input->post())
		{
			show_404();
		}

        //load resource(s)
        $this->load->model('jobs_model');

		//sanitize $_POST variables
		//refer CI documentation
		//this function of the input class filters all post data through XSS
		//assign post data to array
		$post_data = $this->input->post(NULL, TRUE);

        $job_data = [
            'id' => $post_data['id'],
            'is_active' => $post_data['is_active']
        ];

		//update_job_status
		$result = $this->jobs_model->update_job_status($job_data);

		//set response data based on update result
		if($result == 1)
		{

			$response = ['success' => true];

		}
		else
		{

			$response = ['error' => 'There was a problem with the database update!'];

		}

		//Send back response data
		echo json_encode($response);

	}



}
