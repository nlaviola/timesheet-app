<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Approve_hours extends Timesheet_Controller {

	function __construct(){
     	parent::__construct();

     	$this->data['pagetitle'] = 'Timesheet - Approve Hours';

		//assign today's date to view
		$this->data['date_today'] = $this->date_today;

		
		//define controllers not accessible by "public users"
    //list of protected methods to access (for example only by admin )
		//$protected_methods = get_class_methods('Approve_hours');//returns list of all method names in class
		$protected_methods = [
			'approve_all_user_hours',
			'approve_all_user_payperiod_hours',
			'confirm_approve_hours',
			'discard_hours'

		];

		//assign the group(s) to be checked
		//Only admins can approve hours
		$groups = [
				'admin'
			];

		//execute the restrict access method
		parent::restrict_access($protected_methods, $groups);

    }

	/**
	 * Index Page for this controller.
	 *
	 */
	public function index()
	{
		show_404();
	}

	/**
     * approves all outstanding hours for a user
     *
     * @param       $user_id   input num
     *
     */
	public function approve_all_user_hours($user_id = NULL)
	{
		//load in resource(s)
		$this->load->model('hours_model');

		//get referrel URL for redirects
        $referred_from = $this->session->userdata('referred_from');

		//query for user data
		$user_info = $this->ion_auth->user($user_id)->row();

		//query for user's unapproved hours
		$user_unapproved_hours = $this->hours_model->get_user_unapproved_hours($user_id);

		$approved_hours = [];

		//loop through hours and generate array of newly approved hours for bathc inserted
		foreach ($user_unapproved_hours as $hours_data) {

			unset($hours_data['id']);
			$hours_data['entered_by'] = $this->data['current_user'];
			$hours_data['is_approved'] = 1;
			$hours_data['created_at'] = date('Y-m-d H:i:s');
			$hours_data['updated_at'] = date('Y-m-d H:i:s');

			$approved_hours[] = $hours_data;

		}

		//batch insert new hours
		$result = $this->hours_model->insert_batch_hours($approved_hours);

		//set flashdata
		if ($result >= 1)
		{

			//set flash data
			$this->session->set_flashdata('message', "<strong>" . $result . "</strong> entires approved for <strong>" . $user_info->first_name . " " . $user_info->last_name . "</strong>!");

		}
		else
		{

			//set error message
			$data['error'] = 'There was a problem with the database insertion. Please contact the Web Admin.';

		}

		//redirect from last page
		redirect($referred_from);

	}

	/**
     * approves all outstanding hours for a user
     *
     * @param       $user_id   input num
     *
     */
	public function approve_all_user_payperiod_hours($user_id = NULL, $payperiod_start_date = NULL, $payperiod_end_date = NULL)
	{

		//load in resource(s)
		$this->load->model('hours_model');

		//get referrel URL for redirects
        $referred_from = $this->session->userdata('referred_from');

		//query for user data
		$user_info = $this->ion_auth->user($user_id)->row();

		//query for user's unapproved hours
		$user_unapproved_hours = $this->hours_model->get_user_unapproved_payperiod_hours($user_id, $payperiod_start_date, $payperiod_end_date);

		$approved_hours = [];

		//loop through hours and generate array of newly approved hours for bathc inserted
		foreach ($user_unapproved_hours as $hours_data) {

			unset($hours_data['id']);
			$hours_data['entered_by'] = $this->data['current_user'];
			$hours_data['is_approved'] = 1;
			$hours_data['created_at'] = date('Y-m-d H:i:s');
			$hours_data['updated_at'] = date('Y-m-d H:i:s');

			$approved_hours[] = $hours_data;

		}

		//batch insert new hours
		$result = $this->hours_model->insert_batch_hours($approved_hours);

		//set flashdata
		if ($result >= 1)
		{

			//set flash data
			$this->session->set_flashdata('message', "<strong>" . $result . "</strong> entires approved for <strong>" . $user_info->first_name . " " . $user_info->last_name . "</strong>!");

		}
		else
		{

			//set error message
			$data['error'] = 'There was a problem with the database insertion. Please contact the Web Admin.';

		}

		//redirect from last page
		redirect($referred_from);

	}

	/**
     * approves hours by id
     *
     * @param       $hours_id   input num
     *
     */
	public function confirm_approve_hours($hours_id = NULL)
	{
		//load in resource(s)
        $this->load->model('hours_model');

		//get referrel URL for redirects
        $referred_from = $this->session->userdata('referred_from');

		//query for hours to confirm
		$unapproved_hours = $this->hours_model->get_hours($hours_id);

		//prepare data for newly approved hours
		$data = [

			'user_id' => $unapproved_hours[0]->user_id,
			'name' => $unapproved_hours[0]->name,
			'entered_by' => $this->data['current_user'],
			'category' => $unapproved_hours[0]->category,
			'hours' => $unapproved_hours[0]->hours,
			'description' => $unapproved_hours[0]->description,
			'cleanroom_hours' => $unapproved_hours[0]->cleanroom_hours,
			'created_at' => date('Y-m-d H:i:s'),
			'updated_at' => date('Y-m-d H:i:s'),
			'hours_date' => $unapproved_hours[0]->hours_date,
			'is_approved' => 1

		];

		//insert data
		$result = $this->hours_model->insert_hours($data);

		//set flashdata
		//verify row was inserted and redirect to form page
		if ($result == 1)
		{

			//set flash data
			$this->session->set_flashdata('message', $unapproved_hours[0]->category ." hours successfully approved for <strong>" . $unapproved_hours[0]->name . "</strong>!");

		}
		else
		{

			//set error message
			$data['error'] = 'There was a problem with the database insertion. Please contact the Web Admin.';

		}

		//redirect user
		redirect($referred_from);


	}

	/**
     * "discard" hours by id
	 * sets is_newest = 0, effectively removing the hours from a user's view
     *
     * @param       $hours_id   input num
     *
     */
	public function discard_hours($hours_id = NULL)
	{
		//load in resource(s)
        $this->load->model('hours_model');

		//get referrel URL for redirects
        $referred_from = $this->session->userdata('referred_from');

		//query for hours to confirm
		$unapproved_hours = $this->hours_model->get_hours($hours_id);

		//prepare data to discard hours
		$data = [

			'is_newest' => 0,
			'description' => $unapproved_hours[0]->description . ' (hours entered incorrectly and discarded by ' . $this->data['current_user'] . ')',
			'updated_at' => date('Y-m-d H:i:s')

		];

		//update hours, setting is_newest = 0
		$result = $this->hours_model->update_hours_discard($hours_id, $data);

		//set flashdata
		//verify row was updated and redirect to form page
		if ($result == 1)
		{

			//set flash data
			$this->session->set_flashdata('message', $unapproved_hours[0]->category ." hours discarded for <strong>" . $unapproved_hours[0]->name . "</strong>!");

		}
		else
		{

			//set error message
			$data['error'] = 'There was a problem with the database update. Please contact the Web Admin.';

		}

		//redirect user
		redirect($referred_from);

	}

	/**
	 * Grabs all unapproved hours, delivers them to the view, renders the view
	 *
	 */
    public function show_users_unapproved_hours()
    {
        //load in resource(s)
        $this->load->model('hours_model');

		//set the current url in session data for use when redirecting
		$this->session->set_userdata('referred_from', current_url());

        //get list of unapproved hours grouped by user
        $this->data['unapproved_hours_data'] = $this->hours_model->get_unapproved_hours_all_users();

        //render the view
		$this->render('admin/approve_hours_user_list_view');

    }

}
