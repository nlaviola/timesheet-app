<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manage_users extends Timesheet_Controller {

	function __construct(){
     	parent::__construct();

     	$this->data['pagetitle'] = 'Timesheet - Manage Users';

		//define controllers not accessible by "public users"
     	//list of protected methods to access (for example only by admin )
		$protected_methods = get_class_methods('Manage_users');//returns list of all method names in class

  		//assign the group(s) to be checked
		$groups = [
				'admin',
				'supervisor'
			];

		//execute the restrict access method
		parent::restrict_access($protected_methods, $groups);

    }

	/**
	 * Displays user profile and access levels
	 *
	 */
	public function display_user_account($user_id = null, $reset_code = null)
	{
        //load resource(s)
		$this->load->model('pay_information_model');
        $this->load->helper('form');
		$this->load->library('encryption'); //used for decrypting pay information

        //query for user information
		 $user_data = $this->ion_auth->user($user_id)->row();
		 //assign to view
		 $this->data['user'] = $user_data;

		//query for password reset code if present
		if (isset($user_data->forgotten_password_code) && isset($user_data->forgotten_password_time))
		{
			//if a valid reset code is sent in the url make the password reset link available on the page
			if (isset($reset_code))
			{
				$password_reset_check = $this->ion_auth->forgotten_password_check($reset_code);

				if ($password_reset_check)
				{
					//assign info to view
					$this->data['password_reset_status'] = 'active';
					$this->data['password_reset_link'] = base_url() . 'user/password_reset_check/' .$reset_code;
				}
				else
				{
					//assign info to view
					$this->data['password_reset_status'] = 'expired';
				}
			}
			else
			{
				//a password reset has been initiated, but the selector code is not present
				//the admin must initiate another reset action
				$this->data['password_reset_status'] = 'invalid';
			}

		}


		//query for pay information
		$pay_data = $this->pay_information_model->get_user_pay_information($user_id);

		//if the pay information is set, assign variables to the view
		if (count($pay_data) > 0)
		{
			//assign pay information to view variables
			$this->data['pay_type'] = $pay_data[0]->pay_type;
			$this->data['rate'] = $this->encryption->decrypt($pay_data[0]->rate);
		}

    	$this->render('admin/manage_user_account_view');

    }

    /**
	 * Displays site users and registration form
	 *
	 */
	public function display_user_list()
	{
        //load resource(s)
        $this->load->helper('form');

        //query for users sorted by permissions and active status
        //$this->data['elevated_users'] = $this->ion_auth->where('users.active', 1)->users(array('admin','supervisor'))->result();
		$this->data['active_users'] = $this->ion_auth->where('users.active', 1)->users()->result();
		$this->data['inactive_users'] = $this->ion_auth->where('users.active', 0)->users()->result();

    	$this->render('admin/manage_users_list_view');

    }

	/**
	 * Registers a new user
	 * Generates a forgotten_password_code to allow the user to set their own password
	 *
	 */
	public function register_user_ajax()
	{
		//display 404 page if controller accessed directly
		if (!$this->input->post())
		{
			show_404();
		}

		//load resources
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model('ion_auth_model'); //allows access to validation $rules

		//validate form data using rules in model
		$rules = $this->ion_auth_model->rules['add_user_form'];
    	$this->form_validation->set_rules($rules);

		if ($this->form_validation->run() == FALSE)
		{

			//set validation errors
    		$data['errors'] = validation_errors();

		}
		else
		{

			//sanitize form data
			$post_data = $this->input->post(NULL,TRUE);

			//prepare data array for db insert
			$username = $post_data['first_name'] . $post_data['last_name'];
		    $password = $this->timesheet_utilities_library->random_password();
		    $email = $post_data['email'];
		    $additional_data = array(
	                'first_name' => $post_data['first_name'],
	                'last_name' => $post_data['last_name'],
	    		);

			//insert new user
			if($this->ion_auth->register($username,$password,$email,$additional_data))
			{
				//generate forgotten password code and time for the user
				$password_code = $this->ion_auth->forgotten_password($email);

				//set flash data
				$this->session->set_flashdata('message', "<strong>" . $post_data['first_name'] . " " . $post_data['last_name'] . "</strong> successfully added to the application! Click on their name to initiate a <strong>Password Reset</strong> so they can log in!");
				$data['success'] = TRUE;
	        }
			else
			{
				//set error message
				$data['errors'] = 'There was a problem with the database insertion. Please contact the Web Admin.';
			}

		}

		echo json_encode($data);

	}

	/**
	 * Generates a reset code and a random password for the user
	 * Done in the event a user loses/forgets a password
	 *
	 */
	public function reset_user_password($user_id)
	{

		//query for user info
		$user = $this->ion_auth->user($user_id)->row();

		//update user with random password
		$data = [
			'password' => $this->timesheet_utilities_library->random_password()
		];
		$result = $this->ion_auth->update($user_id, $data);

		if ($result == true)
		{
			//create forgotten password code and expiration time (24hrs)
			$data = $this->ion_auth->forgotten_password($user->email);

 			//redirect back to user page
			redirect('manage_users/display_user_account/' . $user_id . '/' . $data['forgotten_password_code']);
		}
		else
		{
			//set flash data
			$this->session->set_flashdata('error', "There was a problem reseting the user password. Please contact the web Administrator.");

			//redirect back to user page
			redirect('manage_users/display_user_account/' .$user_id);
		}

	}

	/**
	 * updates user active status
	 * sets flash data and redirects user
	 *
	 */
	public function update_active_status_ajax()
	{
		//display 404 page if controller accessed directly
		if (!$this->input->post())
		{
			show_404();
		}

		//sanitize $_POST variables
		//refer CI documentation
		//this function of the input class filters all post data through XSS
		//assign post data to array
		$post_data = $this->input->post(NULL, TRUE);

		//update_user_status
		if($post_data['user_id'] != $this->ion_auth->user()->row()->id)
		{

			$result = $this->ion_auth->update($post_data['user_id'], ['active' => $post_data['active']]);

		}

		//set response data based on update result
		if($result == true)
		{

			$response = ['success' => true];

		}
		else
		{

			$response = ['error' => 'There was a problem with the database update!'];

		}

		//Send back response data
		echo json_encode($response);

	}

	/**
	 * updates user active status
	 * sets flash data and redirects user
	 *
	 */
	public function update_user_group_ajax()
	{
		//display 404 page if controller accessed directly
		if (!$this->input->post())
		{
			show_404();
		}

		//sanitize $_POST variables
		//refer CI documentation
		//this function of the input class filters all post data through XSS
		//assign post data to array
		$post_data = $this->input->post(NULL, TRUE);

		//Group|Id table
		$groups = [
			'admin' => 1,
			'members' => 2,
			'supervisor' => 3

		];

		//initialize errors as false
		$response_data['error'] = FALSE;

		//update the user's group status
		//set response data based on result
		if ($post_data['checked'] == 'true')
		{
			if ($this->ion_auth->add_to_group($groups[$post_data['access_category']], $post_data['user_id']))
			{
				$response_data['success'] = "User successfully added to " . $post_data['access_category'] . " group!";
			}
			else
			{
				$response_data['error'] = "There was a problem with this operation, please contact the Web Administrator!";
			}
		}

		if ($post_data['checked'] == 'false')
		{
			if ($this->ion_auth->remove_from_group($groups[$post_data['access_category']], $post_data['user_id']))
			{
				$response_data['success'] = "User successfully removed from " . $post_data['access_category'] . " group!";
			}
			else
			{
				$response_data['error'] = "There was a problem with this operation, please contact the Web Administrator!";
			}
		}

		//echo back json response
		echo json_encode($response_data);

	}

	public function update_user_pay_information_ajax()
	{

		//display 404 page if controller accessed directly
		if (!$this->input->post())
		{
			show_404();
		}

		//load resources
		$this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
		$this->load->model('pay_information_model');
		$this->load->library('encryption'); //used for encrypting pay information

		//sanitize $_POST variables
		//refer CI documentation
		//this function of the input class filters all post data through XSS
		//assign post data to array
		$post_data = $this->input->post(NULL, TRUE);

		//validate data
		$rules = $this->pay_information_model->rules['pay_information_form'];
    	$this->form_validation->set_rules($rules);

		//validate data, store in database
		if ($this->form_validation->run() == FALSE)
		{
			//set validation errors
    		$data['error'] = validation_errors();
		}
		else
		{

			//set pay information data array
			$data = [

				'user_id' => $post_data['user_id'],
				'pay_type' => $post_data['pay_type'],
				'rate' => $this->encryption->encrypt($post_data['rate'])

			];

			$result = $this->pay_information_model->set_user_pay_information($data);

			//verify row was inserted and redirect to form page
			if ($result == 1)
			{

				//set flash data
				$this->session->set_flashdata('message', "Pay Information Updated!");

				//set success message boolean
				$data['success'] = TRUE;

			}
			else
			{

				//set error message
				$data['error'] = 'There was a problem with the database insertion. Please contact the Web Admin.';

			}

		}

		//echo back json response
		echo json_encode($data);

	}

}
