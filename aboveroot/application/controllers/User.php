<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MY_Controller {

	function __construct()
    {
        parent::__construct();
		$this->load->library('ion_auth');
	}

    /**
	 * Index Page for this controller.
	 *
	 */
    public function index()
    {
        show_404();
    }

	/**
	 * displays the login form and logs the user in
	 *
	 */
    public function login()
    {

    	//if the user is already logged in, redirect them home
    	if ($this->ion_auth->logged_in())
		{
			redirect('/');
		}

    	//set the page title
    	$this->data['pagetitle'] = "Timesheet Login";

    	//load form validation library
		$this->load->library('form_validation');

		//validate form
		$this->form_validation->set_rules('email', 'Email', 'trim|required');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');

		//if form validation failed, render the view
        if ($this->form_validation->run() === FALSE)
        {
			$this->render('user/login_view');
		}
        else
        {
			$remember = (bool) $this->input->post('remember');
			$email = $this->input->post('email');
			$password = $this->input->post('password');

			//if the user is logged in successfully, send them to the default controller as defined in config.php
			if ($this->ion_auth->login($email, $password, $remember))
			{
				//If previous session data exists, load previous page that was open before timeout from MY_Controller
				if( $this->session->userdata('redirect_back') )
                {
				    $redirect_url = $this->session->userdata('redirect_back');  // grab value and put into a temp variable so we unset the session value
				    $this->session->unset_userdata('redirect_back');

				    redirect( $redirect_url );
				}
				else
                {
					redirect('/');
				}

			}
			else
			{
				$_SESSION['auth_message'] = $this->ion_auth->errors();
				$this->session->mark_as_flash('auth_message');
				redirect('user/login');
			}

		}

    }

 	/**
	 * logs the user out
	 *
	 */
    public function logout()
    {
		//if the user is logged in, log them out and redirect to login page
		if($this->ion_auth->logged_in())
    	{
			$this->ion_auth->logout();
			redirect('user/login');
		}

		//redirect to login page if accessed while user is already logged out
    	if (!$this->ion_auth->logged_in())
		{
			redirect('user/login');
		}
    }

	/**
	 * displays the initial password reset page
	 * checks the activation code to make sure it's legitimate
	 * @param	$reset_code		string
	 *
	 */
	public function password_reset_check($reset_code)
	{
		//if the user is already logged in, redirect them home
    	if ($this->ion_auth->logged_in())
		{
			redirect('/');
		}

		//set the page title
    	$this->data['pagetitle'] = "Timesheet Password Reset";

		//load resource(s)
		$this->load->library('form_validation');

		//set the current url in session data for use when redirecting
		$this->session->set_userdata('referred_from', current_url());

		//verify reset code is valid
		if ($this->ion_auth->forgotten_password_check($reset_code))
		{
			$this->data['code_status'] = 'active';
			$this->data['reset_code'] = $reset_code;
		}
		else
		{
			$this->data['code_status'] = 'expired';
		}


		//render reset page
		$this->render('user/password_reset_check_view');

	}

	/**
	 * validates the email address against the reset code
	 * displays the form to reset the password
	 * validates reset form
	 *
	 */
	public function password_reset_confirm()
	{
		//display 404 page if controller accessed directly
		if (!$this->input->post())
		{
			show_404();
		}

		//load resources
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model('ion_auth_model'); //allows access to validation $rules

		//get referrel URL for redirects
        $referred_from = $this->session->userdata('referred_from');

		//set the current url in session data for use when redirecting in next controller
		$this->session->set_userdata('referred_from', current_url());

		//sanitize form data
		$post_data = $this->input->post(NULL,TRUE);

		//setup rules data for reset form
		$rules = $this->ion_auth_model->rules['password_reset_email_form'];
    	$this->form_validation->set_rules($rules);

		//if form validation is true, validate email and code and display form
		if ($this->form_validation->run())
		{

			$user_check = $this->ion_auth->forgotten_password_check($post_data['forgotten_password_code']);

			if (isset($user_check) && $user_check->email == $post_data['email'])
			{

				$this->data['reset_code'] = $post_data['forgotten_password_code'];

				//display the form to reset password
				$this->render('user/password_reset_final_view');

			}
			else
			{
				//set flashdata & redirect user to initial screen with warning
				$this->session->set_flashdata('message', "Invalid email entered!");

				//redirect user to previous URL
				redirect($referred_from);

			}
		}
		else
		{

			//set validation errors
		    $this->session->set_flashdata('error', validation_errors());

			//redirect user to previous URL
			redirect($referred_from);

		}

	}

	public function password_reset_final()
	{
		//display 404 page if controller accessed directly
		if (!$this->input->post())
		{
			show_404();
		}

		//load resources
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model('ion_auth_model'); //allows access to validation $rules

		//sanitize form data
		$post_data = $this->input->post(NULL,TRUE);

		//setup rules data for reset form
		$rules = $this->ion_auth_model->rules['password_reset_final_form'];
    	$this->form_validation->set_rules($rules);

		//if form validation is true, update password and display success page
		if ($this->form_validation->run())
		{
			//get user info from reset_code
			$user_check = $this->ion_auth->forgotten_password_check($post_data['forgotten_password_code']);

			if (isset($user_check))
			{

				//update user password
				$id = $user_check->id;
			    $data = [
			          'password' => $post_data['password'],
					  'forgotten_password_code' => null,
					  'forgotten_password_time' => null
				  ];
			    $result = $this->ion_auth->update($id, $data);

				if ($result == true)
				{
					//unset the session variable to when the user logs in they do not return to this page
					$this->session->unset_userdata('redirect_back');

					//set session message for successful password reset
					$_SESSION['password_reset_message'] = "Password successfully updated!";
					$this->session->mark_as_flash('auth_message');
					//redirect to login
					redirect('user/login');
				}
				else
				{
					//set flashdata & redirect user to initial screen with warning
					$this->session->set_flashdata('message', "There was a problem trying to reset your password. Please contact the Web Admin!");

					//redirect user to previous URL
					redirect('user/pasword_reset_check/' . $post_data['forgotten_password_code']);
				}



			}
			else
			{
				//set flashdata & redirect user to initial screen with warning
				$this->session->set_flashdata('message', "There was a problem trying to reset your password. Please contact the Web Admin!");

				//redirect user to previous URL
				redirect('user/pasword_reset_check/' . $post_data['forgotten_password_code']);

			}

		}
		else //redirect back to form with validation errors
		{

			$this->data['reset_code'] = $post_data['forgotten_password_code'];

			//display the form to reset password
			$this->render('user/password_reset_final_view');

		}

	}

}
