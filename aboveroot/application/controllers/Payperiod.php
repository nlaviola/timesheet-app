<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payperiod extends Timesheet_Controller {

	function __construct(){
     	parent::__construct();

     	$this->data['pagetitle'] = 'Timesheet - Payperiod';

    }

	/**
	 * Index Page for this controller.
	 *
	 */
    public function index()
    {
        show_404();
    }

    /**
     * Displays the payperiod data for the given start and end date
     *
     * @param       $payperiod_start_date   input str
     * @param       $payperiod_end_date     input str
     *
     */
    public function display_payperiod($payperiod_start_date = NULL, $payperiod_end_date = NULL)
    {

        //load resource(s)
        $this->load->helper('form');
        $this->load->model('jobs_model');
        $this->load->model('hours_model');

		//set the current url in session data for use when redirecting
		$this->session->set_userdata('referred_from', current_url());

        //query for active jobs
        $this->data['jobs'] = $this->jobs_model->get_active_jobs();

        //query for user's current pay period data
        $user_payperiod_data = $this->hours_model->get_user_payperiod_data($payperiod_start_date, $payperiod_end_date, $this->ion_auth->user()->row()->id);

		//group user payperiod data by date into an array
        $this->data['user_payperiod_data'] = $this->payperiod_library->group_hours_by_date($user_payperiod_data, $payperiod_start_date, $payperiod_end_date);

        //get array of working days in the given period
        $working_days_in_pay_period = $this->payperiod_library->get_working_days($payperiod_start_date, $payperiod_end_date);

        //assign working days to view variable
        $this->data['working_days_in_payperiod_count'] = count($working_days_in_pay_period);

        //query for user's filled out days
        $user_payperiod_days = $this->hours_model->get_user_payperiod_days($payperiod_start_date, $payperiod_end_date, $this->ion_auth->user()->row()->id);

        //calculate the user's missing working days
        $this->data['missing_days'] = $this->payperiod_library->get_missing_days($user_payperiod_days, $working_days_in_pay_period, $payperiod_start_date, $this->date_today);

        //assign today's date to view
        $this->data['date_today'] = $this->date_today;

        //assign date range variables to view
        $this->data['payperiod_start_date'] = $payperiod_start_date;
        $this->data['payperiod_end_date'] = $payperiod_end_date;

		//assign stop date and end date buffer
		//used to shut off access to adding dates on previous pay periods
		//drop date is calculated as the start of the current pay period + 6 days
		$this->data['drop_date'] = date("Y-m-d", strtotime('+6 days', strtotime($this->data['current_payperiod_start_date'])));
		//end_date_buffer is the end of the pay period being viewed + 7 days
		$this->data['end_date_buffer'] = date("Y-m-d", strtotime('+7 days', strtotime($payperiod_end_date)));

        $this->render('payperiod_view');

    }

    /**
     * Displays a list of pay period links by year
     *
     * @param       $year   input num
     *
     */
    public function display_payperiod_list($year = NULL)
    {
		//load resources
		$this->load->model('hours_model');

		//query for user's total paid leave
		$this->data['user_yearly_paid_leave'] = $this->hours_model->get_user_paid_leave($year, $this->ion_auth->user()->row()->id);

		//query for user's total sick leave
		$this->data['user_yearly_sick_leave'] = $this->hours_model->get_user_sick_leave($year, $this->ion_auth->user()->row()->id);

        //use payperiod library function to get array fo pay periods by quarter
        //assign to view
        $this->data['payperiod_ranges'] = $this->payperiod_library->get_payperiod_ranges_by_year($year, $this->current_payperiod_start_date, $this->current_payperiod_end_date);

        //assign year to view
        $this->data['year'] = $year;

        //assign current pay periods to view
        $this->data['current_payperiod_start_date'] = $this->current_payperiod_start_date;
        $this->data['current_payperiod_end_date'] = $this->current_payperiod_end_date;

        $this->render('payperiod_list_view');

    }


	public function get_payperiod_ranges_ajax()
	{

		//display 404 page if controller accessed directly
		if (!$this->input->post())
		{
			show_404();
		}

        //sanitize post data
        $post_data = $this->input->post(NULL,TRUE);

		//use payperiod library function to get array fo pay periods by quarter
        //assign to view
        $payperiod_ranges = $this->payperiod_library->list_payperiods_in_year($post_data['year'], $this->current_payperiod_start_date, $this->current_payperiod_end_date);

		echo json_encode($payperiod_ranges);

	}

}
