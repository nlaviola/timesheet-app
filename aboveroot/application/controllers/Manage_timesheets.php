<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manage_timesheets extends Timesheet_Controller {

	function __construct(){
     	parent::__construct();

     	$this->data['pagetitle'] = 'Timesheet - Manage Users';

		//assign today's date to view
		$this->data['date_today'] = $this->date_today;

		//define controllers not accessible by "public users"
     	//list of protected methods to access (for example only by admin )
		$protected_methods = get_class_methods('Manage_timesheets');//returns list of all method names in class

  		//assign the group(s) to be checked
		$groups = [
				'admin',
				'supervisor'
			];

		//execute the restrict access method
		parent::restrict_access($protected_methods, $groups);

    }

    /**
	 * Index Page for this controller.
	 *
	 */
    public function index()
    {
        show_404();
    }

    public function display_user_payperiod_list($user_id = NULL)
    {

		//load resource(s)
		$this->load->model('hours_model');

		//query for user information
		$this->data['user'] = $this->ion_auth->user($user_id)->row();

		//get earliest posted hours date for user
		$earliest_posted_hours_date = $this->hours_model->get_earliest_hours_date($user_id);

		//check if user has posted any hours
		if ($earliest_posted_hours_date[0]->hours_date)
		{
			//use payperiod library function to get array for all existing pay periods by quarter
			//assign to view
			$this->data['payperiod_ranges'] = $this->payperiod_library->get_payperiod_ranges($earliest_posted_hours_date[0]->hours_date, $this->current_payperiod_start_date, $this->current_payperiod_end_date);

			//assign current pay periods to view
			$this->data['current_payperiod_start_date'] = $this->current_payperiod_start_date;
			$this->data['current_payperiod_end_date'] = $this->current_payperiod_end_date;
		}
		else
		{
			//the user does not yet have hours recorded
			$this->data['payperiod_ranges'] = NULL;

		}

    	$this->render('admin/manage_user_payperiod_list_view');

    }

	public function display_user_payperiod($payperiod_start_date = NULL, $payperiod_end_date = NULL, $user_id = NULL)
    {

		//load resource(s)
        $this->load->helper('form');
        $this->load->model('jobs_model');
        $this->load->model('hours_model');
		$this->load->model('data_model');
		$this->load->model('pay_information_model');
		$this->load->library('encryption'); //used for decrypting pay information

		//set the current url in session data for use when redirecting
		$this->session->set_userdata('referred_from', current_url());

		//query for user information
		$this->data['user'] = $this->ion_auth->user($user_id)->row();

		//get pay data for user
		$pay_data = $this->pay_information_model->get_user_pay_information($user_id);

		//if the pay information is set, assign variables to the view
		if (count($pay_data) > 0)
		{
			//assign pay information to view variables
			$this->data['pay_type'] = $pay_data[0]->pay_type;
			$this->data['rate'] = $this->encryption->decrypt($pay_data[0]->rate);
		}

        //query for active jobs
        $this->data['jobs'] = $this->jobs_model->get_active_jobs();

		//set unapproved check to FALSE
		$this->data['unapproved_check'] = FALSE;
		//query for any unapproved hours
		$unapproved_hours = $this->hours_model->get_user_unapproved_payperiod_hours($user_id, $payperiod_start_date, $payperiod_end_date);
		if (count($unapproved_hours) >= 1) {
			//set unapproved check to TRUE
			$this->data['unapproved_check'] = TRUE;
		}

        //query for user's current pay period hours
        $user_payperiod_data = $this->hours_model->get_user_payperiod_data($payperiod_start_date, $payperiod_end_date, $user_id);

		//group user payperiod data by date into an array
        $this->data['user_payperiod_data'] = $this->payperiod_library->group_hours_by_date($user_payperiod_data, $payperiod_start_date, $payperiod_end_date);

		//print_r($this->data['user_payperiod_data']);

		//query for sum of hours grouped by category
		//$user_hours_category_data = $this->data_model->get_user_hours_by_category($payperiod_start_date, $payperiod_end_date, $user_id);

        //get array of working days in the given period
        $working_days_in_pay_period = $this->payperiod_library->get_working_days($payperiod_start_date, $payperiod_end_date);

        //assign working days to view variable
        $this->data['working_days_in_payperiod_count'] = count($working_days_in_pay_period);

        //query for user's filled out days
        $user_payperiod_days = $this->hours_model->get_user_payperiod_days($payperiod_start_date, $payperiod_end_date, $user_id);

        //calculate the user's missing working days
        $this->data['missing_days'] = $this->payperiod_library->get_missing_days($user_payperiod_days, $working_days_in_pay_period, $payperiod_start_date, $this->date_today);

        //assign today's date to view
        $this->data['date_today'] = $this->date_today;

        //assign date range variables to view
        $this->data['payperiod_start_date'] = $payperiod_start_date;
        $this->data['payperiod_end_date'] = $payperiod_end_date;

		//print_r($this->data);

        $this->render('admin/manage_timesheets_payperiod_view');

	}

	public function print_timesheet($payperiod_start_date = NULL, $payperiod_end_date = NULL, $user_id = NULL)
	{

		//load resource(s)
        $this->load->model('hours_model');

		//query for user information
		$this->data['user'] = $this->ion_auth->user($user_id)->row();

		//query for user's current pay period hours
        $user_payperiod_data = $this->hours_model->get_user_approved_payperiod_data($payperiod_start_date, $payperiod_end_date, $user_id);

		//query for total sick and leave time in pay period
		$this->data['user_payperiod_sick_leave'] = $this->hours_model->get_user_sick_leave(date('Y', strtotime($payperiod_start_date)), $user_id, $payperiod_start_date, $payperiod_end_date);
		$this->data['user_payperiod_paid_leave'] = $this->hours_model->get_user_paid_leave(date('Y', strtotime($payperiod_start_date)), $user_id, $payperiod_start_date, $payperiod_end_date);

		//group user payperiod data by date into an array
        $this->data['user_payperiod_data'] = $this->payperiod_library->group_hours_by_date($user_payperiod_data, $payperiod_start_date, $payperiod_end_date);

		$this->data['payperiod_start_date'] = $payperiod_start_date;
		$this->data['payperiod_end_date'] = $payperiod_end_date;

		//print_r($this->data);

		$this->render('admin/payperiod_print_view', $template='public_master');

	}

	/**
	 * Store hours logged by a supervisor in the database from an AJAX call
	 *
	 */
	public function validate_store_ajax()
	{
		//display 404 page if controller accessed directly
		if (!$this->input->post())
		{
			show_404();
		}

		//load resources
		$this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->model('hours_model');

		//validate form data using rules in model
		$rules = $this->hours_model->rules['hours_form'];
    	$this->form_validation->set_rules($rules);

		//catch post data if it exists
		//if set to Zero category, fill in hours and cleanroom_hours
		if ($this->input->post()) {

			//sanitize form data
			$post_data = $this->input->post(NULL,TRUE);


			//fill in hours of zero category is chosen
			if ($post_data['category'] == 'zero') {
				$post_data['hours'] = 0;
				$post_data['cleanroom_hours'] = 0;
			}

			//set the $post_data array to be validated
			$this->form_validation->set_data($post_data);

		}

    	if ($this->form_validation->run() == FALSE)
		{
			//set validation errors
    		$data['error'] = validation_errors();

		}
		else
		{

			//query for user data
			$user_info = $this->ion_auth->user($post_data['user_id'])->row();

			//assign value to cleanroom_hours if not set
	        if(!$post_data['cleanroom_hours'])
			{
	            $cleanroom_hours = 0;
	        }
			else
			{
	            $cleanroom_hours = $post_data['cleanroom_hours'];
	        }

	        //initialize is_approved and set to 1
			//any supervisor enetered hours are automatically approved
	        $is_approved = 1;

			//prepare data array for db insert
			$data = [
				'user_id' => $user_info->id,
                'name' => $user_info->first_name . ' ' . $user_info->last_name,
                'entered_by' => $this->data['current_user'],
                'category' => $post_data['category'],
                'hours' => $post_data['hours'],
                'description' => $post_data['description'],
                'cleanroom_hours' => $cleanroom_hours,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                'hours_date' => date('Y-m-d', strtotime($post_data['hours_date'])),
                'is_approved' => $is_approved
			];

			//insert data
			$result = $this->hours_model->insert_hours($data);

			//verify row was inserted and redirect to form page
			if ($result == 1)
			{

				//set flashdata message based on form type
				if(isset($post_data['form_type']) && $post_data['form_type'] == 'add_hours_form')
				{

					//set flash data
					$this->session->set_flashdata('message', "Hours successfully entered for <strong>" . $post_data['hours_date'] . "</strong>!");

				}

				if(isset($post_data['form_type']) && $post_data['form_type'] == 'edit_hours_form')
				{

					//set flash data
					$this->session->set_flashdata('message', "<strong>" . $post_data['category'] . "</strong> hours on <strong>" . $post_data['hours_date'] . "</strong> updated!");

				}

				//set success message boolean
				$data['success'] = TRUE;

			}
			else
			{

				//set error message
				$data['error'] = 'There was a problem with the database insertion. Please contact the Web Admin.';

			}

		}

		echo json_encode($data);

	}


}
