<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//default controller as defined in routes.php
class Errors extends Timesheet_Controller {

	function __construct(){
     	parent::__construct();

    }

    public function permission_error(){

    	$this->data['heading'] = "Access Denied";

    	$this->data['message'] = "You do not have permission to use this feature. If you feel you have reached this page in error please contact the Web Administrator.";

    	$this->render("errors/error_permission_view");

    }

}
