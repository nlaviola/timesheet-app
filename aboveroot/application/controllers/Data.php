<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data extends Timesheet_Controller {

	function __construct(){
     	parent::__construct();

     	$this->data['pagetitle'] = 'Timesheet - Data';

		//assign today's date to view
		$this->data['date_today'] = $this->date_today;

    }

    /**
	 * Index Page for this controller.
	 *
	 */
    public function index()
    {
        show_404();
    }

	/**
	 * Data page showing overall hours data for the compnay
	 *
	 */
	public function data_display()
	{
		//load resource(s)
		$this->load->model('jobs_model');
		$this->load->model('hours_model');
		$this->load->model('data_model');

		//not passing a user id will retunr the earliest hours overall
		$earliest_hours_date = $this->hours_model->get_earliest_hours_date();

		$earliest_year = date('Y', strtotime($earliest_hours_date[0]->hours_date));

		for ($i=date('Y'); $i >=  $earliest_year; $i--)
		{
			$years[] = $i;
		}

		//get users and pass to the view
		$this->data['active_users'] = $this->ion_auth->where('users.active', 1)->order_by('first_name', 'ASC')->users()->result();
		$this->data['inactive_users'] = $this->ion_auth->where('users.active', 0)->order_by('first_name', 'ASC')->users()->result();

		$this->data['jobs'] = $this->jobs_model->get_all_jobs();

		$this->data['years'] = $years;

		$this->render('admin/data_display_view.php');

	}

	/**
	 * Preps and downloads a CSV of all user hours for a given time period
	 *
	 * @param	$payperiod_start_date	string
	 * @param	$payperiod_end_date		string
	 *
	 */
	public function download_labor_report($start_date = NULL, $end_date = NULL)
	{
		//load resource(s)
		$this->load->model('data_model');

		//query for all jobs done in the payperiod
		$jobs_array = $this->data_model->get_jobs_worked_in_pay_period($start_date, $end_date);

		$working_days_payperiod = count($this->payperiod_library->get_working_days($start_date, $end_date));
		$working_days_ytd = count($this->payperiod_library->get_working_days(date('Y-01-01', strtotime($start_date)), $end_date));

		//query for all pay period hours for all users
		$payperiod_hours = $this->data_model->get_labor_report_hours($start_date, $end_date);
		$ytd_hours = $this->data_model->get_labor_report_hours(date('Y-01-01', strtotime($start_date)), $end_date);

		//array to hold user pay period hours by category
		$user_totals = [];

		foreach ($payperiod_hours as $user => $hours_data) {
			foreach ($hours_data as $key => $value) {
				if (isset($user_totals[$user][$value['category']]))
				{
					$user_totals[$user][$value['category']] += $value['hours'];
				}
				else
				{
					$user_totals[$user][$value['category']] = $value['hours'];
				}
			
			}
		}

		//array to hold total YTD hours
		$ytd_user_totals = [];

		foreach ($ytd_hours as $user => $hours_data) {
			foreach ($hours_data as $key => $value) {
				if (isset($ytd_user_totals[$user]))
				{
					$ytd_user_totals[$user] += $value['hours'];
				}
				else
				{
					$ytd_user_totals[$user] = $value['hours'];
				}
			
			}
		}

		//build array to convert to CSV file for download

		//heading
		$file_array[] = ['Labor Report',date('Y-m-d', strtotime($start_date)) . ' - ' . date('Y-m-d', strtotime($end_date))];

		$file_array[] = ['Working Days PP:', $working_days_payperiod];
		$file_array[] = ['Working Days YTD:', $working_days_ytd];

		//csv formatting
		$file_array[] = [];

		//format jobs array for us in CSV
		$jobs_formatted = $jobs_array;
		array_unshift($jobs_formatted, '');
		array_push($jobs_formatted, 'Total');
		array_push($jobs_formatted, 'PP Avg.');
		array_push($jobs_formatted, 'YTD Avg.');
		$file_array[] = $jobs_formatted;

		//loop through each user and total jobs in payperiod
		$category_totals = [];
		foreach ($user_totals as $user => $hours)
		{
			$temp_array = [$user];
			$user_total = 0;
			foreach ($jobs_array as $job)
			{

				if (isset($hours[$job])) 
				{
					array_push($temp_array, $hours[$job]);
					$user_total += $hours[$job];

					if (isset($category_totals[$job]))
					{
						$category_totals[$job] += $hours[$job];
					}
					else
					{
						$category_totals[$job] = $hours[$job];
					}

				}
				else
				{
					array_push($temp_array, 0);
				}

			}

			//calculate average for user over payperiod
			$avg = $user_total / $working_days_payperiod;
			//calculate average for user YTD
			$ytd_avg = $ytd_user_totals[$user] / $working_days_ytd;

			array_push($temp_array, $user_total);
			array_push($temp_array, $avg);
			array_push($temp_array, $ytd_avg);

			

			$file_array[] = $temp_array;

		}

		//build totals row
		$total_row = [''];
		$all_hours_total = 0;
		foreach ($jobs_array as $job)
		{
			if (isset($category_totals[$job]))
			{
				array_push($total_row, $category_totals[$job]);
				$all_hours_total += $category_totals[$job];
			}
			else
			{
				array_push($total_row, 0);
			}
		}
		array_push($total_row, $all_hours_total);

		$file_array[] = $total_row;

		//prep file for download
		//prep data for headers
    $filename = 'PraeviumLaborReport_' . date('Y-m-d', strtotime($start_date)) . '_' . date('Y-m-d', strtotime($end_date)) . '.csv';
		//set headers
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Content-type: text/csv; charset=UTF-8');
		header('Content-Disposition: attachment; filename=' . $filename);
		header('Expires: 0');
		header('Pragma: public');

    //prep file for download
		$handle = fopen('php://output', 'w');

		foreach ($file_array as $line_array) {
			fputcsv($handle, $line_array);
		}
		fclose($handle);
		exit;

	}

	/**
	 * Preps and downloads a CSV of user cleanroom hours for the entire month & YTD
	 *
	 * @param	$payperiod_start_date	string
	 * @param	$payperiod_end_date		string
	 * @param	$user_id				int
	 *
	 */
	public function download_user_cleanroom_data($start_date = NULL, $end_date = NULL, $user_id = NULL)
	{

		//load resource(s)
		$this->load->model('jobs_model');
		$this->load->model('data_model');

		//query for user information
		$user = $this->ion_auth->user($user_id)->row();

		//get the monthly cleanroom hours
		$monthly_cleanroom_hours = $this->data_model->get_user_cleanroom_hours_by_category($start_date, $end_date, $user_id);

		//place in associative array
		foreach ($monthly_cleanroom_hours as $hours) {
			$monthly_cleanroom_hours_array[$hours->category] = $hours->cleanroom_hours;
		}

		//get all cleanroom hours through the selected month
		$ytd_cleanroom_hours = $this->data_model->get_user_cleanroom_hours_by_category(date('Y-01-01', strtotime($start_date)), date('Y-m-t', strtotime($end_date)), $user_id);

		//place in associative array
		foreach ($ytd_cleanroom_hours as $hours) {
			$ytd_cleanroom_hours_array[$hours->category] = $hours->cleanroom_hours;
		}

		//build array to convert to CSV file for download

		//heading
		$file_array[] = [date('F', strtotime($start_date)) . ' - ' . date('Y', strtotime($start_date))];
		$file_array[] = [$user->first_name . ' ' . $user->last_name];

		//csv formatting
		$file_array[] = [];

		//jobs
		$jobs = array_keys($ytd_cleanroom_hours_array);
		//add blank entry to jobs array for formatting purposes
		array_unshift($jobs, '');
		array_push($jobs, 'Total');
		$file_array[] = $jobs;

		//add month & ytd hours
		$final_monthly_cleanroom_hours_array[] = 'Monthly Hours';
		$final_ytd_cleanroom_hours_array[] = 'YTD Hours';

		//use ytd hours to generate list of all jobs the user has done in the year
		//if no hours are present for a job, set it to zero
		foreach ($ytd_cleanroom_hours_array as $job => $hours)
		{
			if (isset($monthly_cleanroom_hours_array[$job]))
			{
				$final_monthly_cleanroom_hours_array[] = $monthly_cleanroom_hours_array[$job];
			}
			else
			{
				$final_monthly_cleanroom_hours_array[] = 0;
			}

			if (isset($ytd_cleanroom_hours_array[$job]))
			{
				$final_ytd_cleanroom_hours_array[] = $ytd_cleanroom_hours_array[$job];
			}
			else
			{
				$final_ytd_cleanroom_hours_array[] = 0;
			}

		}

		//hour totals for payperiod hours and ytd hours
		$total_monthly_cleanroom_hours = array_sum($final_monthly_cleanroom_hours_array);
		$total_ytd_cleanroom_hours = array_sum($final_ytd_cleanroom_hours_array);

		//add hour totals to array
		array_push($final_monthly_cleanroom_hours_array, $total_monthly_cleanroom_hours);
		array_push($final_ytd_cleanroom_hours_array, $total_ytd_cleanroom_hours);


		$file_array[] = $final_monthly_cleanroom_hours_array;
		$file_array[] = $final_ytd_cleanroom_hours_array;

		//prep file for download
		//prep data for headers
    $name = $user->first_name . '_' . $user->last_name;//repalce spaces in name with underscores
    $filename = 'MonthlyCleanroomHours-' . $name . '_' . date('F', strtotime($start_date)) . '-' . date('Y', strtotime($end_date)) . '.csv';
		//set headers
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Content-type: text/csv; charset=UTF-8');
		header('Content-Disposition: attachment; filename=' . $filename);
		header('Expires: 0');
		header('Pragma: public');

        //prep file for download
		$handle = fopen('php://output', 'w');

		foreach ($file_array as $line_array) {
			fputcsv($handle, $line_array);
		}
		fclose($handle);
		exit;

	}

	/**
     * Preps and downloads a CSV of user hours for a given pay period and YTD
	 * Calculates job costs if user pay information is present
     *
     * @param	$payperiod_start_date	string
	 * @param	$payperiod_end_date		string
	 * @param	$user_id				int
     *
     */
	public function download_user_payperiod_data($payperiod_start_date = NULL, $payperiod_end_date = NULL, $user_id = NULL)
	{

		//load resource(s)
		$this->load->model('jobs_model');
		$this->load->model('data_model');
		$this->load->model('pay_information_model');
		$this->load->library('timesheet_utilities_library');
		$this->load->library('encryption'); //used for decrypting pay information

		//query for user information
		$user = $this->ion_auth->user($user_id)->row();

		//query for sum of approved hours in payperiod grouped by category & sum of all hours
		$payperiod_hours = $this->data_model->get_user_hours_by_category($payperiod_start_date, $payperiod_end_date, $user_id);
		//place in associative array
		foreach ($payperiod_hours as $hours) {
			$payperiod_hours_array[$hours->category] = $hours->hours;
		}

		//query for sum of approved hours YTD grouped by category & sum of all hours
		$ytd_hours = $this->data_model->get_user_hours_by_category(date('Y-01-01', strtotime($payperiod_start_date)), $payperiod_end_date, $user_id);
		//place in associative array
		foreach ($ytd_hours as $hours) {
			$ytd_hours_array[$hours->category] = $hours->hours;
		}

		//get pay data for user
		$pay_data = $this->pay_information_model->get_user_pay_information($user_id);

		//build array to convert to CSV file for download

		//heading
		$file_array[] = ['Name: ', $user->first_name . ' ' . $user->last_name];

		//csv formatting
		$file_array[] = [];

		//place pay information
		if (count($pay_data) > 0)
		{
			//assign pay information to view variables
			$pay_type = $pay_data[0]->pay_type;
			$rate = $this->encryption->decrypt($pay_data[0]->rate);

			//add pay type to the file_array
			$file_array[] = ['Pay Rate: ', $rate, 'Pay Type: ', $pay_type];

		}
		else
		{
			//add pay type to the file_array
			$file_array[] = ['Pay Rate: ', 'NOT SET', 'Pay Type: ', 'NOT SET'];
		}

		//csv formatting
		$file_array[] = [];

		//jobs
		$jobs = array_keys($ytd_hours_array);
		//add blank entry to jobs array for formatting purposes
		array_unshift($jobs, '');
		array_push($jobs, 'Total');
		$file_array[] = $jobs;

		//add payperiod & ytd hours
		$final_payperiod_hours_array[] = 'Pay Period Hours';
		$final_ytd_hours_array[] = 'YTD Hours';

		//use ytd hours to generate list of all jobs the user has done in the year
		//if no hours are present for a job, set it to zero
		foreach ($ytd_hours_array as $job => $hours)
		{
			if (isset($payperiod_hours_array[$job]))
			{
				$final_payperiod_hours_array[] = $payperiod_hours_array[$job];

			}
			else
			{
				$final_payperiod_hours_array[] = 0;

			}

			if (isset($ytd_hours_array[$job]))
			{
				$final_ytd_hours_array[] = $ytd_hours_array[$job];
			}
			else
			{
				$final_ytd_hours_array[] = 0;
			}

		}

		//hour totals for payperiod hours and ytd hours
		$total_payperiod_hours = array_sum($final_payperiod_hours_array);
		$total_ytd_hours = array_sum($final_ytd_hours_array);

		//add hour totals to array
		array_push($final_payperiod_hours_array, $total_payperiod_hours);
		array_push($final_ytd_hours_array, $total_ytd_hours);


		$file_array[] = $final_payperiod_hours_array;
		$file_array[] = $final_ytd_hours_array;
		//blank rows for formatting
		$file_array[] = [];

		//determine if user pay is set
		//calculate job costs for user based on pay type
		if (count($pay_data) > 0)
		{

			//create arrays for job costs
			$payperiod_job_costs[] = 'Pay Period Job Cost';
			$ytd_job_costs[] = 'YTD Job Cost';

			//perform calcuation on user hours for salary based pay
			if ($pay_data[0]->pay_type == 'salary')
			{
				//set variables
				//the current number of pay periods divived by the total
				$periods_in_year = 24;
				$period_pay_rate = $rate / $periods_in_year;
				//calculate number of periods as period month * 2
				$current_number_of_periods = date('m', strtotime($payperiod_start_date)) * 2;
				//if first pay period of month, subtract 1
				if (date('j', strtotime($payperiod_start_date)) == 1)
				{
					$current_number_of_periods = $current_number_of_periods - 1;
				}
				$current_period_ratio = ($current_number_of_periods) / $periods_in_year;

				//iterate through all jobs the user has hours for
				foreach ($ytd_hours_array as $job => $hours)
				{
					if (isset($payperiod_hours_array[$job]))
					{
						$payperiod_job_costs[] = round(($payperiod_hours_array[$job] / $total_payperiod_hours) * $period_pay_rate, 2);
					}
					else
					{
						$payperiod_job_costs[] = 0;
					}

					if (isset($ytd_hours_array[$job]))
					{
						$ytd_job_costs[] = round(($ytd_hours_array[$job] / $total_ytd_hours) * ($rate * $current_period_ratio), 2);
					}
					else
					{
						$ytd_job_costs[] = 0;
					}
				}


			}

			//hourly employees have number of hours multiplied by hoursly rate to get job costs
			if ($pay_data[0]->pay_type == 'hourly')
			{

				$hourly_pay_rate = $rate;

				//iterate through all jobs the user has hours for
				foreach ($ytd_hours_array as $job => $hours)
				{
					if (isset($payperiod_hours_array[$job]))
					{
						$payperiod_job_costs[] = round($payperiod_hours_array[$job] * $hourly_pay_rate, 2);
					}
					else
					{
						$payperiod_job_costs[] = 0;
					}

					if (isset($ytd_hours_array[$job]))
					{
						$ytd_job_costs[] = round($ytd_hours_array[$job] * $hourly_pay_rate, 2);
					}
					else
					{
						$ytd_job_costs[] = 0;
					}
				}

			}

			//sum total costs
			array_push($payperiod_job_costs, array_sum($payperiod_job_costs));
			array_push($ytd_job_costs, array_sum($ytd_job_costs));

			$file_array[] = $payperiod_job_costs;
			$file_array[] = $ytd_job_costs;

		}

		//prep file for download

		//prep data for headers
        $name = $user->first_name . '_' . $user->last_name;//repalce spaces in name with underscores
        $filename = 'PayPeriodData-' . $name . '_' . $payperiod_start_date . '_' . $payperiod_end_date . '.csv';
		//set headers
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Content-type: text/csv; charset=UTF-8');
		header('Content-Disposition: attachment; filename=' . $filename);
		header('Expires: 0');
		header('Pragma: public');

    //prep file for download
		$handle = fopen('php://output', 'w');

		foreach ($file_array as $line_array) {
				fputcsv($handle, $line_array);
		}
				fclose($handle);
		exit;

	}

	/**
  	* Preps and downloads a CSV of user hours for all pay periods in a given year
    *
		* @param	$user_id	int
		* @param	$year	int
    *
    */
	public function download_user_yearly_data_in_pay_periods($user_id = NULL, $year = NULL)
	{
		//load resource(s)
		$this->load->model('jobs_model');
		$this->load->model('data_model');
		$this->load->model('pay_information_model');
		$this->load->library('payperiod_library');
		$this->load->library('encryption'); //used for decrypting pay information

		//get pay period dates for the given year
		$pay_period_ranges = $this->payperiod_library->get_payperiod_ranges_by_year($year, $this->data['current_payperiod_start_date'], $this->data['current_payperiod_end_date'], $array_style = 'list');

		//query for user information
		$user = $this->ion_auth->user($user_id)->row();

		//query for jobs user worked during the year
		$yearly_jobs = $this->data_model->get_jobs_worked_by_year($year);

		//query for job hours arrayed by pay period
		$pay_period_hours = $this->data_model->get_yearly_hours_by_category($pay_period_ranges, $user_id);		

		//build csv

		$file_array[] = [$user->first_name . ' ' . $user->last_name . ' - ' . $year];
		//blank rows for formatting
		$file_array[] = [];

		//add jobs
		//add blank to beginning for table formatting purposes
		array_unshift($yearly_jobs, '');
		$file_array[] = $yearly_jobs;
		//remove blank from beginning
		array_shift($yearly_jobs);

		//loop through pay periods to build rows
		foreach ($pay_period_hours as $pay_period => $hours)
		{
			$temp_array = [];
			//add the pay period as the first entry in the row
			$temp_array[] = $pay_period;

			//loop through jobs to check for payperiod entry
			foreach ($yearly_jobs as $job)
			{
				if (isset($hours[$job]))
				{
					$temp_array[] = $hours[$job];
				}
				else
				{
					$temp_array[] = 0;
				}
			}

			$file_array[] = $temp_array;

		}

		//prep file for download

		//prep data for headers
		$name = $user->first_name . '_' . $user->last_name;//replace spaces in name with underscores
		$filename = 'PayPeriodData-' . $name . '_' . $year . '.csv';
		//set headers
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Content-type: text/csv; charset=UTF-8');
		header('Content-Disposition: attachment; filename=' . $filename);
		header('Expires: 0');
		header('Pragma: public');

		//prep file for download
		$handle = fopen('php://output', 'w');

		foreach ($file_array as $line_array) {
				fputcsv($handle, $line_array);
		}
				fclose($handle);
		exit;
		
	}

	/**
     *
	 * Retrives total hours by user and job for a given date range
     *
     */
	public function get_hours_by_date_range_ajax()
	{

		//display 404 page if controller accessed directly
		if (!$this->input->post())
		{
			show_404();
		}

		//load resources
		$this->load->model('data_model');

		//sanitize post data
		$post_data = $this->input->post(NULL,TRUE);
		
		//assign user and job variables
		$user_id = trim($post_data['user']);
		$job = trim($post_data['job']);

		$start_date = trim($post_data['date_start']);
		$end_date = trim($post_data['date_end']);

		//query for hours
		$hours_sum = $this->data_model->sum_user_hours_date_range($job, $user_id, $start_date, $end_date);
		$cleanroom_hours_sum = $this->data_model->sum_user_cleanroom_hours_date_range($job, $user_id, $start_date, $end_date);
		$hours= $this->data_model->user_hours_date_range($job, $user_id, $start_date, $end_date);
		$cleanroom_hours = $this->data_model->user_cleanroom_hours_date_range($job, $user_id, $start_date, $end_date);

		$data['hours_sum'] = $hours_sum->hours;
		$data['cleanroom_hours_sum'] = $cleanroom_hours_sum->cleanroom_hours;
		$data['hours'] = $hours;
		$data['cleanroom_hours'] = $cleanroom_hours;

		echo json_encode($data);

	}

	/**
     *
	 * Retrives overall hours and cleanroom hours grouped by job
     *
     */
    public function get_overall_hours_ajax()
    {

        //display 404 page if controller accessed directly
		if (!$this->input->post())
		{
			show_404();
		}

		//load resources
        $this->load->model('data_model');

		//sanitize post data
        $post_data = $this->input->post(NULL,TRUE);

		if ($post_data['payperiod'] == 'all')
		{
			$start_date = null;
			$end_date = null;
		}
		else
		{
			$pieces = explode(':', $post_data['payperiod']);

			$start_date = $pieces[0];
			$end_date = $pieces[1];
		}

		//query for overall hours by project
		$overall_hours = $this->data_model->get_overall_hours($post_data['year'], $start_date, $end_date);

		//query for overall cleanroom hours by project
		$overall_cleanroom_hours = $this->data_model->get_overall_cleanroom_hours($post_data['year'], $start_date, $end_date);

		//create data object
		$data['overall_hours'] = $overall_hours;
		$data['overall_cleanroom_hours'] = $overall_cleanroom_hours;

		//echo back as JSON
		echo json_encode($data);


	}

	/**
     *
	 * Retrives user hours by category for use in the admin payperiod view
     *
     */
    public function get_user_hours_by_category_ajax()
    {

        //display 404 page if controller accessed directly
		if (!$this->input->post())
		{
			show_404();
		}

        //load resources
        $this->load->model('data_model');
		$this->load->model('pay_information_model');
		$this->load->library('pay_information_library');
		$this->load->library('encryption'); //used for decrypting pay information

        //sanitize post data
        $post_data = $this->input->post(NULL,TRUE);

        //query for sum of hours grouped by category & sum of all hours
		$user_hours_category_data = $this->data_model->get_user_hours_by_category($post_data['start_date'], $post_data['end_date'], $post_data['user_id']);

		//query for sum of all hours
		$user_hours_sum = $this->data_model->sum_user_hours($post_data['start_date'], $post_data['end_date'], $post_data['user_id']);

		//get pay data for user
		$pay_data = $this->pay_information_model->get_user_pay_information($post_data['user_id']);

		//if the pay information is set, assign variables to the view
		if (count($pay_data) > 0)
		{
			//assign pay information to view variables
			$pay_type = $pay_data[0]->pay_type;
			$rate = $this->encryption->decrypt($pay_data[0]->rate);

			//use library function to calculate cost per job
			$cost_per_job = $this->pay_information_library->calculate_jobs_cost($user_hours_category_data, $user_hours_sum, $pay_type, $rate, $time_interval = 'period');

			//assign job costs to view
			$json_data['jobCosts'] = $cost_per_job;

		}

		$json_data['hoursData'] = $user_hours_category_data;

        //echo back json data
        echo json_encode($json_data);

    }

	/**
     * Gets total hours and cleanroom hours for a specified user
	 * If no user id is specified, returns JSON data for the logged in user
     *
     * @param	$start_date		string
	 * @param	$end_date		string
	 * @param	$user_id		int
     *
     */
	public function get_user_payperiod_hours_ajax($start_date = NULL, $end_date = NULL, $user_id = NULL)
	{
		//load resources
        $this->load->model('data_model');

		//set $user_id to current logged in user if user_id is NULL
		if (!isset($user_id)) {
			$user_id = $this->ion_auth->user()->row()->id;
		}

		//query for logged_in user payperiod hours data
		$user_payperiod_hours = $this->data_model->get_user_hours_by_category($start_date, $end_date, $user_id);

		//query for user payperiod cleanroom hours data
		$user_payperiod_cleanroom_hours = $this->data_model->get_user_cleanroom_hours_by_category($start_date, $end_date, $user_id);

		$data['user_hours'] = $user_payperiod_hours;
		$data['user_cleanroom_hours'] = $user_payperiod_cleanroom_hours;

		echo json_encode($data);

	}

	/**
   * View list of labor report links
   *
   */
	public function view_labor_reports()
	{
		//load resource(s)
		$this->load->model('hours_model');

		//get all pay period ranges
		$earliest_posted_hours_date = $this->hours_model->get_earliest_hours_date();

		//use payperiod library function to get array for all existing pay periods by quarter
		//assign to view
		$this->data['payperiod_ranges'] = $this->payperiod_library->get_payperiod_ranges($earliest_posted_hours_date[0]->hours_date, $this->current_payperiod_start_date, $this->current_payperiod_end_date);

		//assign current pay periods to view
		$this->data['current_payperiod_start_date'] = $this->current_payperiod_start_date;
		$this->data['current_payperiod_end_date'] = $this->current_payperiod_end_date;

		$this->render('admin/labor_report_links_view');
	
	}


	/**
   * View hours and cleanroom hours charts for a given user and year
   *
	 * @param	$user_id	int
	 * @param	$year		int
   *
   */
	public function view_user_yearly_data($user_id = null, $year = null)
	{

		//query for user information
		$user_data = $this->ion_auth->user($user_id)->row();
		//assign to view
		$this->data['user'] = $user_data;

		//assign year to view
		$this->data['year'] = $year;

		$this->render('admin/manage_user_yearly_data_view');
	}

}
