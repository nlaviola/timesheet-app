<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Information extends Timesheet_Controller {

	function __construct(){
     	parent::__construct();

     	$this->data['pagetitle'] = 'Timesheet - Information';

    }

	/**
	 * Index Page for this controller.
	 *
	 */
	public function index()
	{
		show_404();
	}

	/**
	 * Gathers necessary information for the dashboard and then displays it
	 *
	 */
	public function display_job_codes()
	{

		//load resource(s)
        $this->load->model('jobs_model');
        $this->load->helper('form');

        //query for active jobs
        $active_jobs = $this->jobs_model->get_active_jobs();
        //send data to view
        $this->data['active_jobs'] = $active_jobs;		

		$this->render('job_codes_view');

	}
}
