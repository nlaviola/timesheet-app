<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hours extends Timesheet_Controller {

	function __construct(){
     	parent::__construct();

     	$this->data['pagetitle'] = 'Timesheet - Dashboard';

    }

	/**
     * "discard" hours by id
	 * sets is_newest = 0, effectively removing the hours from a user's view
     *
     * @param       $hours_id   input num
     *
     */
	public function discard_hours($hours_id = NULL)
	{
		//load in resource(s)
        $this->load->model('hours_model');

		//get referrel URL for redirects
        $referred_from = $this->session->userdata('referred_from');

		//query for hours to confirm
		$unapproved_hours = $this->hours_model->get_hours($hours_id);

		//prepare data to discard hours
		$data = [

			'is_newest' => 0,
			'description' => $unapproved_hours[0]->description . ' (hours entered incorrectly and discarded by ' . $this->data['current_user'] . ')',
			'updated_at' => date('Y-m-d H:i:s')

		];

		//update hours, setting is_newest = 0
		$result = $this->hours_model->update_hours_discard($hours_id, $data);

		//set flashdata
		//verify row was updated and redirect to form page
		if ($result == 1)
		{

			//set flash data
			$this->session->set_flashdata('message', $unapproved_hours[0]->category ." hours discarded for <strong>" . $unapproved_hours[0]->name . "</strong>!");

		}
		else
		{

			//set error message
			$data['error'] = 'There was a problem with the database update. Please contact the Web Admin.';

		}

		//redirect user
		redirect($referred_from);

	}

	/**
	 * Store hours logged by the user in the database from an AJAX call
	 *
	 */
	public function validate_store_ajax()
	{
		//display 404 page if controller accessed directly
		if (!$this->input->post())
		{
			show_404();
		}

		//load resources
		$this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->model('hours_model');

		//validate form data using rules in model
		$rules = $this->hours_model->rules['hours_form'];
    	$this->form_validation->set_rules($rules);

		//catch post data if it exists
		//if set to Zero category, fill in hours and cleanroom_hours
		if ($this->input->post()) {

			//sanitize form data
			$post_data = $this->input->post(NULL,TRUE);


			//fill in hours of zero category is chosen
			if ($post_data['category'] == 'zero') {
				$post_data['hours'] = 0;
				$post_data['cleanroom_hours'] = 0;
			}

			//set the $post_data array to be validated
			$this->form_validation->set_data($post_data);

		}

    	if ($this->form_validation->run() == FALSE)
		{
			//set validation errors
    		$data['error'] = validation_errors();

		}
		else
		{

			//assign value to cleanroom_hours if not set
	        if(!$post_data['cleanroom_hours'])
			{
	            $cleanroom_hours = 0;
	        }
			else
			{
	            $cleanroom_hours = $post_data['cleanroom_hours'];
	        }

	        //initialize is_approved and set to 0
	        $is_approved = 0;

	        //compare hours_date for approval
	        //prevent user from creating approved future hours by messing with date
	       	if(date('Y-m-d', strtotime($post_data['hours_date'])) == date('Y-m-d', strtotime('now')))
	        {
	            $is_approved = 1;
	        }


			//prepare data array for db insert
			$data = [
				'user_id' => $this->ion_auth->user()->row()->id,
                'name' => $this->data['current_user'],
                'entered_by' => $this->data['current_user'],
                'category' => $post_data['category'],
                'hours' => $post_data['hours'],
                'description' => $post_data['description'],
                'cleanroom_hours' => $cleanroom_hours,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                'hours_date' => date('Y-m-d', strtotime($post_data['hours_date'])),
                'is_approved' => $is_approved
			];

			//insert data
			$result = $this->hours_model->insert_hours($data);

			//verify row was inserted and redirect to form page
			if ($result == 1)
			{

				//set flashdata message based on form type
				if(isset($post_data['form_type']) && $post_data['form_type'] == 'add_hours_form')
				{

					//set flash data
					$this->session->set_flashdata('message', "Hours successfully entered for <strong>" . $post_data['hours_date'] . "</strong>!");

				}

				if(isset($post_data['form_type']) && $post_data['form_type'] == 'edit_hours_form')
				{

					//set flash data
					$this->session->set_flashdata('message', "<strong>" . $post_data['category'] . "</strong> hours on <strong>" . $post_data['hours_date'] . "</strong> updated!");

				}

				//set success message boolean
				$data['success'] = TRUE;


			}
			else
			{

				//set error message
				$data['error'] = 'There was a problem with the database insertion. Please contact the Web Admin.';

			}

		}

		echo json_encode($data);

	}

	/**
	 * Store hours logged by the user in the database
	 *
	 */
	public function validate_store_redirect()
	{

		//load resources
		$this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->model('hours_model');

        //get referrel URL for redirects
        $referred_from = $this->session->userdata('referred_from');

		//validate form data using rules in model
		$rules = $this->hours_model->rules['hours_form'];
    	$this->form_validation->set_rules($rules);

		//catch post data if it exists
		//if set to Zero category, fill in hours and cleanroom_hours
		if ($this->input->post()) {

			//sanitize form data
			$post_data = $this->input->post(NULL,TRUE);


			//fill in hours of zero category is chosen
			if ($post_data['category'] == 'zero') {
				$post_data['hours'] = 0;
				$post_data['cleanroom_hours'] = 0;
			}

			//set the $post_data array to be validated
			$this->form_validation->set_data($post_data);

		}

    	if ($this->form_validation->run() == FALSE)
		{

			//set validation errors
		    $this->session->set_flashdata('error', validation_errors());

			redirect($referred_from);

		}
		else
		{

			//give value to cleanroom_hours if not set
	        if(!$post_data['cleanroom_hours'])
			{
	            $cleanroom_hours = 0;
	        }
			else
			{
	            $cleanroom_hours = $post_data['cleanroom_hours'];
	        }

	        //initialize is_approved and set to 0
	        $is_approved = 0;

	        //compare hours_date for approval
	        //prevent user from creating approved future hours by messing with date
	       	if(date('Y-m-d', strtotime($post_data['hours_date'])) == date('Y-m-d', strtotime('now')))
	        {
	            $is_approved = 1;
	        }

			//prepare data array for db insert
			$data = [
				'user_id' => $this->ion_auth->user()->row()->id,
                'name' => $this->data['current_user'],
                'entered_by' => $this->data['current_user'],
                'category' => $post_data['category'],
                'hours' => $post_data['hours'],
                'description' => $post_data['description'],
                'cleanroom_hours' => $cleanroom_hours,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                'hours_date' => date('Y-m-d'),
                'is_approved' => $is_approved
			];

			//insert data
			$result = $this->hours_model->insert_hours($data);

			//verify row was inserted and redirect to form page
			if ($result == 1)
			{

				//set success message
				$this->session->set_flashdata('message', $post_data['category']. " hours successfully entered!");

				redirect($referred_from);

			}
			else
			{

				//set error message
				$this->session->set_flashdata('error', 'There was a problem with the databse insertion. Please contact the Web Admin.');

				redirect($referred_from);

			}

		}

	}

}
