<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends Timesheet_Controller {

	function __construct(){
     	parent::__construct();

     	$this->data['pagetitle'] = 'Timesheet - Dashboard';

    }

	/**
	 * Index Page for this controller.
	 *
	 */
	public function index()
	{
		show_404();
	}

	/**
	 * Gathers necessary information for the dashboard and then displays it
	 *
	 */
	public function display_dashboard()
	{

		//load Resources
		$this->load->helper('form');
		$this->load->model('jobs_model');
		$this->load->model('hours_model');

		//set the current url in session data for use when redirecting
		$this->session->set_userdata('referred_from', current_url());

		//query for active jobs
		$this->data['jobs'] = $this->jobs_model->get_active_jobs();

		//query for daily hours by category
		$this->data['daily_hours_category'] = $this->hours_model->get_user_daily_hours_category($this->date_today, $this->ion_auth->user()->row()->id);

		//query for user's daily hours total
		$this->data['daily_hours_count'] = $this->hours_model->get_user_daily_hours_count($this->date_today, $this->ion_auth->user()->row()->id);

		//query for user's current pay period data
		$this->data['user_payperiod_data'] = $this->hours_model->get_user_payperiod_data($this->current_payperiod_start_date, $this->current_payperiod_end_date, $this->ion_auth->user()->row()->id);

		//query for user's total paid leave
		$this->data['user_yearly_paid_leave'] = $this->hours_model->get_user_paid_leave(date('Y'), $this->ion_auth->user()->row()->id);

		//query for user's total sick leave
		$this->data['user_yearly_sick_leave'] = $this->hours_model->get_user_sick_leave(date('Y'), $this->ion_auth->user()->row()->id);

		//assign working days to view variable
		$this->data['working_days_in_current_payperiod_count'] = count($this->working_days_in_current_payperiod);

		//assign today's date to view
		$this->data['date_today'] = $this->date_today;

		//query for user's filled out days
		$user_payperiod_days = $this->hours_model->get_user_payperiod_days($this->current_payperiod_start_date, $this->current_payperiod_end_date, $this->ion_auth->user()->row()->id);

		//calculate the user's missing working days
		$this->data['missing_days'] = $this->payperiod_library->get_missing_days($user_payperiod_days, $this->working_days_in_current_payperiod, $this->current_payperiod_start_date, $this->date_today);

		$this->render('dashboard_view');

	}
}
