<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payperiod_library {

	/**
	 * Returns an array of dates in YYY-MM-DD format for a given date range
	 *
	 * @param       string  $payperiod_start_date    Input string
	 * @param       string  $payperiod_end_date      Input string
	 * @return      array
	 */
    public function get_working_days($payperiod_start_date = NULL, $payperiod_end_date = NULL)
    {

    	$start = new DateTime($payperiod_start_date);
		$end = new DateTime($payperiod_end_date);
		$oneday = new DateInterval("P1D");

		$days = array();

		/* Iterate from $start up to $end+1 day, one day in each iteration.
		   We add one day to the $end date, because the DatePeriod only iterates up to,
		   not including, the end date. */
		foreach(new DatePeriod($start, $oneday, $end->add($oneday)) as $day)
        {
		    $day_num = $day->format("N"); /* 'N' number days 1 (mon) to 7 (sun) */
		    if($day_num < 6) /* weekday */
            {
		        $days[] = $day->format("Y-m-d");
		    }
		}

		return $days;

    }

    /**
	 * compares days a user has filled out in a given time period to the working days ina. given date range
	 * returns missing days in an array
	 *
	 * @param       array  $user_payperiod_days   	Input array
	 * @param       string  $payperiod_start_date   Input string
	 * @param       string  $end_date      			Input string
	 * @return      array
	 */
    public function get_missing_days($user_payperiod_days = NULL, $working_days = NULL, $payperiod_start_date = NULL, $end_date = NULL)
    {

		//initialize missing days array
		$missing_days = [];

		//iterate through working days in date range
		//check for existence of day in user data
		//log missing days
		foreach ($working_days as $work_day)
        {
			if (!isset($user_payperiod_days[$work_day]) && $work_day < date('Y-m-d'))
            {
				$missing_days[] = $work_day;
			}
		}

		return $missing_days;

    }



    /**
	 * returns an array indexed by year and then quarter of start and end dates for all periods for a given user
	 *
	 * @param       string  $user_id   Input integer

	 * @return      array
	 *
	 */
    public function get_payperiod_ranges($start_date = NULL, $current_payperiod_start_date = NULL, $current_payperiod_end_date = NULL)
    {
        //extract parts from start date
        $start_date_year = date('Y', strtotime($start_date));
        $start_date_month = date('m', strtotime($start_date));

        //initialize payperiods array
        $payperiods = [];
        $date_placeholder = date($start_date_year . '-' . $start_date_month . '-01');

        //set end date
        $final_date = $current_payperiod_start_date;

        //loop through payperiods in the year
        while($date_placeholder < $final_date)
        {


            if(date('d', strtotime($date_placeholder)) == '01') //create set of date ranges, then iterate to next period
            {
                $payperiods[date('Y', strtotime($date_placeholder))][] = [

                			'start' =>	$date_placeholder,
                			'end'	=>	date('Y-m-d', strtotime($date_placeholder . ' + 14 days'))

                		];

                //advance the payperiod
                $date_placeholder = date('Y-m-d', strtotime($date_placeholder . ' + 15 days'));
            }
            elseif(date('d', strtotime($date_placeholder)) == '16') //create set of date ranges, then iterate to next period
            {
                $payperiods[date('Y', strtotime($date_placeholder))][] = [

                			'start' =>	$date_placeholder,
                			'end'	=>	date("Y-m-t", strtotime($date_placeholder))

                		];

                //advance the payperiod
                $date_placeholder = date('Y-m-d', strtotime($date_placeholder . ' - 15 days' . ' + 1 month'));
            }


        }

        //add current payperiod
        $payperiods[date('Y', strtotime($current_payperiod_start_date))][] = [

                'start' =>	$current_payperiod_start_date,
                'end'	=>	$current_payperiod_end_date

            ];

        //reverse the years to descending order
        $payperiods = array_reverse($payperiods);
        //reveres the quarters to descending order
        foreach ($payperiods as $year => $ranges) {
            $payperiods[$year] = array_reverse($ranges);
        }

        //initialize final period array
        $payperiods_finalized = [];

        //loop through periods and assign to array by quarter
        foreach ($payperiods as $date_ranges)
        {
            foreach ($date_ranges as $range)
            {

                //assign month of period in question to variable
                $year = date('Y', strtotime($range['start']));
            	$month = date('m', strtotime($range['start']));

                //assign date range set to appropriate quarter
            	if ($month <= 12 && $month > 9 )
                {
            		$payperiods_finalized[$year]['Q4'][] = $range;
            	}
            	if ($month <= 9 && $month > 6 )
                {
            		$payperiods_finalized[$year]['Q3'][] = $range;
            	}
            	if ($month <= 6 && $month > 3 )
                {
            		$payperiods_finalized[$year]['Q2'][] = $range;
            	}
            	if ($month <= 3 && $month >= 1 )
                {
            		$payperiods_finalized[$year]['Q1'][] = $range;
            	}

            }

        }

        return $payperiods_finalized;

    }


    /**
	 * returns an array indexed by quarter of start and end dates for all periods in a year 
     * OR a simple list of payperiods in a given year depending on style chosen
	 * ends with current period if current year is selected
	 *
	 * @param       string  $year                           Input string
     * @param       string  $current_payperiod_start_date   Input string
	 * @param       string  $current_payperiod_end_date     Input string
     * @param       string  $array_style                    Input string
	 * @return      array
	 *
	 */
    public function get_payperiod_ranges_by_year($year = NULL, $current_payperiod_start_date = NULL, $current_payperiod_end_date = NULL, $array_style = NULL)
    {

    	//create array of pay periods
        $year_start_date = date($year . '-01-01');
        $payperiods = [];
        $date_placeholder = $year_start_date;

        //determine end date
        if($year == date('Y'))
        {
        	$final_date = $current_payperiod_start_date;

        }
        else
        {
        	$final_date = date($year . '-12-31');
        }

        //loop through payperiods in the year
        while($date_placeholder < $final_date)
        {

            if(date('d', strtotime($date_placeholder)) == '01') //create set of date ranges, then iterate to next period
            {
                $payperiods[] = [

                			'start' =>	$date_placeholder,
                			'end'	=>	date('Y-m-d', strtotime($date_placeholder . ' + 14 days'))

                		];

                //advance the payperiod
                $date_placeholder = date('Y-m-d', strtotime($date_placeholder . ' + 15 days'));
            }
            elseif(date('d', strtotime($date_placeholder)) == '16') //create set of date ranges, then iterate to next period
            {
                $payperiods[] = [

                			'start' =>	$date_placeholder,
                			'end'	=>	date("Y-m-t", strtotime($date_placeholder))

                		];

                //advance the payperiod
                $date_placeholder = date('Y-m-d', strtotime($date_placeholder . ' - 15 days' . ' + 1 month'));
            }
        }

        //add the latest pay period of the current year if applicable
        if($year == date('Y'))
        {

        	$payperiods[] = [

        			'start' =>	$current_payperiod_start_date,
        			'end'	=>	$current_payperiod_end_date

        		];

        }

        if (isset($array_style) && $array_style = 'list') {
            //return simple list of payperiods from 
            return $payperiods;
        }
        else
        {
            //reverse array to read from latest date down
            $payperiods = array_reverse($payperiods);

            //initialize final period array
            $payperiods_finalized = [];

            //loop through periods and assign to array by quarter
            foreach ($payperiods as $range)
            {
                //assign month of period in question to variable
                $month = date('m', strtotime($range['start']));

                //assign date range set to appropriate quarter
                if ($month <= 12 && $month > 9 )
                {
                    $payperiods_finalized['Q4'][] = $range;
                }
                if ($month <= 9 && $month > 6 )
                {
                    $payperiods_finalized['Q3'][] = $range;
                }
                if ($month <= 6 && $month > 3 )
                {
                    $payperiods_finalized['Q2'][] = $range;
                }
                if ($month <= 3 && $month >= 1 )
                {
                    $payperiods_finalized['Q1'][] = $range;
                }

            }

            return $payperiods_finalized;
        }

    }



    /**
	 * Returns an array of hours objects for a pay period grouped by date along with total hours and cleanroom hours
	 *
	 * @param       array  $payperiod_hours    Input array
	 * @param       string  $payperiod_start_date    Input string
	 * @param       string  $payperiod_end_date    Input string
	 * @return      array
	 */
	public function group_hours_by_date($payperiod_hours, $payperiod_start_date, $payperiod_end_date)
	{

		//set the initial date to be used in a while loop
		$date_track = $payperiod_start_date;

		//initialize array to collect hours objects in an array indexed by the date
		$date_grouped_hours = [];

		//loop through array of objects and assign them to new array grouped by date
		foreach ($payperiod_hours as $entry) {
            $date_grouped_hours[$entry->hours_date][] = $entry;
        }

        //initialize the final array
        $final_array =[];

        while (strtotime($date_track) <= strtotime($payperiod_end_date))
        {

        	//determine if the date in question has any hours entered
        	if (isset($date_grouped_hours[$date_track]))
            {

        		//assign entry to indexed date
        		$final_array[$date_track]['hours'] = $date_grouped_hours[$date_track];

        		//initialize sum variables
        		$sum_hours = 0;
        		$sum_cleanroom_hours = 0;

        		//initialize unapproved hours vairble set as false
        		$unapproved_hours = 0;

        		//iterate through hours objects and sum hours and cleanroom hours
        		foreach ($date_grouped_hours[$date_track] as $entry)
                {

        			if ($entry->is_approved == 0)
                    {
        				$unapproved_hours = 1;
        			}

        			$sum_hours += $entry->hours;
        			$sum_cleanroom_hours += $entry->cleanroom_hours;

        		}

        		//add stats to final array
        		$final_array[$date_track]['stats'] = [

						'total_hours' => $sum_hours,
						'total_cleanroom_hours' => $sum_cleanroom_hours,
						'unapproved_hours' => $unapproved_hours

					];

        	}
            else
            {
        		$final_array[$date_track]['hours'] = false;
        	}

            //advance the payperiod
        	$date_track = date ("Y-m-d", strtotime("+1 day", strtotime($date_track)));

        }

        return $final_array;

	}

    /**
	 * returns an array indexed by year and then quarter of start and end dates for all periods
	 *
	 * @param       string  $year                           Input integer
     * @param       string  $current_payperiod_start_date   Input date
	 * @return      string  $current_payperiod_end_date     Input date
	 *
	 */
    public function list_payperiods_in_year($year = NULL, $current_payperiod_start_date = NULL, $current_payperiod_end_date = NULL)
    {
        //create array of pay periods
        $year_start_date = date($year . '-01-01');
        $payperiods = [];
        $date_placeholder = $year_start_date;

        //determine end date
        if($year == date('Y'))
        {
        	$final_date = $current_payperiod_start_date;

        }
        else
        {
        	$final_date = date($year . '-12-31');
        }

        //loop through payperiods in the year
        while($date_placeholder < $final_date)
        {

            if(date('d', strtotime($date_placeholder)) == '01') //create set of date ranges, then iterate to next period
            {
                $payperiods[] = [

                			'start' =>	$date_placeholder,
                			'end'	=>	date('Y-m-d', strtotime($date_placeholder . ' + 14 days'))

                		];

                //advance the payperiod
                $date_placeholder = date('Y-m-d', strtotime($date_placeholder . ' + 15 days'));
            }
            elseif(date('d', strtotime($date_placeholder)) == '16') //create set of date ranges, then iterate to next period
            {
                $payperiods[] = [

                			'start' =>	$date_placeholder,
                			'end'	=>	date("Y-m-t", strtotime($date_placeholder))

                		];

                //advance the payperiod
                $date_placeholder = date('Y-m-d', strtotime($date_placeholder . ' - 15 days' . ' + 1 month'));
            }
        }

        //add the latest pay period of the current year if applicable
        if($year == date('Y'))
        {

        	$payperiods[] = [

        			'start' =>	$current_payperiod_start_date,
        			'end'	=>	$current_payperiod_end_date

        		];

        }

        return $payperiods;
    }


}
