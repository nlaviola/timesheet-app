<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pay_information_library {

    /**
	 * Returns an array of job categories paired with the cost based on user pay information
	 *
     * @param       array   $hours_data    Input string
	 * @param       string  $pay_type      Input string
	 * @param       string  $rate          Input string
     * @param       string  $time_interval Input string
	 * @return      array
	 */
    public function calculate_jobs_cost($hours_data = NULL, $hours_sum = NULL, $pay_type = NULL, $rate = NULL, $time_interval = NULL)
    {

        $job_cost = [];

        //multiply each job hours by the user pay divided by the time interval
        if ($pay_type == 'salary')
        {

            //determine time period to divide user pay in the case of salary
            switch ($time_interval) {
                case 'period':
                    $denominator = 24;
                    break;

                default:
                    // code...
                    break;
            }

            //calculate time interval cost
            $time_interval_cost = $rate / $denominator;

            //loop through hours data and calaculate cost based on percentage of total hours
            foreach ($hours_data as $job)
            {
                //$hours = (int)$hours;
                $job_cost[$job->category] = round($time_interval_cost * ($job->hours/$hours_sum[0]->hours), 2);
            }

        }

        //multiply job hours by hourly rate to get costs
        if ($pay_type == 'hourly')
        {

            //loop through hours data and calaculate cost based on percentage of total hours
            foreach ($hours_data as $job)
            {
                //$hours = (int)$hours;
                $job_cost[$job->category] = round($job->hours * $rate, 2);
            }

        }

        return $job_cost;

    }

}
