<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Timesheet_utilities_library {

    /**
	 * Returns a randomly generated password
	 *
	 * @return      string
	 */
    public function random_password()
    {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!@#$%&*';
        $password = array();
        $alpha_length = strlen($alphabet) - 1;
        for ($i = 0; $i < 10; $i++)
        {
            $n = rand(0, $alpha_length);
            $password[] = $alphabet[$n];
        }
        return implode($password);
    }

}
