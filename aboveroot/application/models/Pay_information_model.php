<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pay_information_model extends CI_model {

	/**
	 * Define validation rules
	 *
	 */
	public $rules = array(
	    'pay_information_form'=>
		      array(
		        'pay_type' => array('field'=>'pay_type','label'=>'Pay Type','rules'=>'trim|required'),
		        'rate' => array('field'=>'rate','label'=>'Rate','rules'=>'trim|required|numeric')
		      )
	);

    /**
	 * update $this->db to use correct database
	 *
	 */
	public function __construct()
	{
		parent::__construct();
		//update $this->db to use correct database specified in application/config/database.php
		$this->db = $this->load->database('timesheet', TRUE);
	}

    public function get_user_pay_information($user_id = NULL)
    {
        //build query
        $this->db->select('*');
        $this->db->where('user_id', $user_id);
        $pay_data = $this->db->get('pay_information')->result();

        return $pay_data;
    }

    public function set_user_pay_information($data = NULL)
    {
        //check if pay information exists already
        $this->db->where('user_id', $data['user_id']);
        $q = $this->db->get('pay_information')->result();
        $this->db->reset_query();

        if (count($q) > 0 )
        {
            $this->db->where('user_id', $data['user_id'])->update('pay_information', $data);
        }
        else
        {

            $this->db->insert('pay_information', $data);
        }

        //return affected rows
		return $this->db->affected_rows();

    }


}
