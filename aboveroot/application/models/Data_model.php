<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data_model extends CI_model {

    /**
	 * update $this->db to use "testdata" database specified in application/config/database.php
	 *
	 */
	public function __construct()
	{
		parent::__construct();
		//update $this->db to use correct database specified in application/config/database.php
		$this->db = $this->load->database('timesheet', TRUE);
	}

	public function get_jobs_worked_by_year($year = NULL)
	{
		//build query
		$this->db->select('category');
		$this->db->where_not_in('category', 'zero');
		$this->db->where('is_newest', 1);
		$this->db->where('is_approved', 1);
		$this->db->where('hours_date >=', date($year . '-01-01'));
		$this->db->where('hours_date <=', date($year . '-12-31'));
		$this->db->distinct();
		$this->db->order_by('category', 'ASC');
		//execute query
		$user_jobs_data = $this->db->get('hours')->result_array();

		//create 1d array of jobs
		$user_jobs_final=[];

		foreach ($user_jobs_data as $key => $value) {
			$user_jobs_final[]=$value['category'];
		}

		return $user_jobs_final;

	}

	public function get_jobs_worked_in_pay_period($start_date = NULL, $end_date = NULL)
	{
		//build query
		$this->db->select('category');
		$this->db->where_not_in('category', 'zero');
		$this->db->where('is_newest', 1);
		$this->db->where('is_approved', 1);
		$this->db->where('hours_date >=', date($start_date));
		$this->db->where('hours_date <=', date($end_date));
		$this->db->distinct();
		$this->db->order_by('category', 'ASC');
		//execute query
		$user_jobs_data = $this->db->get('hours')->result_array();

		//create 1d array of jobs
		$user_jobs_final=[];

		foreach ($user_jobs_data as $key => $value) {
			$user_jobs_final[]=$value['category'];
		}

		return $user_jobs_final;
	}

	public function get_labor_report_hours($start_date = NULL, $end_date = NULL)
	{	

		//build query
		$this->db->select('name, category, hours');

		$this->db->where('hours_date >=', date($start_date));
		$this->db->where('hours_date <=', date($end_date));
	
		$this->db->where('is_newest', 1);
		$this->db->where('is_approved', 1);
		$this->db->order_by('name', 'ASC');
		//execute query
		$hours_data = $this->db->get('hours')->result_array();

		$user_hours_data = [];

		//group into user hours
		foreach ($hours_data as $hours) {
			$user = str_replace(' ', '', $hours['name']);
			$user_hours_data[$user][] = $hours;
		}

		return $user_hours_data;

	}

	public function get_overall_hours($year = NULL, $start_date = NULL, $end_date = NULL)
	{

		//build query
		$this->db->select('category, SUM(hours) AS hours', FALSE);
		if (isset($start_date) && isset($end_date))
		{
			$this->db->where('hours_date >=', date($start_date));
			$this->db->where('hours_date <=', date($end_date));
		}
		else
		{
			$this->db->where('hours_date >=', date($year . '-01-01'));
			$this->db->where('hours_date <=', date($year . '-12-31'));
		}
		$this->db->where('is_newest', 1);
		$this->db->where('is_approved', 1);
		$this->db->group_by('category');
		//execute query
		$overall_hours_data = $this->db->get('hours')->result();

		return $overall_hours_data;

	}

	public function get_overall_cleanroom_hours($year = NULL, $start_date = NULL, $end_date = NULL)
	{

		//build query
		$this->db->select('category, SUM(cleanroom_hours) AS cleanroom_hours', FALSE);
		if (isset($start_date) && isset($end_date))
		{
			$this->db->where('hours_date >=', date($start_date));
			$this->db->where('hours_date <=', date($end_date));
		}
		else
		{
			$this->db->where('hours_date >=', date($year . '-01-01'));
			$this->db->where('hours_date <=', date($year . '-12-31'));
		}
		$this->db->where('is_newest', 1);
		$this->db->where('is_approved', 1);
		$this->db->group_by('category');
		//execute query
		$overall_cleanroom_hours_data = $this->db->get('hours')->result();

		return $overall_cleanroom_hours_data;

	}

    /**
	 * Get all hours (including unapproved but submitted hours) for a given user and date range
     * group by category
	 * @return array of objects
	 */
    public function get_user_hours_by_category($start_date = NULL, $end_date = NULL, $user_id = NULL)
    {

        //build query
				$this->db->select('category, SUM(hours) AS hours', FALSE);
				$this->db->where('user_id', $user_id);
				$this->db->where('hours_date >=', date('Y-m-d', strtotime($start_date)));
				$this->db->where('hours_date <=', date('Y-m-d', strtotime($end_date)));
				$this->db->where('is_newest', 1);
				$this->db->where('is_approved', 1);
				$this->db->group_by('category');
				//execute query
				$user_hours_data = $this->db->get('hours')->result();

				return $user_hours_data;

		}

		/**
	 * Get all cleanroom hours (including unapproved but submitted hours) for a given user and date range
     * group by category
	 * @return array of objects
	 */
		public function get_user_cleanroom_hours_by_category($start_date = NULL, $end_date = NULL, $user_id = NULL)
		{
			//build query
			$this->db->select('category, SUM(cleanroom_hours) AS cleanroom_hours', FALSE);
			$this->db->where('user_id', $user_id);
			$this->db->where('hours_date >=', date('Y-m-d', strtotime($start_date)));
			$this->db->where('hours_date <=', date('Y-m-d', strtotime($end_date)));
			$this->db->where('cleanroom_hours >', 0);
			$this->db->where('is_newest', 1);
			$this->db->group_by('category');
			//execute query
			$user_cleanroom_hours_data = $this->db->get('hours')->result();

			return $user_cleanroom_hours_data;
		}
		
		 /**
	 * Get all hours for a given user and date range
     * group by category
	 * @return array of objects
	 */
	public function get_yearly_hours_by_category($pay_period_ranges = NULL, $user_id = NULL)
	{
		//create array to hold queries
		$results_array = [];

		//loop through all pay periods, add to results array
		foreach ($pay_period_ranges as $pay_period) {
			//build query
			$this->db->select('category, SUM(hours) AS hours', FALSE);
			$this->db->where('user_id', $user_id);
			$this->db->where('hours_date >=', date('Y-m-d', strtotime($pay_period['start'])));
			$this->db->where('hours_date <=', date('Y-m-d', strtotime($pay_period['end'])));
			$this->db->where('is_newest', 1);
			$this->db->where('is_approved', 1);
			$this->db->group_by('category');
			//execute query
			$hours_data = $this->db->get('hours')->result_array();

			$hours_data_final = [];

			//loop through and format hours data
			foreach ($hours_data as $key => $value) {
				$hours_data_final[$value['category']] =  $value['hours'];
			}

			$array_key = $pay_period['start'] . ' | ' . $pay_period['end'];

			//add to array
			$results_array[$array_key] = $hours_data_final;
			
		}

		return $results_array;

	}

	/**
	 * Sums all user hours for a given user id and date range
	 */
	public function sum_user_hours($start_date = NULL, $end_date = NULL, $user_id = NULL)
	{

		//build query
		$this->db->select('SUM(hours) AS hours', FALSE);
		$this->db->where('user_id', $user_id);
		$this->db->where('hours_date >=', date('Y-m-d', strtotime($start_date)));
		$this->db->where('hours_date <=', date('Y-m-d', strtotime($end_date)));
		$this->db->where('is_newest', 1);
		//execute query
		$user_hours_sum = $this->db->get('hours')->result();

		return $user_hours_sum;

	}

	/**
	 * Sums hours for a given user, job, and date range
	 * Takes 'All' parameter for user and job
	 */
	public function sum_user_hours_date_range($job = NULL, $user_id = NULL, $start_date = NULL, $end_date = NULL)
	{	

		//build query
		$this->db->select('SUM(hours) AS hours', FALSE);
		if ($user_id != 'All') {
			$this->db->where('user_id', $user_id);
		}
		if ($job != 'All') {
			$this->db->where('category', $job);
		}
		$this->db->where('hours_date >=', date('Y-m-d', strtotime($start_date)));
		$this->db->where('hours_date <=', date('Y-m-d', strtotime($end_date)));
		$this->db->where('is_newest', 1);
		//execute query
		$user_hours_sum = $this->db->get('hours')->result();

		return $user_hours_sum[0];

	}

	/**
	 * Sums cleanroom hours for a given user, job, and date range
	 * Takes 'All' parameter for user and job
	 */
	public function sum_user_cleanroom_hours_date_range($job = NULL, $user_id = NULL, $start_date = NULL, $end_date = NULL)
	{
		//build query
		$this->db->select('SUM(cleanroom_hours) AS cleanroom_hours', FALSE);
		if ($user_id != 'All') {
			$this->db->where('user_id', $user_id);
		}
		if ($job != 'All') {
			$this->db->where('category', $job);
		}
		$this->db->where('hours_date >=', date('Y-m-d', strtotime($start_date)));
		$this->db->where('hours_date <=', date('Y-m-d', strtotime($end_date)));
		$this->db->where('cleanroom_hours >', 0);
		$this->db->where('is_newest', 1);
		//execute query
		$user_cleanroom_hours_sum = $this->db->get('hours')->result();

		return $user_cleanroom_hours_sum[0];
	}

	/**
	 * Sums hours for a given user, job, and date range
	 * Takes 'All' parameter for user and job
	 */
	public function user_hours_date_range($job = NULL, $user_id = NULL, $start_date = NULL, $end_date = NULL)
	{	

		//build query
		$this->db->select('*');
		if ($user_id != 'All') {
			$this->db->where('user_id', $user_id);
		}
		if ($job != 'All') {
			$this->db->where('category', $job);
		}
		$this->db->where('hours_date >=', date('Y-m-d', strtotime($start_date)));
		$this->db->where('hours_date <=', date('Y-m-d', strtotime($end_date)));
		$this->db->where('is_newest', 1);
		//execute query
		$user_hours = $this->db->get('hours')->result();

		return $user_hours;

	}

	/**
	 * Sums cleanroom hours for a given user, job, and date range
	 * Takes 'All' parameter for user and job
	 */
	public function user_cleanroom_hours_date_range($job = NULL, $user_id = NULL, $start_date = NULL, $end_date = NULL)
	{
		//build query
		$this->db->select('*');
		if ($user_id != 'All') {
			$this->db->where('user_id', $user_id);
		}
		if ($job != 'All') {
			$this->db->where('category', $job);
		}
		$this->db->where('hours_date >=', date('Y-m-d', strtotime($start_date)));
		$this->db->where('hours_date <=', date('Y-m-d', strtotime($end_date)));
		$this->db->where('cleanroom_hours >', 0);
		$this->db->where('is_newest', 1);
		$this->db->order_by('hours_date', 'ASC');
		//execute query
		$user_cleanroom_hours = $this->db->get('hours')->result();

		return $user_cleanroom_hours;
	}

}
