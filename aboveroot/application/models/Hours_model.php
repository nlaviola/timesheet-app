<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hours_model extends CI_model {

	/**
	 * Define validation rules
	 *
	 */
	public $rules = array(
	    'hours_form'=>
		      array(
		        'category' => array('field'=>'category','label'=>'Category','rules'=>'trim|required'),
		        'hours' => array('field'=>'hours','label'=>'Hours','rules'=>'trim|required|numeric'),
		        'cleanroom_hours' => array('field'=>'cleanroom_hours','label'=>'Cleanroom Hours','rules'=>'trim|numeric'),
		        'description' => array('field'=>'description','label'=>'Description','rules'=>'trim|required')
		      )
	);

	/**
	 * update $this->db to use "testdata" database specified in application/config/database.php
	 *
	 */
	public function __construct()
	{
		parent::__construct();
		//update $this->db to use correct database specified in application/config/database.php
		$this->db = $this->load->database('timesheet', TRUE);
	}

	/**
	 * Get the earliest hours entered by the user
	 * @return array of objects
	 */
	public function get_earliest_hours_date($user_id = NULL)
	{

		//build query
		$this->db->select_min('hours_date');
		if (isset($user_id)) {
			$this->db->where('user_id', $user_id);
		}
		$this->db->where('is_newest', 1);
		//execute query
		$earliest_hours_date = $this->db->get('hours')->result();

		return $earliest_hours_date;

	}

	/**
	 * Get hours by id
	 * @return array of objects
	 */
	public function get_hours($hours_id = NULL)
	{
		//build query
		$this->db->select('*');
		$this->db->where('id', $hours_id);
		//execute query
		$hours_data = $this->db->get('hours')->result();

		return $hours_data;
	}

	/**
	 * Return array of unapproved hours grouped by user
	 * @return array of objects
	 */
	public function get_unapproved_hours_all_users()
	{
		//build query
		$this->db->select('*');
		$this->db->where('is_approved', 0);
		$this->db->where('is_newest', 1);
		$this->db->order_by('user_id', 'ASC');
		$this->db->order_by('hours_date', 'DESC');
		$this->db->order_by('created_at', 'DESC');
		//execute query
		$unapproved_hours_all_users = $this->db->get('hours')->result();

		//create array of objects by user
		$unapproved_hours_user_grouped = [];
		foreach ($unapproved_hours_all_users as $key => $hours) {
			$unapproved_hours_user_grouped[$hours->name]['id'] = $hours->user_id;
			$unapproved_hours_user_grouped[$hours->name]['hours'][] = $hours;
		}

		return $unapproved_hours_user_grouped;

	}

	/**
	 * Get daily user hours and order by category
	 * @return array of objects
	 */
	public function get_user_daily_hours_category($date = NULL, $user_id = NULL)
	{
		//build query
		$this->db->select();
		$this->db->where('user_id', $user_id);
		$this->db->where('hours_date', date('Y-m-d', strtotime($date)));
		$this->db->where('is_newest', 1);
		$this->db->order_by('category', 'ASC');
		//execute query
		$daily_hours_category = $this->db->get('hours')->result();

		return $daily_hours_category;
	}

	/**
	 * Get the total number of user hours for a given day
	 * @return array of objects
	 */
	public function get_user_daily_hours_count($date = NULL, $user_id = NULL)
	{
		//build query
		$this->db->select_sum('hours');
		$this->db->where('user_id', $user_id);
		$this->db->where('hours_date', date('Y-m-d', strtotime($date)));
		$this->db->where('is_newest', 1);
		//execute query
		$daily_hours_count = $this->db->get('hours')->result();

		return $daily_hours_count;
	}


	/**
	 * Get user's paid leave by year
	 * @return array of objects
	 */
	public function get_user_paid_leave($year = NULL, $user_id = NULL, $payperiod_start_date = NULL, $payperiod_end_date = NULL)
	{
		//build query
		$this->db->select_sum('hours');
		$this->db->where('user_id', $user_id);
		$this->db->where('category', 'Vacation/PTO');
		if (isset($payperiod_start_date) && isset($payperiod_end_date))
		{
			$this->db->where('hours_date >=', date('Y-m-d', strtotime($payperiod_start_date)));
			$this->db->where('hours_date <=', date('Y-m-d', strtotime($payperiod_end_date)));
		}
		else
		{
			$this->db->where('hours_date >=', date('Y-01-01', strtotime($year . '-01-01')));
			$this->db->where('hours_date <=', date('Y-12-31', strtotime($year . '-01-01')));
		}
		$this->db->where('is_newest', 1);
		$result = $this->db->get('hours')->result();

		return $result[0]->hours;

	}

	/**
	 * Get user's sick leave by year
	 * @return array of objects
	 */
	public function get_user_sick_leave($year = NULL, $user_id = NULL, $payperiod_start_date = NULL, $payperiod_end_date = NULL)
	{
		//build query
		$this->db->select_sum('hours');
		$this->db->where('user_id', $user_id);
		$this->db->where('category', 'Sick Leave');
		if (isset($payperiod_start_date) && isset($payperiod_end_date))
		{
			$this->db->where('hours_date >=', date('Y-m-d', strtotime($payperiod_start_date)));
			$this->db->where('hours_date <=', date('Y-m-d', strtotime($payperiod_end_date)));
		}
		else
		{
			$this->db->where('hours_date >=', date('Y-01-01', strtotime($year . '-01-01')));
			$this->db->where('hours_date <=', date('Y-12-31', strtotime($year . '-01-01')));
		}
		$this->db->where('is_newest', 1);
		$result = $this->db->get('hours')->result();

		return $result[0]->hours;

	}

	/**
	 * Get all active jobs from Jobs Table
	 * @return array of objects
	 */
	public function get_user_payperiod_data($period_start_date = NULL, $period_end_date = NULL, $user_id = NULL)
	{

		//build query
		$this->db->select();
		$this->db->where('user_id', $user_id);
		$this->db->where('hours_date >=', date('Y-m-d', strtotime($period_start_date)));
		$this->db->where('hours_date <=', date('Y-m-d', strtotime($period_end_date)));
		$this->db->where('is_newest', 1);
		$this->db->order_by('hours_date', 'ASC');
		$this->db->order_by('category', 'DESC');
		//execute query
		$pay_period_data = $this->db->get('hours')->result();

		return $pay_period_data;

	}

	/**
	 * Get all active jobs from Jobs Table
	 * @return array of objects
	 */
	public function get_user_approved_payperiod_data($period_start_date = NULL, $period_end_date = NULL, $user_id = NULL)
	{

		//build query
		$this->db->select();
		$this->db->where('user_id', $user_id);
		$this->db->where('hours_date >=', date('Y-m-d', strtotime($period_start_date)));
		$this->db->where('hours_date <=', date('Y-m-d', strtotime($period_end_date)));
		$this->db->where('is_newest', 1);
		$this->db->where('is_approved', 1);
		$this->db->order_by('hours_date', 'ASC');
		$this->db->order_by('category', 'DESC');
		//execute query
		$pay_period_data = $this->db->get('hours')->result();

		return $pay_period_data;

	}

	/**
	 * Get days in a payperiod that a user has submitted hours for
	 * @return array
	 */
	public function get_user_payperiod_days($period_start_date = NULL, $period_end_date = NULL, $user_id = NULL)
	{

		//build query
		$this->db->select('hours_date');
		$this->db->distinct();
		$this->db->where('user_id', $user_id);
		$this->db->where('hours_date >=', date('Y-m-d', strtotime($period_start_date)));
		$this->db->where('hours_date <=', date('Y-m-d', strtotime($period_end_date)));
		$this->db->where('is_newest', 1);
		//execute query
		$pay_period_days =  $this->db->get('hours')->result();

		//initialize array
		$days_with_hours = [];

		//create indexed array of days containing hours
		foreach ($pay_period_days as $day)
		{
			$days_with_hours[$day->hours_date] = true;
		}

		return $days_with_hours;

	}

	/**
	 * Get all unapproved hours for the user
	 * @return array
	 */
	public function get_user_unapproved_hours($user_id = NULL)
	{
		//build query
		$this->db->select('*');
		$this->db->where('user_id', $user_id);
		$this->db->where('is_approved', 0);
		$this->db->where('is_newest', 1);
		$this->db->order_by('hours_date', 'DESC');
		$this->db->order_by('created_at', 'DESC');
		//execute query
		$user_unapproved_hours = $this->db->get('hours')->result_array();

		return $user_unapproved_hours;
	}

	/**
	 * Get all unapproved hours for the user
	 * @return array
	 */
	public function get_user_unapproved_payperiod_hours($user_id = NULL, $payperiod_start_date = NULL, $payperiod_end_date = NULL)
	{
		//build query
		$this->db->select('*');
		$this->db->where('user_id', $user_id);
		$this->db->where('is_approved', 0);
		$this->db->where('is_newest', 1);
		$this->db->where('hours_date >=', date('Y-m-d', strtotime($payperiod_start_date)));
		$this->db->where('hours_date <=', date('Y-m-d', strtotime($payperiod_end_date)));
		$this->db->order_by('hours_date', 'DESC');
		$this->db->order_by('created_at', 'DESC');
		//execute query
		$user_unapproved_hours = $this->db->get('hours')->result_array();

		return $user_unapproved_hours;
	}

	/**
	 * insert user hours batch
	 * @return int rows affected
	 */
	public function insert_batch_hours($hours_data_array = NULL)
	{

		//insert data
		$this->db->insert_batch('hours', $hours_data_array);

		//return affected rows
		return $this->db->affected_rows();

	}

	/**
	 * insert user hours
	 * @return int rows affected
	 */
	public function insert_hours($hours_data = NULL)
	{

		//insert data
		$this->db->insert('hours', $hours_data);

		//return affected rows
		return $this->db->affected_rows();

	}

	/**
	 * Updates hours, sets is_newest = 0, effectively discarding them from view
	 * @return int rows affected
	 */
	public function update_hours_discard($hours_id = NULL, $hours_data = NULL)
	{
		//query
		$this->db->where('id', $hours_id);
		$this->db->update('hours', $hours_data);

		//return affected rows
		return $this->db->affected_rows();

	}

}
