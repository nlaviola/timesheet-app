<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jobs_model extends CI_model {


	/**
	 * update $this->db to use "timesheet" database specified in application/config/database.php
	 *
	 */
	public function __construct()
	{
		parent::__construct();
		//update $this->db to use correct database specified in application/config/database.php
		$this->db = $this->load->database('timesheet', TRUE);
	}

	/**
	 * Define validation rules
	 *
	 */
	public $rules = [
	    'add_job_form'=>
	    [
	        'category' => ['field'=>'category','label'=>'Job Code','rules'=>'trim|required'],
	      	'description' => ['field'=>'description','label'=>'Description','rules'=>'trim|required']
	    ]
	];

	/**
	 * Add Job
	 * @return rows affected
	 */
	public function add_job($job_data)
	{
		//insert data
		$this->db->insert('jobs', $job_data);

		//return affected rows
		return $this->db->affected_rows();
	}

	/**
	 * Get all active jobs from Jobs Table
	 * @return array of objects
	 */
	public function get_active_jobs()
	{

		//build query
		$this->db->select();
		$this->db->where('is_active', '1');
		//execute query
		$active_jobs = $this->db->get('Jobs')->result();

		return $active_jobs;

	}

	public function get_all_jobs()
	{
		//build query
		$this->db->select();
		$jobs = $this->db->get('Jobs')->result();

		return $jobs;
	}

	public function get_inactive_jobs()
	{
		//build query
		$this->db->select();
		$this->db->where('is_active', '0');
		//execute query
		$inactive_jobs = $this->db->get('Jobs')->result();

		return $inactive_jobs;
	}

	public function update_job_status($job_data)
	{
		//query
		$this->db->where('id', $job_data['id']);
		$this->db->update('jobs', $job_data);

		//return affected rows
		return $this->db->affected_rows();
	}



}
